<?php

namespace RFD\AlfaStrahInsurance\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfassistance ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q16:ArrayOfassistance
 * @subpackage Arrays
 */
class ArrayOfassistance extends AbstractStructArrayBase
{
    /**
     * The assistance
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Assistance[]
     */
    public $assistance;
    /**
     * Constructor method for ArrayOfassistance
     * @uses ArrayOfassistance::setAssistance()
     * @param \RFD\AlfaStrahInsurance\StructType\Assistance[] $assistance
     */
    public function __construct(array $assistance = array())
    {
        $this
            ->setAssistance($assistance);
    }
    /**
     * Get assistance value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Assistance[]|null
     */
    public function getAssistance()
    {
        return isset($this->assistance) ? $this->assistance : null;
    }
    /**
     * This method is responsible for validating the values passed to the setAssistance method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAssistance method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAssistanceForArrayConstraintsFromSetAssistance(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfassistanceAssistanceItem) {
            // validation for constraint: itemType
            if (!$arrayOfassistanceAssistanceItem instanceof \RFD\AlfaStrahInsurance\StructType\Assistance) {
                $invalidValues[] = is_object($arrayOfassistanceAssistanceItem) ? get_class($arrayOfassistanceAssistanceItem) : sprintf('%s(%s)', gettype($arrayOfassistanceAssistanceItem), var_export($arrayOfassistanceAssistanceItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The assistance property can only contain items of type \RFD\AlfaStrahInsurance\StructType\Assistance, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set assistance value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\Assistance[] $assistance
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfassistance
     */
    public function setAssistance(array $assistance = array())
    {
        // validation for constraint: array
        if ('' !== ($assistanceArrayErrorMessage = self::validateAssistanceForArrayConstraintsFromSetAssistance($assistance))) {
            throw new \InvalidArgumentException($assistanceArrayErrorMessage, __LINE__);
        }
        if (is_null($assistance) || (is_array($assistance) && empty($assistance))) {
            unset($this->assistance);
        } else {
            $this->assistance = $assistance;
        }
        return $this;
    }
    /**
     * Add item to assistance value
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\Assistance $item
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfassistance
     */
    public function addToAssistance(\RFD\AlfaStrahInsurance\StructType\Assistance $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \RFD\AlfaStrahInsurance\StructType\Assistance) {
            throw new \InvalidArgumentException(sprintf('The assistance property can only contain items of type \RFD\AlfaStrahInsurance\StructType\Assistance, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->assistance[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \RFD\AlfaStrahInsurance\StructType\Assistance|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \RFD\AlfaStrahInsurance\StructType\Assistance|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \RFD\AlfaStrahInsurance\StructType\Assistance|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \RFD\AlfaStrahInsurance\StructType\Assistance|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \RFD\AlfaStrahInsurance\StructType\Assistance|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string assistance
     */
    public function getAttributeName()
    {
        return 'assistance';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfassistance
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
