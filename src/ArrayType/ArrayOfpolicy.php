<?php

namespace RFD\AlfaStrahInsurance\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfpolicy ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q73:ArrayOfpolicy
 * @subpackage Arrays
 */
class ArrayOfpolicy extends AbstractStructArrayBase
{
    /**
     * The policy
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Policy[]
     */
    public $policy;
    /**
     * Constructor method for ArrayOfpolicy
     * @uses ArrayOfpolicy::setPolicy()
     * @param \RFD\AlfaStrahInsurance\StructType\Policy[] $policy
     */
    public function __construct(array $policy = array())
    {
        $this
            ->setPolicy($policy);
    }
    /**
     * Get policy value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Policy[]|null
     */
    public function getPolicy()
    {
        return isset($this->policy) ? $this->policy : null;
    }
    /**
     * This method is responsible for validating the values passed to the setPolicy method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPolicy method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePolicyForArrayConstraintsFromSetPolicy(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfpolicyPolicyItem) {
            // validation for constraint: itemType
            if (!$arrayOfpolicyPolicyItem instanceof \RFD\AlfaStrahInsurance\StructType\Policy) {
                $invalidValues[] = is_object($arrayOfpolicyPolicyItem) ? get_class($arrayOfpolicyPolicyItem) : sprintf('%s(%s)', gettype($arrayOfpolicyPolicyItem), var_export($arrayOfpolicyPolicyItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The policy property can only contain items of type \RFD\AlfaStrahInsurance\StructType\Policy, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set policy value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\Policy[] $policy
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy
     */
    public function setPolicy(array $policy = array())
    {
        // validation for constraint: array
        if ('' !== ($policyArrayErrorMessage = self::validatePolicyForArrayConstraintsFromSetPolicy($policy))) {
            throw new \InvalidArgumentException($policyArrayErrorMessage, __LINE__);
        }
        if (is_null($policy) || (is_array($policy) && empty($policy))) {
            unset($this->policy);
        } else {
            $this->policy = $policy;
        }
        return $this;
    }
    /**
     * Add item to policy value
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\Policy $item
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy
     */
    public function addToPolicy(\RFD\AlfaStrahInsurance\StructType\Policy $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \RFD\AlfaStrahInsurance\StructType\Policy) {
            throw new \InvalidArgumentException(sprintf('The policy property can only contain items of type \RFD\AlfaStrahInsurance\StructType\Policy, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->policy[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \RFD\AlfaStrahInsurance\StructType\Policy|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \RFD\AlfaStrahInsurance\StructType\Policy|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \RFD\AlfaStrahInsurance\StructType\Policy|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \RFD\AlfaStrahInsurance\StructType\Policy|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \RFD\AlfaStrahInsurance\StructType\Policy|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string policy
     */
    public function getAttributeName()
    {
        return 'policy';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
