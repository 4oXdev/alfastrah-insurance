<?php

namespace RFD\AlfaStrahInsurance\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfcountry ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q22:ArrayOfcountry
 * @subpackage Arrays
 */
class ArrayOfcountry extends AbstractStructArrayBase
{
    /**
     * The country
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Country[]
     */
    public $country;
    /**
     * Constructor method for ArrayOfcountry
     * @uses ArrayOfcountry::setCountry()
     * @param \RFD\AlfaStrahInsurance\StructType\Country[] $country
     */
    public function __construct(array $country = array())
    {
        $this
            ->setCountry($country);
    }
    /**
     * Get country value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Country[]|null
     */
    public function getCountry()
    {
        return isset($this->country) ? $this->country : null;
    }
    /**
     * This method is responsible for validating the values passed to the setCountry method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCountry method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCountryForArrayConstraintsFromSetCountry(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfcountryCountryItem) {
            // validation for constraint: itemType
            if (!$arrayOfcountryCountryItem instanceof \RFD\AlfaStrahInsurance\StructType\Country) {
                $invalidValues[] = is_object($arrayOfcountryCountryItem) ? get_class($arrayOfcountryCountryItem) : sprintf('%s(%s)', gettype($arrayOfcountryCountryItem), var_export($arrayOfcountryCountryItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The country property can only contain items of type \RFD\AlfaStrahInsurance\StructType\Country, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set country value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\Country[] $country
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry
     */
    public function setCountry(array $country = array())
    {
        // validation for constraint: array
        if ('' !== ($countryArrayErrorMessage = self::validateCountryForArrayConstraintsFromSetCountry($country))) {
            throw new \InvalidArgumentException($countryArrayErrorMessage, __LINE__);
        }
        if (is_null($country) || (is_array($country) && empty($country))) {
            unset($this->country);
        } else {
            $this->country = $country;
        }
        return $this;
    }
    /**
     * Add item to country value
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\Country $item
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry
     */
    public function addToCountry(\RFD\AlfaStrahInsurance\StructType\Country $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \RFD\AlfaStrahInsurance\StructType\Country) {
            throw new \InvalidArgumentException(sprintf('The country property can only contain items of type \RFD\AlfaStrahInsurance\StructType\Country, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->country[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \RFD\AlfaStrahInsurance\StructType\Country|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \RFD\AlfaStrahInsurance\StructType\Country|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \RFD\AlfaStrahInsurance\StructType\Country|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \RFD\AlfaStrahInsurance\StructType\Country|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \RFD\AlfaStrahInsurance\StructType\Country|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string country
     */
    public function getAttributeName()
    {
        return 'country';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
