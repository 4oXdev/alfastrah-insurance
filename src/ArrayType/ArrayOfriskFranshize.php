<?php

namespace RFD\AlfaStrahInsurance\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfriskFranshize ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q60:ArrayOfriskFranshize
 * @subpackage Arrays
 */
class ArrayOfriskFranshize extends AbstractStructArrayBase
{
    /**
     * The riskFranshize
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\RiskFranshize[]
     */
    public $riskFranshize;
    /**
     * Constructor method for ArrayOfriskFranshize
     * @uses ArrayOfriskFranshize::setRiskFranshize()
     * @param \RFD\AlfaStrahInsurance\StructType\RiskFranshize[] $riskFranshize
     */
    public function __construct(array $riskFranshize = array())
    {
        $this
            ->setRiskFranshize($riskFranshize);
    }
    /**
     * Get riskFranshize value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\RiskFranshize[]|null
     */
    public function getRiskFranshize()
    {
        return isset($this->riskFranshize) ? $this->riskFranshize : null;
    }
    /**
     * This method is responsible for validating the values passed to the setRiskFranshize method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRiskFranshize method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRiskFranshizeForArrayConstraintsFromSetRiskFranshize(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfriskFranshizeRiskFranshizeItem) {
            // validation for constraint: itemType
            if (!$arrayOfriskFranshizeRiskFranshizeItem instanceof \RFD\AlfaStrahInsurance\StructType\RiskFranshize) {
                $invalidValues[] = is_object($arrayOfriskFranshizeRiskFranshizeItem) ? get_class($arrayOfriskFranshizeRiskFranshizeItem) : sprintf('%s(%s)', gettype($arrayOfriskFranshizeRiskFranshizeItem), var_export($arrayOfriskFranshizeRiskFranshizeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The riskFranshize property can only contain items of type \RFD\AlfaStrahInsurance\StructType\RiskFranshize, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set riskFranshize value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\RiskFranshize[] $riskFranshize
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfriskFranshize
     */
    public function setRiskFranshize(array $riskFranshize = array())
    {
        // validation for constraint: array
        if ('' !== ($riskFranshizeArrayErrorMessage = self::validateRiskFranshizeForArrayConstraintsFromSetRiskFranshize($riskFranshize))) {
            throw new \InvalidArgumentException($riskFranshizeArrayErrorMessage, __LINE__);
        }
        if (is_null($riskFranshize) || (is_array($riskFranshize) && empty($riskFranshize))) {
            unset($this->riskFranshize);
        } else {
            $this->riskFranshize = $riskFranshize;
        }
        return $this;
    }
    /**
     * Add item to riskFranshize value
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\RiskFranshize $item
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfriskFranshize
     */
    public function addToRiskFranshize(\RFD\AlfaStrahInsurance\StructType\RiskFranshize $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \RFD\AlfaStrahInsurance\StructType\RiskFranshize) {
            throw new \InvalidArgumentException(sprintf('The riskFranshize property can only contain items of type \RFD\AlfaStrahInsurance\StructType\RiskFranshize, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->riskFranshize[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \RFD\AlfaStrahInsurance\StructType\RiskFranshize|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \RFD\AlfaStrahInsurance\StructType\RiskFranshize|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \RFD\AlfaStrahInsurance\StructType\RiskFranshize|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \RFD\AlfaStrahInsurance\StructType\RiskFranshize|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \RFD\AlfaStrahInsurance\StructType\RiskFranshize|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string riskFranshize
     */
    public function getAttributeName()
    {
        return 'riskFranshize';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfriskFranshize
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
