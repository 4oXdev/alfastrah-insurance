<?php

namespace RFD\AlfaStrahInsurance\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfadditionalCondition ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q7:ArrayOfadditionalCondition
 * @subpackage Arrays
 */
class ArrayOfadditionalCondition extends AbstractStructArrayBase
{
    /**
     * The additionalCondition
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\AdditionalCondition[]
     */
    public $additionalCondition;
    /**
     * Constructor method for ArrayOfadditionalCondition
     * @uses ArrayOfadditionalCondition::setAdditionalCondition()
     * @param \RFD\AlfaStrahInsurance\StructType\AdditionalCondition[] $additionalCondition
     */
    public function __construct(array $additionalCondition = array())
    {
        $this
            ->setAdditionalCondition($additionalCondition);
    }
    /**
     * Get additionalCondition value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalCondition[]|null
     */
    public function getAdditionalCondition()
    {
        return isset($this->additionalCondition) ? $this->additionalCondition : null;
    }
    /**
     * This method is responsible for validating the values passed to the setAdditionalCondition method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAdditionalCondition method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAdditionalConditionForArrayConstraintsFromSetAdditionalCondition(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfadditionalConditionAdditionalConditionItem) {
            // validation for constraint: itemType
            if (!$arrayOfadditionalConditionAdditionalConditionItem instanceof \RFD\AlfaStrahInsurance\StructType\AdditionalCondition) {
                $invalidValues[] = is_object($arrayOfadditionalConditionAdditionalConditionItem) ? get_class($arrayOfadditionalConditionAdditionalConditionItem) : sprintf('%s(%s)', gettype($arrayOfadditionalConditionAdditionalConditionItem), var_export($arrayOfadditionalConditionAdditionalConditionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The additionalCondition property can only contain items of type \RFD\AlfaStrahInsurance\StructType\AdditionalCondition, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set additionalCondition value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\AdditionalCondition[] $additionalCondition
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition
     */
    public function setAdditionalCondition(array $additionalCondition = array())
    {
        // validation for constraint: array
        if ('' !== ($additionalConditionArrayErrorMessage = self::validateAdditionalConditionForArrayConstraintsFromSetAdditionalCondition($additionalCondition))) {
            throw new \InvalidArgumentException($additionalConditionArrayErrorMessage, __LINE__);
        }
        if (is_null($additionalCondition) || (is_array($additionalCondition) && empty($additionalCondition))) {
            unset($this->additionalCondition);
        } else {
            $this->additionalCondition = $additionalCondition;
        }
        return $this;
    }
    /**
     * Add item to additionalCondition value
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\AdditionalCondition $item
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition
     */
    public function addToAdditionalCondition(\RFD\AlfaStrahInsurance\StructType\AdditionalCondition $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \RFD\AlfaStrahInsurance\StructType\AdditionalCondition) {
            throw new \InvalidArgumentException(sprintf('The additionalCondition property can only contain items of type \RFD\AlfaStrahInsurance\StructType\AdditionalCondition, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->additionalCondition[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalCondition|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalCondition|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalCondition|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalCondition|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalCondition|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string additionalCondition
     */
    public function getAttributeName()
    {
        return 'additionalCondition';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
