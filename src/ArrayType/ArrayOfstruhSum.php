<?php

namespace RFD\AlfaStrahInsurance\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfstruhSum ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q36:ArrayOfstruhSum
 * @subpackage Arrays
 */
class ArrayOfstruhSum extends AbstractStructArrayBase
{
    /**
     * The struhSum
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\StruhSum[]
     */
    public $struhSum;
    /**
     * Constructor method for ArrayOfstruhSum
     * @uses ArrayOfstruhSum::setStruhSum()
     * @param \RFD\AlfaStrahInsurance\StructType\StruhSum[] $struhSum
     */
    public function __construct(array $struhSum = array())
    {
        $this
            ->setStruhSum($struhSum);
    }
    /**
     * Get struhSum value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum[]|null
     */
    public function getStruhSum()
    {
        return isset($this->struhSum) ? $this->struhSum : null;
    }
    /**
     * This method is responsible for validating the values passed to the setStruhSum method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStruhSum method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStruhSumForArrayConstraintsFromSetStruhSum(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfstruhSumStruhSumItem) {
            // validation for constraint: itemType
            if (!$arrayOfstruhSumStruhSumItem instanceof \RFD\AlfaStrahInsurance\StructType\StruhSum) {
                $invalidValues[] = is_object($arrayOfstruhSumStruhSumItem) ? get_class($arrayOfstruhSumStruhSumItem) : sprintf('%s(%s)', gettype($arrayOfstruhSumStruhSumItem), var_export($arrayOfstruhSumStruhSumItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The struhSum property can only contain items of type \RFD\AlfaStrahInsurance\StructType\StruhSum, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set struhSum value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\StruhSum[] $struhSum
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfstruhSum
     */
    public function setStruhSum(array $struhSum = array())
    {
        // validation for constraint: array
        if ('' !== ($struhSumArrayErrorMessage = self::validateStruhSumForArrayConstraintsFromSetStruhSum($struhSum))) {
            throw new \InvalidArgumentException($struhSumArrayErrorMessage, __LINE__);
        }
        if (is_null($struhSum) || (is_array($struhSum) && empty($struhSum))) {
            unset($this->struhSum);
        } else {
            $this->struhSum = $struhSum;
        }
        return $this;
    }
    /**
     * Add item to struhSum value
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\StruhSum $item
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfstruhSum
     */
    public function addToStruhSum(\RFD\AlfaStrahInsurance\StructType\StruhSum $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \RFD\AlfaStrahInsurance\StructType\StruhSum) {
            throw new \InvalidArgumentException(sprintf('The struhSum property can only contain items of type \RFD\AlfaStrahInsurance\StructType\StruhSum, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->struhSum[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string struhSum
     */
    public function getAttributeName()
    {
        return 'struhSum';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfstruhSum
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
