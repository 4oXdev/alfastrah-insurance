<?php

namespace RFD\AlfaStrahInsurance\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfinsuranceProgramm ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q48:ArrayOfinsuranceProgramm
 * @subpackage Arrays
 */
class ArrayOfinsuranceProgramm extends AbstractStructArrayBase
{
    /**
     * The insuranceProgramm
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm[]
     */
    public $insuranceProgramm;
    /**
     * Constructor method for ArrayOfinsuranceProgramm
     * @uses ArrayOfinsuranceProgramm::setInsuranceProgramm()
     * @param \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm[] $insuranceProgramm
     */
    public function __construct(array $insuranceProgramm = array())
    {
        $this
            ->setInsuranceProgramm($insuranceProgramm);
    }
    /**
     * Get insuranceProgramm value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm[]|null
     */
    public function getInsuranceProgramm()
    {
        return isset($this->insuranceProgramm) ? $this->insuranceProgramm : null;
    }
    /**
     * This method is responsible for validating the values passed to the setInsuranceProgramm method
     * This method is willingly generated in order to preserve the one-line inline validation within the setInsuranceProgramm method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateInsuranceProgrammForArrayConstraintsFromSetInsuranceProgramm(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfinsuranceProgrammInsuranceProgrammItem) {
            // validation for constraint: itemType
            if (!$arrayOfinsuranceProgrammInsuranceProgrammItem instanceof \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm) {
                $invalidValues[] = is_object($arrayOfinsuranceProgrammInsuranceProgrammItem) ? get_class($arrayOfinsuranceProgrammInsuranceProgrammItem) : sprintf('%s(%s)', gettype($arrayOfinsuranceProgrammInsuranceProgrammItem), var_export($arrayOfinsuranceProgrammInsuranceProgrammItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The insuranceProgramm property can only contain items of type \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set insuranceProgramm value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm[] $insuranceProgramm
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfinsuranceProgramm
     */
    public function setInsuranceProgramm(array $insuranceProgramm = array())
    {
        // validation for constraint: array
        if ('' !== ($insuranceProgrammArrayErrorMessage = self::validateInsuranceProgrammForArrayConstraintsFromSetInsuranceProgramm($insuranceProgramm))) {
            throw new \InvalidArgumentException($insuranceProgrammArrayErrorMessage, __LINE__);
        }
        if (is_null($insuranceProgramm) || (is_array($insuranceProgramm) && empty($insuranceProgramm))) {
            unset($this->insuranceProgramm);
        } else {
            $this->insuranceProgramm = $insuranceProgramm;
        }
        return $this;
    }
    /**
     * Add item to insuranceProgramm value
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm $item
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfinsuranceProgramm
     */
    public function addToInsuranceProgramm(\RFD\AlfaStrahInsurance\StructType\InsuranceProgramm $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm) {
            throw new \InvalidArgumentException(sprintf('The insuranceProgramm property can only contain items of type \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->insuranceProgramm[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string insuranceProgramm
     */
    public function getAttributeName()
    {
        return 'insuranceProgramm';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfinsuranceProgramm
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
