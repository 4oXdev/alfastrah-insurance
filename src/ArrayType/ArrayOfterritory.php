<?php

namespace RFD\AlfaStrahInsurance\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfterritory ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q66:ArrayOfterritory
 * @subpackage Arrays
 */
class ArrayOfterritory extends AbstractStructArrayBase
{
    /**
     * The territory
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Territory[]
     */
    public $territory;
    /**
     * Constructor method for ArrayOfterritory
     * @uses ArrayOfterritory::setTerritory()
     * @param \RFD\AlfaStrahInsurance\StructType\Territory[] $territory
     */
    public function __construct(array $territory = array())
    {
        $this
            ->setTerritory($territory);
    }
    /**
     * Get territory value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Territory[]|null
     */
    public function getTerritory()
    {
        return isset($this->territory) ? $this->territory : null;
    }
    /**
     * This method is responsible for validating the values passed to the setTerritory method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTerritory method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTerritoryForArrayConstraintsFromSetTerritory(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfterritoryTerritoryItem) {
            // validation for constraint: itemType
            if (!$arrayOfterritoryTerritoryItem instanceof \RFD\AlfaStrahInsurance\StructType\Territory) {
                $invalidValues[] = is_object($arrayOfterritoryTerritoryItem) ? get_class($arrayOfterritoryTerritoryItem) : sprintf('%s(%s)', gettype($arrayOfterritoryTerritoryItem), var_export($arrayOfterritoryTerritoryItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The territory property can only contain items of type \RFD\AlfaStrahInsurance\StructType\Territory, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set territory value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\Territory[] $territory
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfterritory
     */
    public function setTerritory(array $territory = array())
    {
        // validation for constraint: array
        if ('' !== ($territoryArrayErrorMessage = self::validateTerritoryForArrayConstraintsFromSetTerritory($territory))) {
            throw new \InvalidArgumentException($territoryArrayErrorMessage, __LINE__);
        }
        if (is_null($territory) || (is_array($territory) && empty($territory))) {
            unset($this->territory);
        } else {
            $this->territory = $territory;
        }
        return $this;
    }
    /**
     * Add item to territory value
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\Territory $item
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfterritory
     */
    public function addToTerritory(\RFD\AlfaStrahInsurance\StructType\Territory $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \RFD\AlfaStrahInsurance\StructType\Territory) {
            throw new \InvalidArgumentException(sprintf('The territory property can only contain items of type \RFD\AlfaStrahInsurance\StructType\Territory, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->territory[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \RFD\AlfaStrahInsurance\StructType\Territory|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \RFD\AlfaStrahInsurance\StructType\Territory|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \RFD\AlfaStrahInsurance\StructType\Territory|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \RFD\AlfaStrahInsurance\StructType\Territory|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \RFD\AlfaStrahInsurance\StructType\Territory|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string territory
     */
    public function getAttributeName()
    {
        return 'territory';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfterritory
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
