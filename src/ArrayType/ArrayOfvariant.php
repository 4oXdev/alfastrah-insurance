<?php

namespace RFD\AlfaStrahInsurance\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfvariant ArrayType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q42:ArrayOfvariant
 * @subpackage Arrays
 */
class ArrayOfvariant extends AbstractStructArrayBase
{
    /**
     * The variant
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Variant[]
     */
    public $variant;
    /**
     * Constructor method for ArrayOfvariant
     * @uses ArrayOfvariant::setVariant()
     * @param \RFD\AlfaStrahInsurance\StructType\Variant[] $variant
     */
    public function __construct(array $variant = array())
    {
        $this
            ->setVariant($variant);
    }
    /**
     * Get variant value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Variant[]|null
     */
    public function getVariant()
    {
        return isset($this->variant) ? $this->variant : null;
    }
    /**
     * This method is responsible for validating the values passed to the setVariant method
     * This method is willingly generated in order to preserve the one-line inline validation within the setVariant method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateVariantForArrayConstraintsFromSetVariant(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfvariantVariantItem) {
            // validation for constraint: itemType
            if (!$arrayOfvariantVariantItem instanceof \RFD\AlfaStrahInsurance\StructType\Variant) {
                $invalidValues[] = is_object($arrayOfvariantVariantItem) ? get_class($arrayOfvariantVariantItem) : sprintf('%s(%s)', gettype($arrayOfvariantVariantItem), var_export($arrayOfvariantVariantItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The variant property can only contain items of type \RFD\AlfaStrahInsurance\StructType\Variant, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set variant value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\Variant[] $variant
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfvariant
     */
    public function setVariant(array $variant = array())
    {
        // validation for constraint: array
        if ('' !== ($variantArrayErrorMessage = self::validateVariantForArrayConstraintsFromSetVariant($variant))) {
            throw new \InvalidArgumentException($variantArrayErrorMessage, __LINE__);
        }
        if (is_null($variant) || (is_array($variant) && empty($variant))) {
            unset($this->variant);
        } else {
            $this->variant = $variant;
        }
        return $this;
    }
    /**
     * Add item to variant value
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\Variant $item
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfvariant
     */
    public function addToVariant(\RFD\AlfaStrahInsurance\StructType\Variant $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \RFD\AlfaStrahInsurance\StructType\Variant) {
            throw new \InvalidArgumentException(sprintf('The variant property can only contain items of type \RFD\AlfaStrahInsurance\StructType\Variant, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->variant[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \RFD\AlfaStrahInsurance\StructType\Variant|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \RFD\AlfaStrahInsurance\StructType\Variant|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \RFD\AlfaStrahInsurance\StructType\Variant|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \RFD\AlfaStrahInsurance\StructType\Variant|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \RFD\AlfaStrahInsurance\StructType\Variant|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string variant
     */
    public function getAttributeName()
    {
        return 'variant';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfvariant
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
