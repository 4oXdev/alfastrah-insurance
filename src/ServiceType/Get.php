<?php

namespace RFD\AlfaStrahInsurance\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get ServiceType
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named GetAdditionalConditions
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetAdditionalConditions $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetAdditionalConditionsResponse|bool
     */
    public function GetAdditionalConditions(\RFD\AlfaStrahInsurance\StructType\GetAdditionalConditions $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetAdditionalConditions($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetAdditionalConditions2
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetAdditionalConditions2 $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetAdditionalConditions2Response|bool
     */
    public function GetAdditionalConditions2(\RFD\AlfaStrahInsurance\StructType\GetAdditionalConditions2 $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetAdditionalConditions2($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetAssistance
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetAssistance $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetAssistanceResponse|bool
     */
    public function GetAssistance(\RFD\AlfaStrahInsurance\StructType\GetAssistance $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetAssistance($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCountries
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetCountries $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetCountriesResponse|bool
     */
    public function GetCountries(\RFD\AlfaStrahInsurance\StructType\GetCountries $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetCountries($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCountriesRus
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetCountriesRus $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetCountriesRusResponse|bool
     */
    public function GetCountriesRus(\RFD\AlfaStrahInsurance\StructType\GetCountriesRus $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetCountriesRus($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetCurrency
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetCurrency $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetCurrencyResponse|bool
     */
    public function GetCurrency(\RFD\AlfaStrahInsurance\StructType\GetCurrency $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetCurrency($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetStruhSum
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetStruhSum $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetStruhSumResponse|bool
     */
    public function GetStruhSum(\RFD\AlfaStrahInsurance\StructType\GetStruhSum $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetStruhSum($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetVariant
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetVariant $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetVariantResponse|bool
     */
    public function GetVariant(\RFD\AlfaStrahInsurance\StructType\GetVariant $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetVariant($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetInsuranceProgramms
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetInsuranceProgramms $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetInsuranceProgrammsResponse|bool
     */
    public function GetInsuranceProgramms(\RFD\AlfaStrahInsurance\StructType\GetInsuranceProgramms $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetInsuranceProgramms($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetRisks
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetRisks $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetRisksResponse|bool
     */
    public function GetRisks(\RFD\AlfaStrahInsurance\StructType\GetRisks $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetRisks($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetFransize
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetFransize $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetFransizeResponse|bool
     */
    public function GetFransize(\RFD\AlfaStrahInsurance\StructType\GetFransize $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetFransize($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetTerritory
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetTerritory $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetTerritoryResponse|bool
     */
    public function GetTerritory(\RFD\AlfaStrahInsurance\StructType\GetTerritory $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetTerritory($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetPoliciesByBeginDate
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetPoliciesByBeginDate $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesByBeginDateResponse|bool
     */
    public function GetPoliciesByBeginDate(\RFD\AlfaStrahInsurance\StructType\GetPoliciesByBeginDate $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetPoliciesByBeginDate($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetPoliciesByCreateDate
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetPoliciesByCreateDate $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesByCreateDateResponse|bool
     */
    public function GetPoliciesByCreateDate(\RFD\AlfaStrahInsurance\StructType\GetPoliciesByCreateDate $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetPoliciesByCreateDate($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetPoliciesByPolicyNumber
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\GetPoliciesByPolicyNumber $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesByPolicyNumberResponse|bool
     */
    public function GetPoliciesByPolicyNumber(\RFD\AlfaStrahInsurance\StructType\GetPoliciesByPolicyNumber $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetPoliciesByPolicyNumber($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \RFD\AlfaStrahInsurance\StructType\GetAdditionalConditions2Response|\RFD\AlfaStrahInsurance\StructType\GetAdditionalConditionsResponse|\RFD\AlfaStrahInsurance\StructType\GetAssistanceResponse|\RFD\AlfaStrahInsurance\StructType\GetCountriesResponse|\RFD\AlfaStrahInsurance\StructType\GetCountriesRusResponse|\RFD\AlfaStrahInsurance\StructType\GetCurrencyResponse|\RFD\AlfaStrahInsurance\StructType\GetFransizeResponse|\RFD\AlfaStrahInsurance\StructType\GetInsuranceProgrammsResponse|\RFD\AlfaStrahInsurance\StructType\GetPoliciesByBeginDateResponse|\RFD\AlfaStrahInsurance\StructType\GetPoliciesByCreateDateResponse|\RFD\AlfaStrahInsurance\StructType\GetPoliciesByPolicyNumberResponse|\RFD\AlfaStrahInsurance\StructType\GetRisksResponse|\RFD\AlfaStrahInsurance\StructType\GetStruhSumResponse|\RFD\AlfaStrahInsurance\StructType\GetTerritoryResponse|\RFD\AlfaStrahInsurance\StructType\GetVariantResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
