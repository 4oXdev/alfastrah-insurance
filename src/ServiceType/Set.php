<?php

namespace RFD\AlfaStrahInsurance\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Set ServiceType
 * @subpackage Services
 */
class Set extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SetCancelPolicy
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\SetCancelPolicy $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\SetCancelPolicyResponse|bool
     */
    public function SetCancelPolicy(\RFD\AlfaStrahInsurance\StructType\SetCancelPolicy $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->SetCancelPolicy($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named SetAcceptPolicy
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\SetAcceptPolicy $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\SetAcceptPolicyResponse|bool
     */
    public function SetAcceptPolicy(\RFD\AlfaStrahInsurance\StructType\SetAcceptPolicy $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->SetAcceptPolicy($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named SetPayPolicy
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\SetPayPolicy $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\SetPayPolicyResponse|bool
     */
    public function SetPayPolicy(\RFD\AlfaStrahInsurance\StructType\SetPayPolicy $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->SetPayPolicy($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \RFD\AlfaStrahInsurance\StructType\SetAcceptPolicyResponse|\RFD\AlfaStrahInsurance\StructType\SetCancelPolicyResponse|\RFD\AlfaStrahInsurance\StructType\SetPayPolicyResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
