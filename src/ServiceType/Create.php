<?php

namespace RFD\AlfaStrahInsurance\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Create ServiceType
 * @subpackage Services
 */
class Create extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named CreatePayPolicy
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\CreatePayPolicy $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\CreatePayPolicyResponse|bool
     */
    public function CreatePayPolicy(\RFD\AlfaStrahInsurance\StructType\CreatePayPolicy $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->CreatePayPolicy($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \RFD\AlfaStrahInsurance\StructType\CreatePayPolicyResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
