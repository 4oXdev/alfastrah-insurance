<?php

namespace RFD\AlfaStrahInsurance\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Person ServiceType
 * @subpackage Services
 */
class Person extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named PersonEditPolicy
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\PersonEditPolicy $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\PersonEditPolicyResponse|bool
     */
    public function PersonEditPolicy(\RFD\AlfaStrahInsurance\StructType\PersonEditPolicy $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->PersonEditPolicy($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \RFD\AlfaStrahInsurance\StructType\PersonEditPolicyResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
