<?php

namespace RFD\AlfaStrahInsurance\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for New ServiceType
 * @subpackage Services
 */
class _New extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named NewPolicty
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \RFD\AlfaStrahInsurance\StructType\NewPolicty $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\NewPolictyResponse|bool
     */
    public function NewPolicty(\RFD\AlfaStrahInsurance\StructType\NewPolicty $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->NewPolicty($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \RFD\AlfaStrahInsurance\StructType\NewPolictyResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
