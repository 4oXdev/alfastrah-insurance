<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for acceptPolicyParameters StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: q122:acceptPolicyParameters
 * @subpackage Structs
 */
class AcceptPolicyParameters extends AbstractStructBase
{
    /**
     * The agentLogin
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $agentLogin;
    /**
     * The agentPassword
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $agentPassword;
    /**
     * The agentUid
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $agentUid;
    /**
     * The number
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $number;
    /**
     * Constructor method for acceptPolicyParameters
     * @uses AcceptPolicyParameters::setAgentLogin()
     * @uses AcceptPolicyParameters::setAgentPassword()
     * @uses AcceptPolicyParameters::setAgentUid()
     * @uses AcceptPolicyParameters::setNumber()
     * @param string $agentLogin
     * @param string $agentPassword
     * @param string $agentUid
     * @param string $number
     */
    public function __construct($agentLogin = null, $agentPassword = null, $agentUid = null, $number = null)
    {
        $this
            ->setAgentLogin($agentLogin)
            ->setAgentPassword($agentPassword)
            ->setAgentUid($agentUid)
            ->setNumber($number);
    }
    /**
     * Get agentLogin value
     * @return string|null
     */
    public function getAgentLogin()
    {
        return $this->agentLogin;
    }
    /**
     * Set agentLogin value
     * @param string $agentLogin
     * @return \RFD\AlfaStrahInsurance\StructType\AcceptPolicyParameters
     */
    public function setAgentLogin($agentLogin = null)
    {
        // validation for constraint: string
        if (!is_null($agentLogin) && !is_string($agentLogin)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($agentLogin, true), gettype($agentLogin)), __LINE__);
        }
        $this->agentLogin = $agentLogin;
        return $this;
    }
    /**
     * Get agentPassword value
     * @return string|null
     */
    public function getAgentPassword()
    {
        return $this->agentPassword;
    }
    /**
     * Set agentPassword value
     * @param string $agentPassword
     * @return \RFD\AlfaStrahInsurance\StructType\AcceptPolicyParameters
     */
    public function setAgentPassword($agentPassword = null)
    {
        // validation for constraint: string
        if (!is_null($agentPassword) && !is_string($agentPassword)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($agentPassword, true), gettype($agentPassword)), __LINE__);
        }
        $this->agentPassword = $agentPassword;
        return $this;
    }
    /**
     * Get agentUid value
     * @return string|null
     */
    public function getAgentUid()
    {
        return $this->agentUid;
    }
    /**
     * Set agentUid value
     * @param string $agentUid
     * @return \RFD\AlfaStrahInsurance\StructType\AcceptPolicyParameters
     */
    public function setAgentUid($agentUid = null)
    {
        // validation for constraint: string
        if (!is_null($agentUid) && !is_string($agentUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($agentUid, true), gettype($agentUid)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($agentUid) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $agentUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($agentUid, true)), __LINE__);
        }
        $this->agentUid = $agentUid;
        return $this;
    }
    /**
     * Get number value
     * @return string|null
     */
    public function getNumber()
    {
        return $this->number;
    }
    /**
     * Set number value
     * @param string $number
     * @return \RFD\AlfaStrahInsurance\StructType\AcceptPolicyParameters
     */
    public function setNumber($number = null)
    {
        // validation for constraint: string
        if (!is_null($number) && !is_string($number)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($number, true), gettype($number)), __LINE__);
        }
        $this->number = $number;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\AcceptPolicyParameters
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
