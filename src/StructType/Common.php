<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for common StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q84:common
 * @subpackage Structs
 */
class Common extends AbstractStructBase
{
    /**
     * The additionalCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $additionalCode;
    /**
     * The additionalCondition
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $additionalCondition;
    /**
     * The additionalCondition2
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $additionalCondition2;
    /**
     * The additionalConditionName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $additionalConditionName;
    /**
     * The additionalConditionName2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $additionalConditionName2;
    /**
     * The addressTel
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $addressTel;
    /**
     * The assistanceCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $assistanceCode;
    /**
     * The assistanceID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $assistanceID;
    /**
     * The assistanceName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $assistanceName;
    /**
     * The assistancePhones
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $assistancePhones;
    /**
     * The comments
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $comments;
    /**
     * The countriesForPrint
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $countriesForPrint;
    /**
     * The countryName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $countryName;
    /**
     * The countryUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $countryUID;
    /**
     * The dateOfBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $dateOfBirth;
    /**
     * The depCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $depCode;
    /**
     * The dtCreated
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $dtCreated;
    /**
     * The eurRate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $eurRate;
    /**
     * The fio
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $fio;
    /**
     * The insuranceProgramm
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $insuranceProgramm;
    /**
     * The insuranceProgrammPrintName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $insuranceProgrammPrintName;
    /**
     * The insuranceProgrammUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $insuranceProgrammUID;
    /**
     * The multiDays
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $multiDays;
    /**
     * The nettoPremRUR
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $nettoPremRUR;
    /**
     * The operation
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $operation;
    /**
     * The plicyConfirmDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $plicyConfirmDate;
    /**
     * The policyID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $policyID;
    /**
     * The policyLink
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $policyLink;
    /**
     * The policyNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $policyNumber;
    /**
     * The policyPeriodFrom
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $policyPeriodFrom;
    /**
     * The policyPeriodTill
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $policyPeriodTill;
    /**
     * The policyPlace
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $policyPlace;
    /**
     * The policyStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $policyStatus;
    /**
     * The policyUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $policyUID;
    /**
     * The premRUR
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $premRUR;
    /**
     * The territory
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $territory;
    /**
     * The territoryENG
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $territoryENG;
    /**
     * The territoryRUR
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $territoryRUR;
    /**
     * The usdRate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $usdRate;
    /**
     * The userId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $userId;
    /**
     * The usersLogin
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $usersLogin;
    /**
     * The usersName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $usersName;
    /**
     * Constructor method for common
     * @uses Common::setAdditionalCode()
     * @uses Common::setAdditionalCondition()
     * @uses Common::setAdditionalCondition2()
     * @uses Common::setAdditionalConditionName()
     * @uses Common::setAdditionalConditionName2()
     * @uses Common::setAddressTel()
     * @uses Common::setAssistanceCode()
     * @uses Common::setAssistanceID()
     * @uses Common::setAssistanceName()
     * @uses Common::setAssistancePhones()
     * @uses Common::setComments()
     * @uses Common::setCountriesForPrint()
     * @uses Common::setCountryName()
     * @uses Common::setCountryUID()
     * @uses Common::setDateOfBirth()
     * @uses Common::setDepCode()
     * @uses Common::setDtCreated()
     * @uses Common::setEurRate()
     * @uses Common::setFio()
     * @uses Common::setInsuranceProgramm()
     * @uses Common::setInsuranceProgrammPrintName()
     * @uses Common::setInsuranceProgrammUID()
     * @uses Common::setMultiDays()
     * @uses Common::setNettoPremRUR()
     * @uses Common::setOperation()
     * @uses Common::setPlicyConfirmDate()
     * @uses Common::setPolicyID()
     * @uses Common::setPolicyLink()
     * @uses Common::setPolicyNumber()
     * @uses Common::setPolicyPeriodFrom()
     * @uses Common::setPolicyPeriodTill()
     * @uses Common::setPolicyPlace()
     * @uses Common::setPolicyStatus()
     * @uses Common::setPolicyUID()
     * @uses Common::setPremRUR()
     * @uses Common::setTerritory()
     * @uses Common::setTerritoryENG()
     * @uses Common::setTerritoryRUR()
     * @uses Common::setUsdRate()
     * @uses Common::setUserId()
     * @uses Common::setUsersLogin()
     * @uses Common::setUsersName()
     * @param string $additionalCode
     * @param string $additionalCondition
     * @param string $additionalCondition2
     * @param string $additionalConditionName
     * @param string $additionalConditionName2
     * @param string $addressTel
     * @param string $assistanceCode
     * @param string $assistanceID
     * @param string $assistanceName
     * @param string $assistancePhones
     * @param string $comments
     * @param string $countriesForPrint
     * @param string $countryName
     * @param string $countryUID
     * @param string $dateOfBirth
     * @param string $depCode
     * @param string $dtCreated
     * @param float $eurRate
     * @param string $fio
     * @param string $insuranceProgramm
     * @param string $insuranceProgrammPrintName
     * @param string $insuranceProgrammUID
     * @param int $multiDays
     * @param float $nettoPremRUR
     * @param string $operation
     * @param string $plicyConfirmDate
     * @param int $policyID
     * @param string $policyLink
     * @param string $policyNumber
     * @param string $policyPeriodFrom
     * @param string $policyPeriodTill
     * @param string $policyPlace
     * @param string $policyStatus
     * @param string $policyUID
     * @param float $premRUR
     * @param string $territory
     * @param string $territoryENG
     * @param string $territoryRUR
     * @param float $usdRate
     * @param string $userId
     * @param string $usersLogin
     * @param string $usersName
     */
    public function __construct($additionalCode = null, $additionalCondition = null, $additionalCondition2 = null, $additionalConditionName = null, $additionalConditionName2 = null, $addressTel = null, $assistanceCode = null, $assistanceID = null, $assistanceName = null, $assistancePhones = null, $comments = null, $countriesForPrint = null, $countryName = null, $countryUID = null, $dateOfBirth = null, $depCode = null, $dtCreated = null, $eurRate = null, $fio = null, $insuranceProgramm = null, $insuranceProgrammPrintName = null, $insuranceProgrammUID = null, $multiDays = null, $nettoPremRUR = null, $operation = null, $plicyConfirmDate = null, $policyID = null, $policyLink = null, $policyNumber = null, $policyPeriodFrom = null, $policyPeriodTill = null, $policyPlace = null, $policyStatus = null, $policyUID = null, $premRUR = null, $territory = null, $territoryENG = null, $territoryRUR = null, $usdRate = null, $userId = null, $usersLogin = null, $usersName = null)
    {
        $this
            ->setAdditionalCode($additionalCode)
            ->setAdditionalCondition($additionalCondition)
            ->setAdditionalCondition2($additionalCondition2)
            ->setAdditionalConditionName($additionalConditionName)
            ->setAdditionalConditionName2($additionalConditionName2)
            ->setAddressTel($addressTel)
            ->setAssistanceCode($assistanceCode)
            ->setAssistanceID($assistanceID)
            ->setAssistanceName($assistanceName)
            ->setAssistancePhones($assistancePhones)
            ->setComments($comments)
            ->setCountriesForPrint($countriesForPrint)
            ->setCountryName($countryName)
            ->setCountryUID($countryUID)
            ->setDateOfBirth($dateOfBirth)
            ->setDepCode($depCode)
            ->setDtCreated($dtCreated)
            ->setEurRate($eurRate)
            ->setFio($fio)
            ->setInsuranceProgramm($insuranceProgramm)
            ->setInsuranceProgrammPrintName($insuranceProgrammPrintName)
            ->setInsuranceProgrammUID($insuranceProgrammUID)
            ->setMultiDays($multiDays)
            ->setNettoPremRUR($nettoPremRUR)
            ->setOperation($operation)
            ->setPlicyConfirmDate($plicyConfirmDate)
            ->setPolicyID($policyID)
            ->setPolicyLink($policyLink)
            ->setPolicyNumber($policyNumber)
            ->setPolicyPeriodFrom($policyPeriodFrom)
            ->setPolicyPeriodTill($policyPeriodTill)
            ->setPolicyPlace($policyPlace)
            ->setPolicyStatus($policyStatus)
            ->setPolicyUID($policyUID)
            ->setPremRUR($premRUR)
            ->setTerritory($territory)
            ->setTerritoryENG($territoryENG)
            ->setTerritoryRUR($territoryRUR)
            ->setUsdRate($usdRate)
            ->setUserId($userId)
            ->setUsersLogin($usersLogin)
            ->setUsersName($usersName);
    }
    /**
     * Get additionalCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdditionalCode()
    {
        return isset($this->additionalCode) ? $this->additionalCode : null;
    }
    /**
     * Set additionalCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $additionalCode
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setAdditionalCode($additionalCode = null)
    {
        // validation for constraint: string
        if (!is_null($additionalCode) && !is_string($additionalCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalCode, true), gettype($additionalCode)), __LINE__);
        }
        if (is_null($additionalCode) || (is_array($additionalCode) && empty($additionalCode))) {
            unset($this->additionalCode);
        } else {
            $this->additionalCode = $additionalCode;
        }
        return $this;
    }
    /**
     * Get additionalCondition value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdditionalCondition()
    {
        return isset($this->additionalCondition) ? $this->additionalCondition : null;
    }
    /**
     * Set additionalCondition value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $additionalCondition
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setAdditionalCondition($additionalCondition = null)
    {
        // validation for constraint: string
        if (!is_null($additionalCondition) && !is_string($additionalCondition)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalCondition, true), gettype($additionalCondition)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($additionalCondition) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $additionalCondition)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($additionalCondition, true)), __LINE__);
        }
        if (is_null($additionalCondition) || (is_array($additionalCondition) && empty($additionalCondition))) {
            unset($this->additionalCondition);
        } else {
            $this->additionalCondition = $additionalCondition;
        }
        return $this;
    }
    /**
     * Get additionalCondition2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdditionalCondition2()
    {
        return isset($this->additionalCondition2) ? $this->additionalCondition2 : null;
    }
    /**
     * Set additionalCondition2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $additionalCondition2
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setAdditionalCondition2($additionalCondition2 = null)
    {
        // validation for constraint: string
        if (!is_null($additionalCondition2) && !is_string($additionalCondition2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalCondition2, true), gettype($additionalCondition2)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($additionalCondition2) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $additionalCondition2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($additionalCondition2, true)), __LINE__);
        }
        if (is_null($additionalCondition2) || (is_array($additionalCondition2) && empty($additionalCondition2))) {
            unset($this->additionalCondition2);
        } else {
            $this->additionalCondition2 = $additionalCondition2;
        }
        return $this;
    }
    /**
     * Get additionalConditionName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdditionalConditionName()
    {
        return isset($this->additionalConditionName) ? $this->additionalConditionName : null;
    }
    /**
     * Set additionalConditionName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $additionalConditionName
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setAdditionalConditionName($additionalConditionName = null)
    {
        // validation for constraint: string
        if (!is_null($additionalConditionName) && !is_string($additionalConditionName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalConditionName, true), gettype($additionalConditionName)), __LINE__);
        }
        if (is_null($additionalConditionName) || (is_array($additionalConditionName) && empty($additionalConditionName))) {
            unset($this->additionalConditionName);
        } else {
            $this->additionalConditionName = $additionalConditionName;
        }
        return $this;
    }
    /**
     * Get additionalConditionName2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdditionalConditionName2()
    {
        return isset($this->additionalConditionName2) ? $this->additionalConditionName2 : null;
    }
    /**
     * Set additionalConditionName2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $additionalConditionName2
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setAdditionalConditionName2($additionalConditionName2 = null)
    {
        // validation for constraint: string
        if (!is_null($additionalConditionName2) && !is_string($additionalConditionName2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalConditionName2, true), gettype($additionalConditionName2)), __LINE__);
        }
        if (is_null($additionalConditionName2) || (is_array($additionalConditionName2) && empty($additionalConditionName2))) {
            unset($this->additionalConditionName2);
        } else {
            $this->additionalConditionName2 = $additionalConditionName2;
        }
        return $this;
    }
    /**
     * Get addressTel value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressTel()
    {
        return isset($this->addressTel) ? $this->addressTel : null;
    }
    /**
     * Set addressTel value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressTel
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setAddressTel($addressTel = null)
    {
        // validation for constraint: string
        if (!is_null($addressTel) && !is_string($addressTel)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressTel, true), gettype($addressTel)), __LINE__);
        }
        if (is_null($addressTel) || (is_array($addressTel) && empty($addressTel))) {
            unset($this->addressTel);
        } else {
            $this->addressTel = $addressTel;
        }
        return $this;
    }
    /**
     * Get assistanceCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAssistanceCode()
    {
        return isset($this->assistanceCode) ? $this->assistanceCode : null;
    }
    /**
     * Set assistanceCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $assistanceCode
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setAssistanceCode($assistanceCode = null)
    {
        // validation for constraint: string
        if (!is_null($assistanceCode) && !is_string($assistanceCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($assistanceCode, true), gettype($assistanceCode)), __LINE__);
        }
        if (is_null($assistanceCode) || (is_array($assistanceCode) && empty($assistanceCode))) {
            unset($this->assistanceCode);
        } else {
            $this->assistanceCode = $assistanceCode;
        }
        return $this;
    }
    /**
     * Get assistanceID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAssistanceID()
    {
        return isset($this->assistanceID) ? $this->assistanceID : null;
    }
    /**
     * Set assistanceID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $assistanceID
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setAssistanceID($assistanceID = null)
    {
        // validation for constraint: string
        if (!is_null($assistanceID) && !is_string($assistanceID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($assistanceID, true), gettype($assistanceID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($assistanceID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $assistanceID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($assistanceID, true)), __LINE__);
        }
        if (is_null($assistanceID) || (is_array($assistanceID) && empty($assistanceID))) {
            unset($this->assistanceID);
        } else {
            $this->assistanceID = $assistanceID;
        }
        return $this;
    }
    /**
     * Get assistanceName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAssistanceName()
    {
        return isset($this->assistanceName) ? $this->assistanceName : null;
    }
    /**
     * Set assistanceName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $assistanceName
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setAssistanceName($assistanceName = null)
    {
        // validation for constraint: string
        if (!is_null($assistanceName) && !is_string($assistanceName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($assistanceName, true), gettype($assistanceName)), __LINE__);
        }
        if (is_null($assistanceName) || (is_array($assistanceName) && empty($assistanceName))) {
            unset($this->assistanceName);
        } else {
            $this->assistanceName = $assistanceName;
        }
        return $this;
    }
    /**
     * Get assistancePhones value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAssistancePhones()
    {
        return isset($this->assistancePhones) ? $this->assistancePhones : null;
    }
    /**
     * Set assistancePhones value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $assistancePhones
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setAssistancePhones($assistancePhones = null)
    {
        // validation for constraint: string
        if (!is_null($assistancePhones) && !is_string($assistancePhones)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($assistancePhones, true), gettype($assistancePhones)), __LINE__);
        }
        if (is_null($assistancePhones) || (is_array($assistancePhones) && empty($assistancePhones))) {
            unset($this->assistancePhones);
        } else {
            $this->assistancePhones = $assistancePhones;
        }
        return $this;
    }
    /**
     * Get comments value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getComments()
    {
        return isset($this->comments) ? $this->comments : null;
    }
    /**
     * Set comments value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $comments
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setComments($comments = null)
    {
        // validation for constraint: string
        if (!is_null($comments) && !is_string($comments)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($comments, true), gettype($comments)), __LINE__);
        }
        if (is_null($comments) || (is_array($comments) && empty($comments))) {
            unset($this->comments);
        } else {
            $this->comments = $comments;
        }
        return $this;
    }
    /**
     * Get countriesForPrint value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountriesForPrint()
    {
        return isset($this->countriesForPrint) ? $this->countriesForPrint : null;
    }
    /**
     * Set countriesForPrint value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countriesForPrint
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setCountriesForPrint($countriesForPrint = null)
    {
        // validation for constraint: string
        if (!is_null($countriesForPrint) && !is_string($countriesForPrint)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countriesForPrint, true), gettype($countriesForPrint)), __LINE__);
        }
        if (is_null($countriesForPrint) || (is_array($countriesForPrint) && empty($countriesForPrint))) {
            unset($this->countriesForPrint);
        } else {
            $this->countriesForPrint = $countriesForPrint;
        }
        return $this;
    }
    /**
     * Get countryName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryName()
    {
        return isset($this->countryName) ? $this->countryName : null;
    }
    /**
     * Set countryName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryName
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setCountryName($countryName = null)
    {
        // validation for constraint: string
        if (!is_null($countryName) && !is_string($countryName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryName, true), gettype($countryName)), __LINE__);
        }
        if (is_null($countryName) || (is_array($countryName) && empty($countryName))) {
            unset($this->countryName);
        } else {
            $this->countryName = $countryName;
        }
        return $this;
    }
    /**
     * Get countryUID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryUID()
    {
        return isset($this->countryUID) ? $this->countryUID : null;
    }
    /**
     * Set countryUID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryUID
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setCountryUID($countryUID = null)
    {
        // validation for constraint: string
        if (!is_null($countryUID) && !is_string($countryUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryUID, true), gettype($countryUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($countryUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $countryUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($countryUID, true)), __LINE__);
        }
        if (is_null($countryUID) || (is_array($countryUID) && empty($countryUID))) {
            unset($this->countryUID);
        } else {
            $this->countryUID = $countryUID;
        }
        return $this;
    }
    /**
     * Get dateOfBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDateOfBirth()
    {
        return isset($this->dateOfBirth) ? $this->dateOfBirth : null;
    }
    /**
     * Set dateOfBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dateOfBirth
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setDateOfBirth($dateOfBirth = null)
    {
        // validation for constraint: string
        if (!is_null($dateOfBirth) && !is_string($dateOfBirth)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateOfBirth, true), gettype($dateOfBirth)), __LINE__);
        }
        if (is_null($dateOfBirth) || (is_array($dateOfBirth) && empty($dateOfBirth))) {
            unset($this->dateOfBirth);
        } else {
            $this->dateOfBirth = $dateOfBirth;
        }
        return $this;
    }
    /**
     * Get depCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDepCode()
    {
        return isset($this->depCode) ? $this->depCode : null;
    }
    /**
     * Set depCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $depCode
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setDepCode($depCode = null)
    {
        // validation for constraint: string
        if (!is_null($depCode) && !is_string($depCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($depCode, true), gettype($depCode)), __LINE__);
        }
        if (is_null($depCode) || (is_array($depCode) && empty($depCode))) {
            unset($this->depCode);
        } else {
            $this->depCode = $depCode;
        }
        return $this;
    }
    /**
     * Get dtCreated value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDtCreated()
    {
        return isset($this->dtCreated) ? $this->dtCreated : null;
    }
    /**
     * Set dtCreated value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dtCreated
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setDtCreated($dtCreated = null)
    {
        // validation for constraint: string
        if (!is_null($dtCreated) && !is_string($dtCreated)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dtCreated, true), gettype($dtCreated)), __LINE__);
        }
        if (is_null($dtCreated) || (is_array($dtCreated) && empty($dtCreated))) {
            unset($this->dtCreated);
        } else {
            $this->dtCreated = $dtCreated;
        }
        return $this;
    }
    /**
     * Get eurRate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getEurRate()
    {
        return isset($this->eurRate) ? $this->eurRate : null;
    }
    /**
     * Set eurRate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $eurRate
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setEurRate($eurRate = null)
    {
        // validation for constraint: float
        if (!is_null($eurRate) && !(is_float($eurRate) || is_numeric($eurRate))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($eurRate, true), gettype($eurRate)), __LINE__);
        }
        if (is_null($eurRate) || (is_array($eurRate) && empty($eurRate))) {
            unset($this->eurRate);
        } else {
            $this->eurRate = $eurRate;
        }
        return $this;
    }
    /**
     * Get fio value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFio()
    {
        return isset($this->fio) ? $this->fio : null;
    }
    /**
     * Set fio value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $fio
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setFio($fio = null)
    {
        // validation for constraint: string
        if (!is_null($fio) && !is_string($fio)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fio, true), gettype($fio)), __LINE__);
        }
        if (is_null($fio) || (is_array($fio) && empty($fio))) {
            unset($this->fio);
        } else {
            $this->fio = $fio;
        }
        return $this;
    }
    /**
     * Get insuranceProgramm value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getInsuranceProgramm()
    {
        return isset($this->insuranceProgramm) ? $this->insuranceProgramm : null;
    }
    /**
     * Set insuranceProgramm value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $insuranceProgramm
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setInsuranceProgramm($insuranceProgramm = null)
    {
        // validation for constraint: string
        if (!is_null($insuranceProgramm) && !is_string($insuranceProgramm)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($insuranceProgramm, true), gettype($insuranceProgramm)), __LINE__);
        }
        if (is_null($insuranceProgramm) || (is_array($insuranceProgramm) && empty($insuranceProgramm))) {
            unset($this->insuranceProgramm);
        } else {
            $this->insuranceProgramm = $insuranceProgramm;
        }
        return $this;
    }
    /**
     * Get insuranceProgrammPrintName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getInsuranceProgrammPrintName()
    {
        return isset($this->insuranceProgrammPrintName) ? $this->insuranceProgrammPrintName : null;
    }
    /**
     * Set insuranceProgrammPrintName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $insuranceProgrammPrintName
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setInsuranceProgrammPrintName($insuranceProgrammPrintName = null)
    {
        // validation for constraint: string
        if (!is_null($insuranceProgrammPrintName) && !is_string($insuranceProgrammPrintName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($insuranceProgrammPrintName, true), gettype($insuranceProgrammPrintName)), __LINE__);
        }
        if (is_null($insuranceProgrammPrintName) || (is_array($insuranceProgrammPrintName) && empty($insuranceProgrammPrintName))) {
            unset($this->insuranceProgrammPrintName);
        } else {
            $this->insuranceProgrammPrintName = $insuranceProgrammPrintName;
        }
        return $this;
    }
    /**
     * Get insuranceProgrammUID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getInsuranceProgrammUID()
    {
        return isset($this->insuranceProgrammUID) ? $this->insuranceProgrammUID : null;
    }
    /**
     * Set insuranceProgrammUID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $insuranceProgrammUID
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setInsuranceProgrammUID($insuranceProgrammUID = null)
    {
        // validation for constraint: string
        if (!is_null($insuranceProgrammUID) && !is_string($insuranceProgrammUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($insuranceProgrammUID, true), gettype($insuranceProgrammUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($insuranceProgrammUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $insuranceProgrammUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($insuranceProgrammUID, true)), __LINE__);
        }
        if (is_null($insuranceProgrammUID) || (is_array($insuranceProgrammUID) && empty($insuranceProgrammUID))) {
            unset($this->insuranceProgrammUID);
        } else {
            $this->insuranceProgrammUID = $insuranceProgrammUID;
        }
        return $this;
    }
    /**
     * Get multiDays value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getMultiDays()
    {
        return isset($this->multiDays) ? $this->multiDays : null;
    }
    /**
     * Set multiDays value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $multiDays
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setMultiDays($multiDays = null)
    {
        // validation for constraint: int
        if (!is_null($multiDays) && !(is_int($multiDays) || ctype_digit($multiDays))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($multiDays, true), gettype($multiDays)), __LINE__);
        }
        if (is_null($multiDays) || (is_array($multiDays) && empty($multiDays))) {
            unset($this->multiDays);
        } else {
            $this->multiDays = $multiDays;
        }
        return $this;
    }
    /**
     * Get nettoPremRUR value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getNettoPremRUR()
    {
        return isset($this->nettoPremRUR) ? $this->nettoPremRUR : null;
    }
    /**
     * Set nettoPremRUR value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $nettoPremRUR
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setNettoPremRUR($nettoPremRUR = null)
    {
        // validation for constraint: float
        if (!is_null($nettoPremRUR) && !(is_float($nettoPremRUR) || is_numeric($nettoPremRUR))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($nettoPremRUR, true), gettype($nettoPremRUR)), __LINE__);
        }
        if (is_null($nettoPremRUR) || (is_array($nettoPremRUR) && empty($nettoPremRUR))) {
            unset($this->nettoPremRUR);
        } else {
            $this->nettoPremRUR = $nettoPremRUR;
        }
        return $this;
    }
    /**
     * Get operation value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOperation()
    {
        return isset($this->operation) ? $this->operation : null;
    }
    /**
     * Set operation value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $operation
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setOperation($operation = null)
    {
        // validation for constraint: string
        if (!is_null($operation) && !is_string($operation)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($operation, true), gettype($operation)), __LINE__);
        }
        if (is_null($operation) || (is_array($operation) && empty($operation))) {
            unset($this->operation);
        } else {
            $this->operation = $operation;
        }
        return $this;
    }
    /**
     * Get plicyConfirmDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPlicyConfirmDate()
    {
        return isset($this->plicyConfirmDate) ? $this->plicyConfirmDate : null;
    }
    /**
     * Set plicyConfirmDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $plicyConfirmDate
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setPlicyConfirmDate($plicyConfirmDate = null)
    {
        // validation for constraint: string
        if (!is_null($plicyConfirmDate) && !is_string($plicyConfirmDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($plicyConfirmDate, true), gettype($plicyConfirmDate)), __LINE__);
        }
        if (is_null($plicyConfirmDate) || (is_array($plicyConfirmDate) && empty($plicyConfirmDate))) {
            unset($this->plicyConfirmDate);
        } else {
            $this->plicyConfirmDate = $plicyConfirmDate;
        }
        return $this;
    }
    /**
     * Get policyID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPolicyID()
    {
        return isset($this->policyID) ? $this->policyID : null;
    }
    /**
     * Set policyID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $policyID
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setPolicyID($policyID = null)
    {
        // validation for constraint: int
        if (!is_null($policyID) && !(is_int($policyID) || ctype_digit($policyID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($policyID, true), gettype($policyID)), __LINE__);
        }
        if (is_null($policyID) || (is_array($policyID) && empty($policyID))) {
            unset($this->policyID);
        } else {
            $this->policyID = $policyID;
        }
        return $this;
    }
    /**
     * Get policyLink value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPolicyLink()
    {
        return isset($this->policyLink) ? $this->policyLink : null;
    }
    /**
     * Set policyLink value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $policyLink
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setPolicyLink($policyLink = null)
    {
        // validation for constraint: string
        if (!is_null($policyLink) && !is_string($policyLink)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($policyLink, true), gettype($policyLink)), __LINE__);
        }
        if (is_null($policyLink) || (is_array($policyLink) && empty($policyLink))) {
            unset($this->policyLink);
        } else {
            $this->policyLink = $policyLink;
        }
        return $this;
    }
    /**
     * Get policyNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPolicyNumber()
    {
        return isset($this->policyNumber) ? $this->policyNumber : null;
    }
    /**
     * Set policyNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $policyNumber
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setPolicyNumber($policyNumber = null)
    {
        // validation for constraint: string
        if (!is_null($policyNumber) && !is_string($policyNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($policyNumber, true), gettype($policyNumber)), __LINE__);
        }
        if (is_null($policyNumber) || (is_array($policyNumber) && empty($policyNumber))) {
            unset($this->policyNumber);
        } else {
            $this->policyNumber = $policyNumber;
        }
        return $this;
    }
    /**
     * Get policyPeriodFrom value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPolicyPeriodFrom()
    {
        return isset($this->policyPeriodFrom) ? $this->policyPeriodFrom : null;
    }
    /**
     * Set policyPeriodFrom value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $policyPeriodFrom
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setPolicyPeriodFrom($policyPeriodFrom = null)
    {
        // validation for constraint: string
        if (!is_null($policyPeriodFrom) && !is_string($policyPeriodFrom)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($policyPeriodFrom, true), gettype($policyPeriodFrom)), __LINE__);
        }
        if (is_null($policyPeriodFrom) || (is_array($policyPeriodFrom) && empty($policyPeriodFrom))) {
            unset($this->policyPeriodFrom);
        } else {
            $this->policyPeriodFrom = $policyPeriodFrom;
        }
        return $this;
    }
    /**
     * Get policyPeriodTill value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPolicyPeriodTill()
    {
        return isset($this->policyPeriodTill) ? $this->policyPeriodTill : null;
    }
    /**
     * Set policyPeriodTill value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $policyPeriodTill
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setPolicyPeriodTill($policyPeriodTill = null)
    {
        // validation for constraint: string
        if (!is_null($policyPeriodTill) && !is_string($policyPeriodTill)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($policyPeriodTill, true), gettype($policyPeriodTill)), __LINE__);
        }
        if (is_null($policyPeriodTill) || (is_array($policyPeriodTill) && empty($policyPeriodTill))) {
            unset($this->policyPeriodTill);
        } else {
            $this->policyPeriodTill = $policyPeriodTill;
        }
        return $this;
    }
    /**
     * Get policyPlace value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPolicyPlace()
    {
        return isset($this->policyPlace) ? $this->policyPlace : null;
    }
    /**
     * Set policyPlace value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $policyPlace
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setPolicyPlace($policyPlace = null)
    {
        // validation for constraint: string
        if (!is_null($policyPlace) && !is_string($policyPlace)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($policyPlace, true), gettype($policyPlace)), __LINE__);
        }
        if (is_null($policyPlace) || (is_array($policyPlace) && empty($policyPlace))) {
            unset($this->policyPlace);
        } else {
            $this->policyPlace = $policyPlace;
        }
        return $this;
    }
    /**
     * Get policyStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPolicyStatus()
    {
        return isset($this->policyStatus) ? $this->policyStatus : null;
    }
    /**
     * Set policyStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $policyStatus
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setPolicyStatus($policyStatus = null)
    {
        // validation for constraint: string
        if (!is_null($policyStatus) && !is_string($policyStatus)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($policyStatus, true), gettype($policyStatus)), __LINE__);
        }
        if (is_null($policyStatus) || (is_array($policyStatus) && empty($policyStatus))) {
            unset($this->policyStatus);
        } else {
            $this->policyStatus = $policyStatus;
        }
        return $this;
    }
    /**
     * Get policyUID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPolicyUID()
    {
        return isset($this->policyUID) ? $this->policyUID : null;
    }
    /**
     * Set policyUID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $policyUID
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setPolicyUID($policyUID = null)
    {
        // validation for constraint: string
        if (!is_null($policyUID) && !is_string($policyUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($policyUID, true), gettype($policyUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($policyUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $policyUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($policyUID, true)), __LINE__);
        }
        if (is_null($policyUID) || (is_array($policyUID) && empty($policyUID))) {
            unset($this->policyUID);
        } else {
            $this->policyUID = $policyUID;
        }
        return $this;
    }
    /**
     * Get premRUR value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getPremRUR()
    {
        return isset($this->premRUR) ? $this->premRUR : null;
    }
    /**
     * Set premRUR value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $premRUR
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setPremRUR($premRUR = null)
    {
        // validation for constraint: float
        if (!is_null($premRUR) && !(is_float($premRUR) || is_numeric($premRUR))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($premRUR, true), gettype($premRUR)), __LINE__);
        }
        if (is_null($premRUR) || (is_array($premRUR) && empty($premRUR))) {
            unset($this->premRUR);
        } else {
            $this->premRUR = $premRUR;
        }
        return $this;
    }
    /**
     * Get territory value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTerritory()
    {
        return isset($this->territory) ? $this->territory : null;
    }
    /**
     * Set territory value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $territory
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setTerritory($territory = null)
    {
        // validation for constraint: string
        if (!is_null($territory) && !is_string($territory)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($territory, true), gettype($territory)), __LINE__);
        }
        if (is_null($territory) || (is_array($territory) && empty($territory))) {
            unset($this->territory);
        } else {
            $this->territory = $territory;
        }
        return $this;
    }
    /**
     * Get territoryENG value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTerritoryENG()
    {
        return isset($this->territoryENG) ? $this->territoryENG : null;
    }
    /**
     * Set territoryENG value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $territoryENG
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setTerritoryENG($territoryENG = null)
    {
        // validation for constraint: string
        if (!is_null($territoryENG) && !is_string($territoryENG)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($territoryENG, true), gettype($territoryENG)), __LINE__);
        }
        if (is_null($territoryENG) || (is_array($territoryENG) && empty($territoryENG))) {
            unset($this->territoryENG);
        } else {
            $this->territoryENG = $territoryENG;
        }
        return $this;
    }
    /**
     * Get territoryRUR value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTerritoryRUR()
    {
        return isset($this->territoryRUR) ? $this->territoryRUR : null;
    }
    /**
     * Set territoryRUR value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $territoryRUR
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setTerritoryRUR($territoryRUR = null)
    {
        // validation for constraint: string
        if (!is_null($territoryRUR) && !is_string($territoryRUR)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($territoryRUR, true), gettype($territoryRUR)), __LINE__);
        }
        if (is_null($territoryRUR) || (is_array($territoryRUR) && empty($territoryRUR))) {
            unset($this->territoryRUR);
        } else {
            $this->territoryRUR = $territoryRUR;
        }
        return $this;
    }
    /**
     * Get usdRate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getUsdRate()
    {
        return isset($this->usdRate) ? $this->usdRate : null;
    }
    /**
     * Set usdRate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $usdRate
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setUsdRate($usdRate = null)
    {
        // validation for constraint: float
        if (!is_null($usdRate) && !(is_float($usdRate) || is_numeric($usdRate))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($usdRate, true), gettype($usdRate)), __LINE__);
        }
        if (is_null($usdRate) || (is_array($usdRate) && empty($usdRate))) {
            unset($this->usdRate);
        } else {
            $this->usdRate = $usdRate;
        }
        return $this;
    }
    /**
     * Get userId value
     * @return string|null
     */
    public function getUserId()
    {
        return $this->userId;
    }
    /**
     * Set userId value
     * @param string $userId
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setUserId($userId = null)
    {
        // validation for constraint: string
        if (!is_null($userId) && !is_string($userId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userId, true), gettype($userId)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($userId) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $userId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($userId, true)), __LINE__);
        }
        $this->userId = $userId;
        return $this;
    }
    /**
     * Get usersLogin value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsersLogin()
    {
        return isset($this->usersLogin) ? $this->usersLogin : null;
    }
    /**
     * Set usersLogin value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $usersLogin
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setUsersLogin($usersLogin = null)
    {
        // validation for constraint: string
        if (!is_null($usersLogin) && !is_string($usersLogin)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($usersLogin, true), gettype($usersLogin)), __LINE__);
        }
        if (is_null($usersLogin) || (is_array($usersLogin) && empty($usersLogin))) {
            unset($this->usersLogin);
        } else {
            $this->usersLogin = $usersLogin;
        }
        return $this;
    }
    /**
     * Get usersName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsersName()
    {
        return isset($this->usersName) ? $this->usersName : null;
    }
    /**
     * Set usersName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $usersName
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public function setUsersName($usersName = null)
    {
        // validation for constraint: string
        if (!is_null($usersName) && !is_string($usersName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($usersName, true), gettype($usersName)), __LINE__);
        }
        if (is_null($usersName) || (is_array($usersName) && empty($usersName))) {
            unset($this->usersName);
        } else {
            $this->usersName = $usersName;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\Common
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
