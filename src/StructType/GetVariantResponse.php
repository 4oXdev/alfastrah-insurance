<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetVariantResponse StructType
 * @subpackage Structs
 */
class GetVariantResponse extends AbstractStructBase
{
    /**
     * The GetVariantResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\ArrayType\ArrayOfvariant
     */
    public $GetVariantResult;
    /**
     * Constructor method for GetVariantResponse
     * @uses GetVariantResponse::setGetVariantResult()
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfvariant $getVariantResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfvariant $getVariantResult = null)
    {
        $this
            ->setGetVariantResult($getVariantResult);
    }
    /**
     * Get GetVariantResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfvariant|null
     */
    public function getGetVariantResult()
    {
        return isset($this->GetVariantResult) ? $this->GetVariantResult : null;
    }
    /**
     * Set GetVariantResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfvariant $getVariantResult
     * @return \RFD\AlfaStrahInsurance\StructType\GetVariantResponse
     */
    public function setGetVariantResult(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfvariant $getVariantResult = null)
    {
        if (is_null($getVariantResult) || (is_array($getVariantResult) && empty($getVariantResult))) {
            unset($this->GetVariantResult);
        } else {
            $this->GetVariantResult = $getVariantResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetVariantResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
