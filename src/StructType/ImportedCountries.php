<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for importedCountries StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q109:importedCountries
 * @subpackage Structs
 */
class ImportedCountries extends AbstractStructBase
{
    /**
     * The country
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\ImportedPolicyCountry[]
     */
    public $country;
    /**
     * Constructor method for importedCountries
     * @uses ImportedCountries::setCountry()
     * @param \RFD\AlfaStrahInsurance\StructType\ImportedPolicyCountry[] $country
     */
    public function __construct(array $country = array())
    {
        $this
            ->setCountry($country);
    }
    /**
     * Get country value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedPolicyCountry[]|null
     */
    public function getCountry()
    {
        return isset($this->country) ? $this->country : null;
    }
    /**
     * This method is responsible for validating the values passed to the setCountry method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCountry method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCountryForArrayConstraintsFromSetCountry(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $importedCountriesCountryItem) {
            // validation for constraint: itemType
            if (!$importedCountriesCountryItem instanceof \RFD\AlfaStrahInsurance\StructType\ImportedPolicyCountry) {
                $invalidValues[] = is_object($importedCountriesCountryItem) ? get_class($importedCountriesCountryItem) : sprintf('%s(%s)', gettype($importedCountriesCountryItem), var_export($importedCountriesCountryItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The country property can only contain items of type \RFD\AlfaStrahInsurance\StructType\ImportedPolicyCountry, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set country value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\ImportedPolicyCountry[] $country
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCountries
     */
    public function setCountry(array $country = array())
    {
        // validation for constraint: array
        if ('' !== ($countryArrayErrorMessage = self::validateCountryForArrayConstraintsFromSetCountry($country))) {
            throw new \InvalidArgumentException($countryArrayErrorMessage, __LINE__);
        }
        if (is_null($country) || (is_array($country) && empty($country))) {
            unset($this->country);
        } else {
            $this->country = $country;
        }
        return $this;
    }
    /**
     * Add item to country value
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\ImportedPolicyCountry $item
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCountries
     */
    public function addToCountry(\RFD\AlfaStrahInsurance\StructType\ImportedPolicyCountry $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \RFD\AlfaStrahInsurance\StructType\ImportedPolicyCountry) {
            throw new \InvalidArgumentException(sprintf('The country property can only contain items of type \RFD\AlfaStrahInsurance\StructType\ImportedPolicyCountry, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->country[] = $item;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCountries
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
