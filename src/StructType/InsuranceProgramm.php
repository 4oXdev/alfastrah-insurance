<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for insuranceProgramm StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q50:insuranceProgramm
 * @subpackage Structs
 */
class InsuranceProgramm extends BaseDictionary
{
    /**
     * The insuranceProgrammID
     * @var int
     */
    public $insuranceProgrammID;
    /**
     * The insuranceProgrammName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $insuranceProgrammName;
    /**
     * The insuranceProgrammPrintName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $insuranceProgrammPrintName;
    /**
     * The insuranceProgrammUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $insuranceProgrammUID;
    /**
     * Constructor method for insuranceProgramm
     * @uses InsuranceProgramm::setInsuranceProgrammID()
     * @uses InsuranceProgramm::setInsuranceProgrammName()
     * @uses InsuranceProgramm::setInsuranceProgrammPrintName()
     * @uses InsuranceProgramm::setInsuranceProgrammUID()
     * @param int $insuranceProgrammID
     * @param string $insuranceProgrammName
     * @param string $insuranceProgrammPrintName
     * @param string $insuranceProgrammUID
     */
    public function __construct($insuranceProgrammID = null, $insuranceProgrammName = null, $insuranceProgrammPrintName = null, $insuranceProgrammUID = null)
    {
        $this
            ->setInsuranceProgrammID($insuranceProgrammID)
            ->setInsuranceProgrammName($insuranceProgrammName)
            ->setInsuranceProgrammPrintName($insuranceProgrammPrintName)
            ->setInsuranceProgrammUID($insuranceProgrammUID);
    }
    /**
     * Get insuranceProgrammID value
     * @return int|null
     */
    public function getInsuranceProgrammID()
    {
        return $this->insuranceProgrammID;
    }
    /**
     * Set insuranceProgrammID value
     * @param int $insuranceProgrammID
     * @return \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm
     */
    public function setInsuranceProgrammID($insuranceProgrammID = null)
    {
        // validation for constraint: int
        if (!is_null($insuranceProgrammID) && !(is_int($insuranceProgrammID) || ctype_digit($insuranceProgrammID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($insuranceProgrammID, true), gettype($insuranceProgrammID)), __LINE__);
        }
        $this->insuranceProgrammID = $insuranceProgrammID;
        return $this;
    }
    /**
     * Get insuranceProgrammName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getInsuranceProgrammName()
    {
        return isset($this->insuranceProgrammName) ? $this->insuranceProgrammName : null;
    }
    /**
     * Set insuranceProgrammName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $insuranceProgrammName
     * @return \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm
     */
    public function setInsuranceProgrammName($insuranceProgrammName = null)
    {
        // validation for constraint: string
        if (!is_null($insuranceProgrammName) && !is_string($insuranceProgrammName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($insuranceProgrammName, true), gettype($insuranceProgrammName)), __LINE__);
        }
        if (is_null($insuranceProgrammName) || (is_array($insuranceProgrammName) && empty($insuranceProgrammName))) {
            unset($this->insuranceProgrammName);
        } else {
            $this->insuranceProgrammName = $insuranceProgrammName;
        }
        return $this;
    }
    /**
     * Get insuranceProgrammPrintName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getInsuranceProgrammPrintName()
    {
        return isset($this->insuranceProgrammPrintName) ? $this->insuranceProgrammPrintName : null;
    }
    /**
     * Set insuranceProgrammPrintName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $insuranceProgrammPrintName
     * @return \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm
     */
    public function setInsuranceProgrammPrintName($insuranceProgrammPrintName = null)
    {
        // validation for constraint: string
        if (!is_null($insuranceProgrammPrintName) && !is_string($insuranceProgrammPrintName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($insuranceProgrammPrintName, true), gettype($insuranceProgrammPrintName)), __LINE__);
        }
        if (is_null($insuranceProgrammPrintName) || (is_array($insuranceProgrammPrintName) && empty($insuranceProgrammPrintName))) {
            unset($this->insuranceProgrammPrintName);
        } else {
            $this->insuranceProgrammPrintName = $insuranceProgrammPrintName;
        }
        return $this;
    }
    /**
     * Get insuranceProgrammUID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getInsuranceProgrammUID()
    {
        return isset($this->insuranceProgrammUID) ? $this->insuranceProgrammUID : null;
    }
    /**
     * Set insuranceProgrammUID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $insuranceProgrammUID
     * @return \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm
     */
    public function setInsuranceProgrammUID($insuranceProgrammUID = null)
    {
        // validation for constraint: string
        if (!is_null($insuranceProgrammUID) && !is_string($insuranceProgrammUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($insuranceProgrammUID, true), gettype($insuranceProgrammUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($insuranceProgrammUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $insuranceProgrammUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($insuranceProgrammUID, true)), __LINE__);
        }
        if (is_null($insuranceProgrammUID) || (is_array($insuranceProgrammUID) && empty($insuranceProgrammUID))) {
            unset($this->insuranceProgrammUID);
        } else {
            $this->insuranceProgrammUID = $insuranceProgrammUID;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\InsuranceProgramm
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
