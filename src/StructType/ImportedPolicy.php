<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for importedPolicy StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q106:importedPolicy
 * @subpackage Structs
 */
class ImportedPolicy extends AbstractStructBase
{
    /**
     * The additionalConditions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\AdditionalConditions
     */
    public $additionalConditions;
    /**
     * The common
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public $common;
    /**
     * The countries
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\ImportedCountries
     */
    public $countries;
    /**
     * The countryUIDs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\CountryUIDs
     */
    public $countryUIDs;
    /**
     * The insureds
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\ImportedInsureds
     */
    public $insureds;
    /**
     * The risks
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\ImportedRisks
     */
    public $risks;
    /**
     * Constructor method for importedPolicy
     * @uses ImportedPolicy::setAdditionalConditions()
     * @uses ImportedPolicy::setCommon()
     * @uses ImportedPolicy::setCountries()
     * @uses ImportedPolicy::setCountryUIDs()
     * @uses ImportedPolicy::setInsureds()
     * @uses ImportedPolicy::setRisks()
     * @param \RFD\AlfaStrahInsurance\StructType\AdditionalConditions $additionalConditions
     * @param \RFD\AlfaStrahInsurance\StructType\ImportedCommon $common
     * @param \RFD\AlfaStrahInsurance\StructType\ImportedCountries $countries
     * @param \RFD\AlfaStrahInsurance\StructType\CountryUIDs $countryUIDs
     * @param \RFD\AlfaStrahInsurance\StructType\ImportedInsureds $insureds
     * @param \RFD\AlfaStrahInsurance\StructType\ImportedRisks $risks
     */
    public function __construct(\RFD\AlfaStrahInsurance\StructType\AdditionalConditions $additionalConditions = null, \RFD\AlfaStrahInsurance\StructType\ImportedCommon $common = null, \RFD\AlfaStrahInsurance\StructType\ImportedCountries $countries = null, \RFD\AlfaStrahInsurance\StructType\CountryUIDs $countryUIDs = null, \RFD\AlfaStrahInsurance\StructType\ImportedInsureds $insureds = null, \RFD\AlfaStrahInsurance\StructType\ImportedRisks $risks = null)
    {
        $this
            ->setAdditionalConditions($additionalConditions)
            ->setCommon($common)
            ->setCountries($countries)
            ->setCountryUIDs($countryUIDs)
            ->setInsureds($insureds)
            ->setRisks($risks);
    }
    /**
     * Get additionalConditions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalConditions|null
     */
    public function getAdditionalConditions()
    {
        return isset($this->additionalConditions) ? $this->additionalConditions : null;
    }
    /**
     * Set additionalConditions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\AdditionalConditions $additionalConditions
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedPolicy
     */
    public function setAdditionalConditions(\RFD\AlfaStrahInsurance\StructType\AdditionalConditions $additionalConditions = null)
    {
        if (is_null($additionalConditions) || (is_array($additionalConditions) && empty($additionalConditions))) {
            unset($this->additionalConditions);
        } else {
            $this->additionalConditions = $additionalConditions;
        }
        return $this;
    }
    /**
     * Get common value
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon|null
     */
    public function getCommon()
    {
        return $this->common;
    }
    /**
     * Set common value
     * @param \RFD\AlfaStrahInsurance\StructType\ImportedCommon $common
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedPolicy
     */
    public function setCommon(\RFD\AlfaStrahInsurance\StructType\ImportedCommon $common = null)
    {
        $this->common = $common;
        return $this;
    }
    /**
     * Get countries value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCountries|null
     */
    public function getCountries()
    {
        return isset($this->countries) ? $this->countries : null;
    }
    /**
     * Set countries value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\ImportedCountries $countries
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedPolicy
     */
    public function setCountries(\RFD\AlfaStrahInsurance\StructType\ImportedCountries $countries = null)
    {
        if (is_null($countries) || (is_array($countries) && empty($countries))) {
            unset($this->countries);
        } else {
            $this->countries = $countries;
        }
        return $this;
    }
    /**
     * Get countryUIDs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\CountryUIDs|null
     */
    public function getCountryUIDs()
    {
        return isset($this->countryUIDs) ? $this->countryUIDs : null;
    }
    /**
     * Set countryUIDs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\CountryUIDs $countryUIDs
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedPolicy
     */
    public function setCountryUIDs(\RFD\AlfaStrahInsurance\StructType\CountryUIDs $countryUIDs = null)
    {
        if (is_null($countryUIDs) || (is_array($countryUIDs) && empty($countryUIDs))) {
            unset($this->countryUIDs);
        } else {
            $this->countryUIDs = $countryUIDs;
        }
        return $this;
    }
    /**
     * Get insureds value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedInsureds|null
     */
    public function getInsureds()
    {
        return isset($this->insureds) ? $this->insureds : null;
    }
    /**
     * Set insureds value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\ImportedInsureds $insureds
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedPolicy
     */
    public function setInsureds(\RFD\AlfaStrahInsurance\StructType\ImportedInsureds $insureds = null)
    {
        if (is_null($insureds) || (is_array($insureds) && empty($insureds))) {
            unset($this->insureds);
        } else {
            $this->insureds = $insureds;
        }
        return $this;
    }
    /**
     * Get risks value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedRisks|null
     */
    public function getRisks()
    {
        return isset($this->risks) ? $this->risks : null;
    }
    /**
     * Set risks value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\ImportedRisks $risks
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedPolicy
     */
    public function setRisks(\RFD\AlfaStrahInsurance\StructType\ImportedRisks $risks = null)
    {
        if (is_null($risks) || (is_array($risks) && empty($risks))) {
            unset($this->risks);
        } else {
            $this->risks = $risks;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedPolicy
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
