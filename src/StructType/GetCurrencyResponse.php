<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCurrencyResponse StructType
 * @subpackage Structs
 */
class GetCurrencyResponse extends AbstractStructBase
{
    /**
     * The GetCurrencyResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcurrency
     */
    public $GetCurrencyResult;
    /**
     * Constructor method for GetCurrencyResponse
     * @uses GetCurrencyResponse::setGetCurrencyResult()
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcurrency $getCurrencyResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfcurrency $getCurrencyResult = null)
    {
        $this
            ->setGetCurrencyResult($getCurrencyResult);
    }
    /**
     * Get GetCurrencyResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcurrency|null
     */
    public function getGetCurrencyResult()
    {
        return isset($this->GetCurrencyResult) ? $this->GetCurrencyResult : null;
    }
    /**
     * Set GetCurrencyResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcurrency $getCurrencyResult
     * @return \RFD\AlfaStrahInsurance\StructType\GetCurrencyResponse
     */
    public function setGetCurrencyResult(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfcurrency $getCurrencyResult = null)
    {
        if (is_null($getCurrencyResult) || (is_array($getCurrencyResult) && empty($getCurrencyResult))) {
            unset($this->GetCurrencyResult);
        } else {
            $this->GetCurrencyResult = $getCurrencyResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetCurrencyResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
