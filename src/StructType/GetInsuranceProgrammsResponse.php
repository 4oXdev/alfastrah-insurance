<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetInsuranceProgrammsResponse StructType
 * @subpackage Structs
 */
class GetInsuranceProgrammsResponse extends AbstractStructBase
{
    /**
     * The GetInsuranceProgrammsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\ArrayType\ArrayOfinsuranceProgramm
     */
    public $GetInsuranceProgrammsResult;
    /**
     * Constructor method for GetInsuranceProgrammsResponse
     * @uses GetInsuranceProgrammsResponse::setGetInsuranceProgrammsResult()
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfinsuranceProgramm $getInsuranceProgrammsResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfinsuranceProgramm $getInsuranceProgrammsResult = null)
    {
        $this
            ->setGetInsuranceProgrammsResult($getInsuranceProgrammsResult);
    }
    /**
     * Get GetInsuranceProgrammsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfinsuranceProgramm|null
     */
    public function getGetInsuranceProgrammsResult()
    {
        return isset($this->GetInsuranceProgrammsResult) ? $this->GetInsuranceProgrammsResult : null;
    }
    /**
     * Set GetInsuranceProgrammsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfinsuranceProgramm $getInsuranceProgrammsResult
     * @return \RFD\AlfaStrahInsurance\StructType\GetInsuranceProgrammsResponse
     */
    public function setGetInsuranceProgrammsResult(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfinsuranceProgramm $getInsuranceProgrammsResult = null)
    {
        if (is_null($getInsuranceProgrammsResult) || (is_array($getInsuranceProgrammsResult) && empty($getInsuranceProgrammsResult))) {
            unset($this->GetInsuranceProgrammsResult);
        } else {
            $this->GetInsuranceProgrammsResult = $getInsuranceProgrammsResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetInsuranceProgrammsResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
