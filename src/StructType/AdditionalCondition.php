<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for additionalCondition StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q9:additionalCondition
 * @subpackage Structs
 */
class AdditionalCondition extends BaseDictionary
{
    /**
     * The additionalCondition
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $additionalCondition;
    /**
     * The additionalConditionID
     * @var int
     */
    public $additionalConditionID;
    /**
     * The additionalConditionUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $additionalConditionUID;
    /**
     * The additionalConditionValue
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $additionalConditionValue;
    /**
     * Constructor method for additionalCondition
     * @uses AdditionalCondition::setAdditionalCondition()
     * @uses AdditionalCondition::setAdditionalConditionID()
     * @uses AdditionalCondition::setAdditionalConditionUID()
     * @uses AdditionalCondition::setAdditionalConditionValue()
     * @param string $additionalCondition
     * @param int $additionalConditionID
     * @param string $additionalConditionUID
     * @param float $additionalConditionValue
     */
    public function __construct($additionalCondition = null, $additionalConditionID = null, $additionalConditionUID = null, $additionalConditionValue = null)
    {
        $this
            ->setAdditionalCondition($additionalCondition)
            ->setAdditionalConditionID($additionalConditionID)
            ->setAdditionalConditionUID($additionalConditionUID)
            ->setAdditionalConditionValue($additionalConditionValue);
    }
    /**
     * Get additionalCondition value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdditionalCondition()
    {
        return isset($this->additionalCondition) ? $this->additionalCondition : null;
    }
    /**
     * Set additionalCondition value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $additionalCondition
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalCondition
     */
    public function setAdditionalCondition($additionalCondition = null)
    {
        // validation for constraint: string
        if (!is_null($additionalCondition) && !is_string($additionalCondition)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalCondition, true), gettype($additionalCondition)), __LINE__);
        }
        if (is_null($additionalCondition) || (is_array($additionalCondition) && empty($additionalCondition))) {
            unset($this->additionalCondition);
        } else {
            $this->additionalCondition = $additionalCondition;
        }
        return $this;
    }
    /**
     * Get additionalConditionID value
     * @return int|null
     */
    public function getAdditionalConditionID()
    {
        return $this->additionalConditionID;
    }
    /**
     * Set additionalConditionID value
     * @param int $additionalConditionID
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalCondition
     */
    public function setAdditionalConditionID($additionalConditionID = null)
    {
        // validation for constraint: int
        if (!is_null($additionalConditionID) && !(is_int($additionalConditionID) || ctype_digit($additionalConditionID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($additionalConditionID, true), gettype($additionalConditionID)), __LINE__);
        }
        $this->additionalConditionID = $additionalConditionID;
        return $this;
    }
    /**
     * Get additionalConditionUID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdditionalConditionUID()
    {
        return isset($this->additionalConditionUID) ? $this->additionalConditionUID : null;
    }
    /**
     * Set additionalConditionUID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $additionalConditionUID
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalCondition
     */
    public function setAdditionalConditionUID($additionalConditionUID = null)
    {
        // validation for constraint: string
        if (!is_null($additionalConditionUID) && !is_string($additionalConditionUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalConditionUID, true), gettype($additionalConditionUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($additionalConditionUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $additionalConditionUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($additionalConditionUID, true)), __LINE__);
        }
        if (is_null($additionalConditionUID) || (is_array($additionalConditionUID) && empty($additionalConditionUID))) {
            unset($this->additionalConditionUID);
        } else {
            $this->additionalConditionUID = $additionalConditionUID;
        }
        return $this;
    }
    /**
     * Get additionalConditionValue value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getAdditionalConditionValue()
    {
        return isset($this->additionalConditionValue) ? $this->additionalConditionValue : null;
    }
    /**
     * Set additionalConditionValue value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $additionalConditionValue
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalCondition
     */
    public function setAdditionalConditionValue($additionalConditionValue = null)
    {
        // validation for constraint: float
        if (!is_null($additionalConditionValue) && !(is_float($additionalConditionValue) || is_numeric($additionalConditionValue))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($additionalConditionValue, true), gettype($additionalConditionValue)), __LINE__);
        }
        if (is_null($additionalConditionValue) || (is_array($additionalConditionValue) && empty($additionalConditionValue))) {
            unset($this->additionalConditionValue);
        } else {
            $this->additionalConditionValue = $additionalConditionValue;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalCondition
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
