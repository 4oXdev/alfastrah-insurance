<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SetAcceptPolicyResponse StructType
 * @subpackage Structs
 */
class SetAcceptPolicyResponse extends AbstractStructBase
{
    /**
     * The SetAcceptPolicyResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public $SetAcceptPolicyResult;
    /**
     * Constructor method for SetAcceptPolicyResponse
     * @uses SetAcceptPolicyResponse::setSetAcceptPolicyResult()
     * @param \RFD\AlfaStrahInsurance\StructType\Policy $setAcceptPolicyResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\StructType\Policy $setAcceptPolicyResult = null)
    {
        $this
            ->setSetAcceptPolicyResult($setAcceptPolicyResult);
    }
    /**
     * Get SetAcceptPolicyResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Policy|null
     */
    public function getSetAcceptPolicyResult()
    {
        return isset($this->SetAcceptPolicyResult) ? $this->SetAcceptPolicyResult : null;
    }
    /**
     * Set SetAcceptPolicyResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\Policy $setAcceptPolicyResult
     * @return \RFD\AlfaStrahInsurance\StructType\SetAcceptPolicyResponse
     */
    public function setSetAcceptPolicyResult(\RFD\AlfaStrahInsurance\StructType\Policy $setAcceptPolicyResult = null)
    {
        if (is_null($setAcceptPolicyResult) || (is_array($setAcceptPolicyResult) && empty($setAcceptPolicyResult))) {
            unset($this->SetAcceptPolicyResult);
        } else {
            $this->SetAcceptPolicyResult = $setAcceptPolicyResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\SetAcceptPolicyResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
