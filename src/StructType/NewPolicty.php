<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for NewPolicty StructType
 * @subpackage Structs
 */
class NewPolicty extends AbstractStructBase
{
    /**
     * The policy
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\ImportedPolicy
     */
    public $policy;
    /**
     * Constructor method for NewPolicty
     * @uses NewPolicty::setPolicy()
     * @param \RFD\AlfaStrahInsurance\StructType\ImportedPolicy $policy
     */
    public function __construct(\RFD\AlfaStrahInsurance\StructType\ImportedPolicy $policy = null)
    {
        $this
            ->setPolicy($policy);
    }
    /**
     * Get policy value
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedPolicy
     */
    public function getPolicy()
    {
        return $this->policy;
    }
    /**
     * Set policy value
     * @param \RFD\AlfaStrahInsurance\StructType\ImportedPolicy $policy
     * @return \RFD\AlfaStrahInsurance\StructType\NewPolicty
     */
    public function setPolicy(\RFD\AlfaStrahInsurance\StructType\ImportedPolicy $policy = null)
    {
        $this->policy = $policy;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\NewPolicty
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
