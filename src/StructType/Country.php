<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for country StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q24:country
 * @subpackage Structs
 */
class Country extends BaseDictionary
{
    /**
     * The assistanceCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $assistanceCode;
    /**
     * The assistanceName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $assistanceName;
    /**
     * The assistancePhones
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $assistancePhones;
    /**
     * The assistanteID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $assistanteID;
    /**
     * The assistanteUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $assistanteUID;
    /**
     * The countryID
     * @var int
     */
    public $countryID;
    /**
     * The countryKV
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $countryKV;
    /**
     * The countryName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $countryName;
    /**
     * The countryUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $countryUID;
    /**
     * The terName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $terName;
    /**
     * Constructor method for country
     * @uses Country::setAssistanceCode()
     * @uses Country::setAssistanceName()
     * @uses Country::setAssistancePhones()
     * @uses Country::setAssistanteID()
     * @uses Country::setAssistanteUID()
     * @uses Country::setCountryID()
     * @uses Country::setCountryKV()
     * @uses Country::setCountryName()
     * @uses Country::setCountryUID()
     * @uses Country::setTerName()
     * @param string $assistanceCode
     * @param string $assistanceName
     * @param string $assistancePhones
     * @param int $assistanteID
     * @param string $assistanteUID
     * @param int $countryID
     * @param float $countryKV
     * @param string $countryName
     * @param string $countryUID
     * @param string $terName
     */
    public function __construct($assistanceCode = null, $assistanceName = null, $assistancePhones = null, $assistanteID = null, $assistanteUID = null, $countryID = null, $countryKV = null, $countryName = null, $countryUID = null, $terName = null)
    {
        $this
            ->setAssistanceCode($assistanceCode)
            ->setAssistanceName($assistanceName)
            ->setAssistancePhones($assistancePhones)
            ->setAssistanteID($assistanteID)
            ->setAssistanteUID($assistanteUID)
            ->setCountryID($countryID)
            ->setCountryKV($countryKV)
            ->setCountryName($countryName)
            ->setCountryUID($countryUID)
            ->setTerName($terName);
    }
    /**
     * Get assistanceCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAssistanceCode()
    {
        return isset($this->assistanceCode) ? $this->assistanceCode : null;
    }
    /**
     * Set assistanceCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $assistanceCode
     * @return \RFD\AlfaStrahInsurance\StructType\Country
     */
    public function setAssistanceCode($assistanceCode = null)
    {
        // validation for constraint: string
        if (!is_null($assistanceCode) && !is_string($assistanceCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($assistanceCode, true), gettype($assistanceCode)), __LINE__);
        }
        if (is_null($assistanceCode) || (is_array($assistanceCode) && empty($assistanceCode))) {
            unset($this->assistanceCode);
        } else {
            $this->assistanceCode = $assistanceCode;
        }
        return $this;
    }
    /**
     * Get assistanceName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAssistanceName()
    {
        return isset($this->assistanceName) ? $this->assistanceName : null;
    }
    /**
     * Set assistanceName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $assistanceName
     * @return \RFD\AlfaStrahInsurance\StructType\Country
     */
    public function setAssistanceName($assistanceName = null)
    {
        // validation for constraint: string
        if (!is_null($assistanceName) && !is_string($assistanceName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($assistanceName, true), gettype($assistanceName)), __LINE__);
        }
        if (is_null($assistanceName) || (is_array($assistanceName) && empty($assistanceName))) {
            unset($this->assistanceName);
        } else {
            $this->assistanceName = $assistanceName;
        }
        return $this;
    }
    /**
     * Get assistancePhones value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAssistancePhones()
    {
        return isset($this->assistancePhones) ? $this->assistancePhones : null;
    }
    /**
     * Set assistancePhones value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $assistancePhones
     * @return \RFD\AlfaStrahInsurance\StructType\Country
     */
    public function setAssistancePhones($assistancePhones = null)
    {
        // validation for constraint: string
        if (!is_null($assistancePhones) && !is_string($assistancePhones)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($assistancePhones, true), gettype($assistancePhones)), __LINE__);
        }
        if (is_null($assistancePhones) || (is_array($assistancePhones) && empty($assistancePhones))) {
            unset($this->assistancePhones);
        } else {
            $this->assistancePhones = $assistancePhones;
        }
        return $this;
    }
    /**
     * Get assistanteID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getAssistanteID()
    {
        return isset($this->assistanteID) ? $this->assistanteID : null;
    }
    /**
     * Set assistanteID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $assistanteID
     * @return \RFD\AlfaStrahInsurance\StructType\Country
     */
    public function setAssistanteID($assistanteID = null)
    {
        // validation for constraint: int
        if (!is_null($assistanteID) && !(is_int($assistanteID) || ctype_digit($assistanteID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($assistanteID, true), gettype($assistanteID)), __LINE__);
        }
        if (is_null($assistanteID) || (is_array($assistanteID) && empty($assistanteID))) {
            unset($this->assistanteID);
        } else {
            $this->assistanteID = $assistanteID;
        }
        return $this;
    }
    /**
     * Get assistanteUID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAssistanteUID()
    {
        return isset($this->assistanteUID) ? $this->assistanteUID : null;
    }
    /**
     * Set assistanteUID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $assistanteUID
     * @return \RFD\AlfaStrahInsurance\StructType\Country
     */
    public function setAssistanteUID($assistanteUID = null)
    {
        // validation for constraint: string
        if (!is_null($assistanteUID) && !is_string($assistanteUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($assistanteUID, true), gettype($assistanteUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($assistanteUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $assistanteUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($assistanteUID, true)), __LINE__);
        }
        if (is_null($assistanteUID) || (is_array($assistanteUID) && empty($assistanteUID))) {
            unset($this->assistanteUID);
        } else {
            $this->assistanteUID = $assistanteUID;
        }
        return $this;
    }
    /**
     * Get countryID value
     * @return int|null
     */
    public function getCountryID()
    {
        return $this->countryID;
    }
    /**
     * Set countryID value
     * @param int $countryID
     * @return \RFD\AlfaStrahInsurance\StructType\Country
     */
    public function setCountryID($countryID = null)
    {
        // validation for constraint: int
        if (!is_null($countryID) && !(is_int($countryID) || ctype_digit($countryID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($countryID, true), gettype($countryID)), __LINE__);
        }
        $this->countryID = $countryID;
        return $this;
    }
    /**
     * Get countryKV value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getCountryKV()
    {
        return isset($this->countryKV) ? $this->countryKV : null;
    }
    /**
     * Set countryKV value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $countryKV
     * @return \RFD\AlfaStrahInsurance\StructType\Country
     */
    public function setCountryKV($countryKV = null)
    {
        // validation for constraint: float
        if (!is_null($countryKV) && !(is_float($countryKV) || is_numeric($countryKV))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($countryKV, true), gettype($countryKV)), __LINE__);
        }
        if (is_null($countryKV) || (is_array($countryKV) && empty($countryKV))) {
            unset($this->countryKV);
        } else {
            $this->countryKV = $countryKV;
        }
        return $this;
    }
    /**
     * Get countryName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryName()
    {
        return isset($this->countryName) ? $this->countryName : null;
    }
    /**
     * Set countryName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryName
     * @return \RFD\AlfaStrahInsurance\StructType\Country
     */
    public function setCountryName($countryName = null)
    {
        // validation for constraint: string
        if (!is_null($countryName) && !is_string($countryName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryName, true), gettype($countryName)), __LINE__);
        }
        if (is_null($countryName) || (is_array($countryName) && empty($countryName))) {
            unset($this->countryName);
        } else {
            $this->countryName = $countryName;
        }
        return $this;
    }
    /**
     * Get countryUID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryUID()
    {
        return isset($this->countryUID) ? $this->countryUID : null;
    }
    /**
     * Set countryUID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryUID
     * @return \RFD\AlfaStrahInsurance\StructType\Country
     */
    public function setCountryUID($countryUID = null)
    {
        // validation for constraint: string
        if (!is_null($countryUID) && !is_string($countryUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryUID, true), gettype($countryUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($countryUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $countryUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($countryUID, true)), __LINE__);
        }
        if (is_null($countryUID) || (is_array($countryUID) && empty($countryUID))) {
            unset($this->countryUID);
        } else {
            $this->countryUID = $countryUID;
        }
        return $this;
    }
    /**
     * Get terName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTerName()
    {
        return isset($this->terName) ? $this->terName : null;
    }
    /**
     * Set terName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $terName
     * @return \RFD\AlfaStrahInsurance\StructType\Country
     */
    public function setTerName($terName = null)
    {
        // validation for constraint: string
        if (!is_null($terName) && !is_string($terName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($terName, true), gettype($terName)), __LINE__);
        }
        if (is_null($terName) || (is_array($terName) && empty($terName))) {
            unset($this->terName);
        } else {
            $this->terName = $terName;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\Country
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
