<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for personEditParameters StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q129:personEditParameters
 * @subpackage Structs
 */
class PersonEditParameters extends AbstractStructBase
{
    /**
     * The agentLogin
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $agentLogin;
    /**
     * The agentPassword
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $agentPassword;
    /**
     * The agentUid
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $agentUid;
    /**
     * The dateOfBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $dateOfBirth;
    /**
     * The fio
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $fio;
    /**
     * The number
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $number;
    /**
     * The passport
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $passport;
    /**
     * The stId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $stId;
    /**
     * Constructor method for personEditParameters
     * @uses PersonEditParameters::setAgentLogin()
     * @uses PersonEditParameters::setAgentPassword()
     * @uses PersonEditParameters::setAgentUid()
     * @uses PersonEditParameters::setDateOfBirth()
     * @uses PersonEditParameters::setFio()
     * @uses PersonEditParameters::setNumber()
     * @uses PersonEditParameters::setPassport()
     * @uses PersonEditParameters::setStId()
     * @param string $agentLogin
     * @param string $agentPassword
     * @param string $agentUid
     * @param string $dateOfBirth
     * @param string $fio
     * @param string $number
     * @param string $passport
     * @param int $stId
     */
    public function __construct($agentLogin = null, $agentPassword = null, $agentUid = null, $dateOfBirth = null, $fio = null, $number = null, $passport = null, $stId = null)
    {
        $this
            ->setAgentLogin($agentLogin)
            ->setAgentPassword($agentPassword)
            ->setAgentUid($agentUid)
            ->setDateOfBirth($dateOfBirth)
            ->setFio($fio)
            ->setNumber($number)
            ->setPassport($passport)
            ->setStId($stId);
    }
    /**
     * Get agentLogin value
     * @return string|null
     */
    public function getAgentLogin()
    {
        return $this->agentLogin;
    }
    /**
     * Set agentLogin value
     * @param string $agentLogin
     * @return \RFD\AlfaStrahInsurance\StructType\PersonEditParameters
     */
    public function setAgentLogin($agentLogin = null)
    {
        // validation for constraint: string
        if (!is_null($agentLogin) && !is_string($agentLogin)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($agentLogin, true), gettype($agentLogin)), __LINE__);
        }
        $this->agentLogin = $agentLogin;
        return $this;
    }
    /**
     * Get agentPassword value
     * @return string|null
     */
    public function getAgentPassword()
    {
        return $this->agentPassword;
    }
    /**
     * Set agentPassword value
     * @param string $agentPassword
     * @return \RFD\AlfaStrahInsurance\StructType\PersonEditParameters
     */
    public function setAgentPassword($agentPassword = null)
    {
        // validation for constraint: string
        if (!is_null($agentPassword) && !is_string($agentPassword)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($agentPassword, true), gettype($agentPassword)), __LINE__);
        }
        $this->agentPassword = $agentPassword;
        return $this;
    }
    /**
     * Get agentUid value
     * @return string|null
     */
    public function getAgentUid()
    {
        return $this->agentUid;
    }
    /**
     * Set agentUid value
     * @param string $agentUid
     * @return \RFD\AlfaStrahInsurance\StructType\PersonEditParameters
     */
    public function setAgentUid($agentUid = null)
    {
        // validation for constraint: string
        if (!is_null($agentUid) && !is_string($agentUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($agentUid, true), gettype($agentUid)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($agentUid) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $agentUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($agentUid, true)), __LINE__);
        }
        $this->agentUid = $agentUid;
        return $this;
    }
    /**
     * Get dateOfBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDateOfBirth()
    {
        return isset($this->dateOfBirth) ? $this->dateOfBirth : null;
    }
    /**
     * Set dateOfBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dateOfBirth
     * @return \RFD\AlfaStrahInsurance\StructType\PersonEditParameters
     */
    public function setDateOfBirth($dateOfBirth = null)
    {
        // validation for constraint: string
        if (!is_null($dateOfBirth) && !is_string($dateOfBirth)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateOfBirth, true), gettype($dateOfBirth)), __LINE__);
        }
        if (is_null($dateOfBirth) || (is_array($dateOfBirth) && empty($dateOfBirth))) {
            unset($this->dateOfBirth);
        } else {
            $this->dateOfBirth = $dateOfBirth;
        }
        return $this;
    }
    /**
     * Get fio value
     * @return string|null
     */
    public function getFio()
    {
        return $this->fio;
    }
    /**
     * Set fio value
     * @param string $fio
     * @return \RFD\AlfaStrahInsurance\StructType\PersonEditParameters
     */
    public function setFio($fio = null)
    {
        // validation for constraint: string
        if (!is_null($fio) && !is_string($fio)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fio, true), gettype($fio)), __LINE__);
        }
        $this->fio = $fio;
        return $this;
    }
    /**
     * Get number value
     * @return string|null
     */
    public function getNumber()
    {
        return $this->number;
    }
    /**
     * Set number value
     * @param string $number
     * @return \RFD\AlfaStrahInsurance\StructType\PersonEditParameters
     */
    public function setNumber($number = null)
    {
        // validation for constraint: string
        if (!is_null($number) && !is_string($number)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($number, true), gettype($number)), __LINE__);
        }
        $this->number = $number;
        return $this;
    }
    /**
     * Get passport value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassport()
    {
        return isset($this->passport) ? $this->passport : null;
    }
    /**
     * Set passport value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $passport
     * @return \RFD\AlfaStrahInsurance\StructType\PersonEditParameters
     */
    public function setPassport($passport = null)
    {
        // validation for constraint: string
        if (!is_null($passport) && !is_string($passport)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($passport, true), gettype($passport)), __LINE__);
        }
        if (is_null($passport) || (is_array($passport) && empty($passport))) {
            unset($this->passport);
        } else {
            $this->passport = $passport;
        }
        return $this;
    }
    /**
     * Get stId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getStId()
    {
        return isset($this->stId) ? $this->stId : null;
    }
    /**
     * Set stId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $stId
     * @return \RFD\AlfaStrahInsurance\StructType\PersonEditParameters
     */
    public function setStId($stId = null)
    {
        // validation for constraint: int
        if (!is_null($stId) && !(is_int($stId) || ctype_digit($stId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($stId, true), gettype($stId)), __LINE__);
        }
        if (is_null($stId) || (is_array($stId) && empty($stId))) {
            unset($this->stId);
        } else {
            $this->stId = $stId;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\PersonEditParameters
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
