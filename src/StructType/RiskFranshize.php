<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for riskFranshize StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q62:riskFranshize
 * @subpackage Structs
 */
class RiskFranshize extends BaseDictionary
{
    /**
     * The Franshize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Franshize;
    /**
     * The risk
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $risk;
    /**
     * The riskID
     * @var int
     */
    public $riskID;
    /**
     * The riskPrintName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $riskPrintName;
    /**
     * The riskUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $riskUID;
    /**
     * Constructor method for riskFranshize
     * @uses RiskFranshize::setFranshize()
     * @uses RiskFranshize::setRisk()
     * @uses RiskFranshize::setRiskID()
     * @uses RiskFranshize::setRiskPrintName()
     * @uses RiskFranshize::setRiskUID()
     * @param string $franshize
     * @param string $risk
     * @param int $riskID
     * @param string $riskPrintName
     * @param string $riskUID
     */
    public function __construct($franshize = null, $risk = null, $riskID = null, $riskPrintName = null, $riskUID = null)
    {
        $this
            ->setFranshize($franshize)
            ->setRisk($risk)
            ->setRiskID($riskID)
            ->setRiskPrintName($riskPrintName)
            ->setRiskUID($riskUID);
    }
    /**
     * Get Franshize value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFranshize()
    {
        return isset($this->Franshize) ? $this->Franshize : null;
    }
    /**
     * Set Franshize value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $franshize
     * @return \RFD\AlfaStrahInsurance\StructType\RiskFranshize
     */
    public function setFranshize($franshize = null)
    {
        // validation for constraint: string
        if (!is_null($franshize) && !is_string($franshize)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($franshize, true), gettype($franshize)), __LINE__);
        }
        if (is_null($franshize) || (is_array($franshize) && empty($franshize))) {
            unset($this->Franshize);
        } else {
            $this->Franshize = $franshize;
        }
        return $this;
    }
    /**
     * Get risk value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRisk()
    {
        return isset($this->risk) ? $this->risk : null;
    }
    /**
     * Set risk value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $risk
     * @return \RFD\AlfaStrahInsurance\StructType\RiskFranshize
     */
    public function setRisk($risk = null)
    {
        // validation for constraint: string
        if (!is_null($risk) && !is_string($risk)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($risk, true), gettype($risk)), __LINE__);
        }
        if (is_null($risk) || (is_array($risk) && empty($risk))) {
            unset($this->risk);
        } else {
            $this->risk = $risk;
        }
        return $this;
    }
    /**
     * Get riskID value
     * @return int|null
     */
    public function getRiskID()
    {
        return $this->riskID;
    }
    /**
     * Set riskID value
     * @param int $riskID
     * @return \RFD\AlfaStrahInsurance\StructType\RiskFranshize
     */
    public function setRiskID($riskID = null)
    {
        // validation for constraint: int
        if (!is_null($riskID) && !(is_int($riskID) || ctype_digit($riskID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($riskID, true), gettype($riskID)), __LINE__);
        }
        $this->riskID = $riskID;
        return $this;
    }
    /**
     * Get riskPrintName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRiskPrintName()
    {
        return isset($this->riskPrintName) ? $this->riskPrintName : null;
    }
    /**
     * Set riskPrintName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $riskPrintName
     * @return \RFD\AlfaStrahInsurance\StructType\RiskFranshize
     */
    public function setRiskPrintName($riskPrintName = null)
    {
        // validation for constraint: string
        if (!is_null($riskPrintName) && !is_string($riskPrintName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($riskPrintName, true), gettype($riskPrintName)), __LINE__);
        }
        if (is_null($riskPrintName) || (is_array($riskPrintName) && empty($riskPrintName))) {
            unset($this->riskPrintName);
        } else {
            $this->riskPrintName = $riskPrintName;
        }
        return $this;
    }
    /**
     * Get riskUID value
     * @return string|null
     */
    public function getRiskUID()
    {
        return $this->riskUID;
    }
    /**
     * Set riskUID value
     * @param string $riskUID
     * @return \RFD\AlfaStrahInsurance\StructType\RiskFranshize
     */
    public function setRiskUID($riskUID = null)
    {
        // validation for constraint: string
        if (!is_null($riskUID) && !is_string($riskUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($riskUID, true), gettype($riskUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($riskUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $riskUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($riskUID, true)), __LINE__);
        }
        $this->riskUID = $riskUID;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\RiskFranshize
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
