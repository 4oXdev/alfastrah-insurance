<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getPoliciesParameters StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q70:getPoliciesParameters
 * @subpackage Structs
 */
class GetPoliciesParameters extends AbstractStructBase
{
    /**
     * The policyNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $policyNumber;
    /**
     * The policyPeriodFrom
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $policyPeriodFrom;
    /**
     * The policyPeriodTill
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $policyPeriodTill;
    /**
     * The userId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $userId;
    /**
     * The userLogin
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $userLogin;
    /**
     * The userPSW
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $userPSW;
    /**
     * Constructor method for getPoliciesParameters
     * @uses GetPoliciesParameters::setPolicyNumber()
     * @uses GetPoliciesParameters::setPolicyPeriodFrom()
     * @uses GetPoliciesParameters::setPolicyPeriodTill()
     * @uses GetPoliciesParameters::setUserId()
     * @uses GetPoliciesParameters::setUserLogin()
     * @uses GetPoliciesParameters::setUserPSW()
     * @param string $policyNumber
     * @param string $policyPeriodFrom
     * @param string $policyPeriodTill
     * @param string $userId
     * @param string $userLogin
     * @param string $userPSW
     */
    public function __construct($policyNumber = null, $policyPeriodFrom = null, $policyPeriodTill = null, $userId = null, $userLogin = null, $userPSW = null)
    {
        $this
            ->setPolicyNumber($policyNumber)
            ->setPolicyPeriodFrom($policyPeriodFrom)
            ->setPolicyPeriodTill($policyPeriodTill)
            ->setUserId($userId)
            ->setUserLogin($userLogin)
            ->setUserPSW($userPSW);
    }
    /**
     * Get policyNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPolicyNumber()
    {
        return isset($this->policyNumber) ? $this->policyNumber : null;
    }
    /**
     * Set policyNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $policyNumber
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesParameters
     */
    public function setPolicyNumber($policyNumber = null)
    {
        // validation for constraint: string
        if (!is_null($policyNumber) && !is_string($policyNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($policyNumber, true), gettype($policyNumber)), __LINE__);
        }
        if (is_null($policyNumber) || (is_array($policyNumber) && empty($policyNumber))) {
            unset($this->policyNumber);
        } else {
            $this->policyNumber = $policyNumber;
        }
        return $this;
    }
    /**
     * Get policyPeriodFrom value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPolicyPeriodFrom()
    {
        return isset($this->policyPeriodFrom) ? $this->policyPeriodFrom : null;
    }
    /**
     * Set policyPeriodFrom value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $policyPeriodFrom
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesParameters
     */
    public function setPolicyPeriodFrom($policyPeriodFrom = null)
    {
        // validation for constraint: string
        if (!is_null($policyPeriodFrom) && !is_string($policyPeriodFrom)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($policyPeriodFrom, true), gettype($policyPeriodFrom)), __LINE__);
        }
        if (is_null($policyPeriodFrom) || (is_array($policyPeriodFrom) && empty($policyPeriodFrom))) {
            unset($this->policyPeriodFrom);
        } else {
            $this->policyPeriodFrom = $policyPeriodFrom;
        }
        return $this;
    }
    /**
     * Get policyPeriodTill value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPolicyPeriodTill()
    {
        return isset($this->policyPeriodTill) ? $this->policyPeriodTill : null;
    }
    /**
     * Set policyPeriodTill value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $policyPeriodTill
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesParameters
     */
    public function setPolicyPeriodTill($policyPeriodTill = null)
    {
        // validation for constraint: string
        if (!is_null($policyPeriodTill) && !is_string($policyPeriodTill)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($policyPeriodTill, true), gettype($policyPeriodTill)), __LINE__);
        }
        if (is_null($policyPeriodTill) || (is_array($policyPeriodTill) && empty($policyPeriodTill))) {
            unset($this->policyPeriodTill);
        } else {
            $this->policyPeriodTill = $policyPeriodTill;
        }
        return $this;
    }
    /**
     * Get userId value
     * @return string|null
     */
    public function getUserId()
    {
        return $this->userId;
    }
    /**
     * Set userId value
     * @param string $userId
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesParameters
     */
    public function setUserId($userId = null)
    {
        // validation for constraint: string
        if (!is_null($userId) && !is_string($userId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userId, true), gettype($userId)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($userId) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $userId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($userId, true)), __LINE__);
        }
        $this->userId = $userId;
        return $this;
    }
    /**
     * Get userLogin value
     * @return string|null
     */
    public function getUserLogin()
    {
        return $this->userLogin;
    }
    /**
     * Set userLogin value
     * @param string $userLogin
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesParameters
     */
    public function setUserLogin($userLogin = null)
    {
        // validation for constraint: string
        if (!is_null($userLogin) && !is_string($userLogin)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userLogin, true), gettype($userLogin)), __LINE__);
        }
        $this->userLogin = $userLogin;
        return $this;
    }
    /**
     * Get userPSW value
     * @return string|null
     */
    public function getUserPSW()
    {
        return $this->userPSW;
    }
    /**
     * Set userPSW value
     * @param string $userPSW
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesParameters
     */
    public function setUserPSW($userPSW = null)
    {
        // validation for constraint: string
        if (!is_null($userPSW) && !is_string($userPSW)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userPSW, true), gettype($userPSW)), __LINE__);
        }
        $this->userPSW = $userPSW;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesParameters
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
