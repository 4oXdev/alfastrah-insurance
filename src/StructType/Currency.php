<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for currency StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q32:currency
 * @subpackage Structs
 */
class Currency extends BaseDictionary
{
    /**
     * The currency
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $currency;
    /**
     * The currencyID
     * @var int
     */
    public $currencyID;
    /**
     * The currencyName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $currencyName;
    /**
     * Constructor method for currency
     * @uses Currency::setCurrency()
     * @uses Currency::setCurrencyID()
     * @uses Currency::setCurrencyName()
     * @param string $currency
     * @param int $currencyID
     * @param string $currencyName
     */
    public function __construct($currency = null, $currencyID = null, $currencyName = null)
    {
        $this
            ->setCurrency($currency)
            ->setCurrencyID($currencyID)
            ->setCurrencyName($currencyName);
    }
    /**
     * Get currency value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCurrency()
    {
        return isset($this->currency) ? $this->currency : null;
    }
    /**
     * Set currency value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $currency
     * @return \RFD\AlfaStrahInsurance\StructType\Currency
     */
    public function setCurrency($currency = null)
    {
        // validation for constraint: string
        if (!is_null($currency) && !is_string($currency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currency, true), gettype($currency)), __LINE__);
        }
        if (is_null($currency) || (is_array($currency) && empty($currency))) {
            unset($this->currency);
        } else {
            $this->currency = $currency;
        }
        return $this;
    }
    /**
     * Get currencyID value
     * @return int|null
     */
    public function getCurrencyID()
    {
        return $this->currencyID;
    }
    /**
     * Set currencyID value
     * @param int $currencyID
     * @return \RFD\AlfaStrahInsurance\StructType\Currency
     */
    public function setCurrencyID($currencyID = null)
    {
        // validation for constraint: int
        if (!is_null($currencyID) && !(is_int($currencyID) || ctype_digit($currencyID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($currencyID, true), gettype($currencyID)), __LINE__);
        }
        $this->currencyID = $currencyID;
        return $this;
    }
    /**
     * Get currencyName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCurrencyName()
    {
        return isset($this->currencyName) ? $this->currencyName : null;
    }
    /**
     * Set currencyName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $currencyName
     * @return \RFD\AlfaStrahInsurance\StructType\Currency
     */
    public function setCurrencyName($currencyName = null)
    {
        // validation for constraint: string
        if (!is_null($currencyName) && !is_string($currencyName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyName, true), gettype($currencyName)), __LINE__);
        }
        if (is_null($currencyName) || (is_array($currencyName) && empty($currencyName))) {
            unset($this->currencyName);
        } else {
            $this->currencyName = $currencyName;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\Currency
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
