<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getDictionariesParameters StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q3:getDictionariesParameters
 * @subpackage Structs
 */
class GetDictionariesParameters extends AbstractStructBase
{
    /**
     * The agentUid
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $agentUid;
    /**
     * The assistanceUid
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $assistanceUid;
    /**
     * The countryUIDs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\CountryUIDs
     */
    public $countryUIDs;
    /**
     * The countryUid
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $countryUid;
    /**
     * The programUid
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $programUid;
    /**
     * The riskUid
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $riskUid;
    /**
     * Constructor method for getDictionariesParameters
     * @uses GetDictionariesParameters::setAgentUid()
     * @uses GetDictionariesParameters::setAssistanceUid()
     * @uses GetDictionariesParameters::setCountryUIDs()
     * @uses GetDictionariesParameters::setCountryUid()
     * @uses GetDictionariesParameters::setProgramUid()
     * @uses GetDictionariesParameters::setRiskUid()
     * @param string $agentUid
     * @param string $assistanceUid
     * @param \RFD\AlfaStrahInsurance\StructType\CountryUIDs $countryUIDs
     * @param string $countryUid
     * @param string $programUid
     * @param string $riskUid
     */
    public function __construct($agentUid = null, $assistanceUid = null, \RFD\AlfaStrahInsurance\StructType\CountryUIDs $countryUIDs = null, $countryUid = null, $programUid = null, $riskUid = null)
    {
        $this
            ->setAgentUid($agentUid)
            ->setAssistanceUid($assistanceUid)
            ->setCountryUIDs($countryUIDs)
            ->setCountryUid($countryUid)
            ->setProgramUid($programUid)
            ->setRiskUid($riskUid);
    }
    /**
     * Get agentUid value
     * @return string|null
     */
    public function getAgentUid()
    {
        return $this->agentUid;
    }
    /**
     * Set agentUid value
     * @param string $agentUid
     * @return \RFD\AlfaStrahInsurance\StructType\GetDictionariesParameters
     */
    public function setAgentUid($agentUid = null)
    {
        // validation for constraint: string
        if (!is_null($agentUid) && !is_string($agentUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($agentUid, true), gettype($agentUid)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($agentUid) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $agentUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($agentUid, true)), __LINE__);
        }
        $this->agentUid = $agentUid;
        return $this;
    }
    /**
     * Get assistanceUid value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAssistanceUid()
    {
        return isset($this->assistanceUid) ? $this->assistanceUid : null;
    }
    /**
     * Set assistanceUid value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $assistanceUid
     * @return \RFD\AlfaStrahInsurance\StructType\GetDictionariesParameters
     */
    public function setAssistanceUid($assistanceUid = null)
    {
        // validation for constraint: string
        if (!is_null($assistanceUid) && !is_string($assistanceUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($assistanceUid, true), gettype($assistanceUid)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($assistanceUid) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $assistanceUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($assistanceUid, true)), __LINE__);
        }
        if (is_null($assistanceUid) || (is_array($assistanceUid) && empty($assistanceUid))) {
            unset($this->assistanceUid);
        } else {
            $this->assistanceUid = $assistanceUid;
        }
        return $this;
    }
    /**
     * Get countryUIDs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\CountryUIDs|null
     */
    public function getCountryUIDs()
    {
        return isset($this->countryUIDs) ? $this->countryUIDs : null;
    }
    /**
     * Set countryUIDs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\CountryUIDs $countryUIDs
     * @return \RFD\AlfaStrahInsurance\StructType\GetDictionariesParameters
     */
    public function setCountryUIDs(\RFD\AlfaStrahInsurance\StructType\CountryUIDs $countryUIDs = null)
    {
        if (is_null($countryUIDs) || (is_array($countryUIDs) && empty($countryUIDs))) {
            unset($this->countryUIDs);
        } else {
            $this->countryUIDs = $countryUIDs;
        }
        return $this;
    }
    /**
     * Get countryUid value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryUid()
    {
        return isset($this->countryUid) ? $this->countryUid : null;
    }
    /**
     * Set countryUid value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryUid
     * @return \RFD\AlfaStrahInsurance\StructType\GetDictionariesParameters
     */
    public function setCountryUid($countryUid = null)
    {
        // validation for constraint: string
        if (!is_null($countryUid) && !is_string($countryUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryUid, true), gettype($countryUid)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($countryUid) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $countryUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($countryUid, true)), __LINE__);
        }
        if (is_null($countryUid) || (is_array($countryUid) && empty($countryUid))) {
            unset($this->countryUid);
        } else {
            $this->countryUid = $countryUid;
        }
        return $this;
    }
    /**
     * Get programUid value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProgramUid()
    {
        return isset($this->programUid) ? $this->programUid : null;
    }
    /**
     * Set programUid value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $programUid
     * @return \RFD\AlfaStrahInsurance\StructType\GetDictionariesParameters
     */
    public function setProgramUid($programUid = null)
    {
        // validation for constraint: string
        if (!is_null($programUid) && !is_string($programUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($programUid, true), gettype($programUid)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($programUid) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $programUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($programUid, true)), __LINE__);
        }
        if (is_null($programUid) || (is_array($programUid) && empty($programUid))) {
            unset($this->programUid);
        } else {
            $this->programUid = $programUid;
        }
        return $this;
    }
    /**
     * Get riskUid value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRiskUid()
    {
        return isset($this->riskUid) ? $this->riskUid : null;
    }
    /**
     * Set riskUid value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $riskUid
     * @return \RFD\AlfaStrahInsurance\StructType\GetDictionariesParameters
     */
    public function setRiskUid($riskUid = null)
    {
        // validation for constraint: string
        if (!is_null($riskUid) && !is_string($riskUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($riskUid, true), gettype($riskUid)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($riskUid) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $riskUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($riskUid, true)), __LINE__);
        }
        if (is_null($riskUid) || (is_array($riskUid) && empty($riskUid))) {
            unset($this->riskUid);
        } else {
            $this->riskUid = $riskUid;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetDictionariesParameters
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
