<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for additionalConditions StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q83:additionalConditions
 * @subpackage Structs
 */
class AdditionalConditions extends AbstractStructBase
{
    /**
     * The additionalCondition
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string[]
     */
    public $additionalCondition;
    /**
     * Constructor method for additionalConditions
     * @uses AdditionalConditions::setAdditionalCondition()
     * @param string[] $additionalCondition
     */
    public function __construct(array $additionalCondition = array())
    {
        $this
            ->setAdditionalCondition($additionalCondition);
    }
    /**
     * Get additionalCondition value
     * @return string[]|null
     */
    public function getAdditionalCondition()
    {
        return $this->additionalCondition;
    }
    /**
     * This method is responsible for validating the values passed to the setAdditionalCondition method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAdditionalCondition method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAdditionalConditionForArrayConstraintsFromSetAdditionalCondition(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $additionalConditionsAdditionalConditionItem) {
            // validation for constraint: itemType
            if (!is_string($additionalConditionsAdditionalConditionItem)) {
                $invalidValues[] = is_object($additionalConditionsAdditionalConditionItem) ? get_class($additionalConditionsAdditionalConditionItem) : sprintf('%s(%s)', gettype($additionalConditionsAdditionalConditionItem), var_export($additionalConditionsAdditionalConditionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The additionalCondition property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set additionalCondition value
     * @throws \InvalidArgumentException
     * @param string[] $additionalCondition
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalConditions
     */
    public function setAdditionalCondition(array $additionalCondition = array())
    {
        // validation for constraint: array
        if ('' !== ($additionalConditionArrayErrorMessage = self::validateAdditionalConditionForArrayConstraintsFromSetAdditionalCondition($additionalCondition))) {
            throw new \InvalidArgumentException($additionalConditionArrayErrorMessage, __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($additionalCondition) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $additionalCondition)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($additionalCondition, true)), __LINE__);
        }
        $this->additionalCondition = $additionalCondition;
        return $this;
    }
    /**
     * Add item to additionalCondition value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalConditions
     */
    public function addToAdditionalCondition($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The additionalCondition property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $item)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($item, true)), __LINE__);
        }
        $this->additionalCondition[] = $item;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalConditions
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
