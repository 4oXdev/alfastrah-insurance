<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for countryUIDs StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q4:countryUIDs
 * @subpackage Structs
 */
class CountryUIDs extends AbstractStructBase
{
    /**
     * The countryUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string[]
     */
    public $countryUID;
    /**
     * Constructor method for countryUIDs
     * @uses CountryUIDs::setCountryUID()
     * @param string[] $countryUID
     */
    public function __construct(array $countryUID = array())
    {
        $this
            ->setCountryUID($countryUID);
    }
    /**
     * Get countryUID value
     * @return string[]|null
     */
    public function getCountryUID()
    {
        return $this->countryUID;
    }
    /**
     * This method is responsible for validating the values passed to the setCountryUID method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCountryUID method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCountryUIDForArrayConstraintsFromSetCountryUID(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $countryUIDsCountryUIDItem) {
            // validation for constraint: itemType
            if (!is_string($countryUIDsCountryUIDItem)) {
                $invalidValues[] = is_object($countryUIDsCountryUIDItem) ? get_class($countryUIDsCountryUIDItem) : sprintf('%s(%s)', gettype($countryUIDsCountryUIDItem), var_export($countryUIDsCountryUIDItem, true));
            }
            // Added
            // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
            elseif (empty($countryUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $countryUIDsCountryUIDItem)) {
                $invalidValues[] = sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($countryUID, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The countryUID property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set countryUID value
     * @throws \InvalidArgumentException
     * @param string[] $countryUID
     * @return \RFD\AlfaStrahInsurance\StructType\CountryUIDs
     */
    public function setCountryUID(array $countryUID = array())
    {
        // validation for constraint: array
        if ('' !== ($countryUIDArrayErrorMessage = self::validateCountryUIDForArrayConstraintsFromSetCountryUID($countryUID))) {
            throw new \InvalidArgumentException($countryUIDArrayErrorMessage, __LINE__);
        }
//        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
//        if (!is_null($countryUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $countryUID)) {
//            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($countryUID, true)), __LINE__);
//        }
        $this->countryUID = $countryUID;
        return $this;
    }
    /**
     * Add item to countryUID value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \RFD\AlfaStrahInsurance\StructType\CountryUIDs
     */
    public function addToCountryUID($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The countryUID property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $item)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($item, true)), __LINE__);
        }
        $this->countryUID[] = $item;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\CountryUIDs
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
