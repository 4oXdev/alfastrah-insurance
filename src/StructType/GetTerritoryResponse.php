<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetTerritoryResponse StructType
 * @subpackage Structs
 */
class GetTerritoryResponse extends AbstractStructBase
{
    /**
     * The GetTerritoryResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\ArrayType\ArrayOfterritory
     */
    public $GetTerritoryResult;
    /**
     * Constructor method for GetTerritoryResponse
     * @uses GetTerritoryResponse::setGetTerritoryResult()
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfterritory $getTerritoryResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfterritory $getTerritoryResult = null)
    {
        $this
            ->setGetTerritoryResult($getTerritoryResult);
    }
    /**
     * Get GetTerritoryResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfterritory|null
     */
    public function getGetTerritoryResult()
    {
        return isset($this->GetTerritoryResult) ? $this->GetTerritoryResult : null;
    }
    /**
     * Set GetTerritoryResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfterritory $getTerritoryResult
     * @return \RFD\AlfaStrahInsurance\StructType\GetTerritoryResponse
     */
    public function setGetTerritoryResult(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfterritory $getTerritoryResult = null)
    {
        if (is_null($getTerritoryResult) || (is_array($getTerritoryResult) && empty($getTerritoryResult))) {
            unset($this->GetTerritoryResult);
        } else {
            $this->GetTerritoryResult = $getTerritoryResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetTerritoryResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
