<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCountriesRusResponse StructType
 * @subpackage Structs
 */
class GetCountriesRusResponse extends AbstractStructBase
{
    /**
     * The GetCountriesRusResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry
     */
    public $GetCountriesRusResult;
    /**
     * Constructor method for GetCountriesRusResponse
     * @uses GetCountriesRusResponse::setGetCountriesRusResult()
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry $getCountriesRusResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry $getCountriesRusResult = null)
    {
        $this
            ->setGetCountriesRusResult($getCountriesRusResult);
    }
    /**
     * Get GetCountriesRusResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry|null
     */
    public function getGetCountriesRusResult()
    {
        return isset($this->GetCountriesRusResult) ? $this->GetCountriesRusResult : null;
    }
    /**
     * Set GetCountriesRusResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry $getCountriesRusResult
     * @return \RFD\AlfaStrahInsurance\StructType\GetCountriesRusResponse
     */
    public function setGetCountriesRusResult(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry $getCountriesRusResult = null)
    {
        if (is_null($getCountriesRusResult) || (is_array($getCountriesRusResult) && empty($getCountriesRusResult))) {
            unset($this->GetCountriesRusResult);
        } else {
            $this->GetCountriesRusResult = $getCountriesRusResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetCountriesRusResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
