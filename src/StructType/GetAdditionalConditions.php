<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAdditionalConditions StructType
 * @subpackage Structs
 */
class GetAdditionalConditions extends AbstractStructBase
{
    /**
     * The parameters
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\GetDictionariesParameters
     */
    public $parameters;
    /**
     * Constructor method for GetAdditionalConditions
     * @uses GetAdditionalConditions::setParameters()
     * @param \RFD\AlfaStrahInsurance\StructType\GetDictionariesParameters $parameters
     */
    public function __construct(\RFD\AlfaStrahInsurance\StructType\GetDictionariesParameters $parameters = null)
    {
        $this
            ->setParameters($parameters);
    }
    /**
     * Get parameters value
     * @return \RFD\AlfaStrahInsurance\StructType\GetDictionariesParameters
     */
    public function getParameters()
    {
        return $this->parameters;
    }
    /**
     * Set parameters value
     * @param \RFD\AlfaStrahInsurance\StructType\GetDictionariesParameters $parameters
     * @return \RFD\AlfaStrahInsurance\StructType\GetAdditionalConditions
     */
    public function setParameters(\RFD\AlfaStrahInsurance\StructType\GetDictionariesParameters $parameters = null)
    {
        $this->parameters = $parameters;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetAdditionalConditions
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
