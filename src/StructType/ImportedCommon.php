<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for importedCommon StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q107:importedCommon
 * @subpackage Structs
 */
class ImportedCommon extends AbstractStructBase
{
    /**
     * The CountriesForPrint
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CountriesForPrint;
    /**
     * The additionalCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $additionalCode;
    /**
     * The additionalCondition
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $additionalCondition;
    /**
     * The additionalCondition2
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $additionalCondition2;
    /**
     * The addressTel
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $addressTel;
    /**
     * The comments
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $comments;
    /**
     * The countryUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $countryUID;
    /**
     * The dateOfBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $dateOfBirth;
    /**
     * The documentNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $documentNumber;
    /**
     * The documentSerial
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $documentSerial;
    /**
     * The documentType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $documentType;
    /**
     * The dtCreated
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $dtCreated;
    /**
     * The eMail
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $eMail;
    /**
     * The eurRate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $eurRate;
    /**
     * The fio
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $fio;
    /**
     * The insuranceProgrammUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $insuranceProgrammUID;
    /**
     * The multiDays
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $multiDays;
    /**
     * The nettoPremRUR
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $nettoPremRUR;
    /**
     * The operation
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $operation;
    /**
     * The policyNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $policyNumber;
    /**
     * The policyPeriodFrom
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $policyPeriodFrom;
    /**
     * The policyPeriodTill
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $policyPeriodTill;
    /**
     * The usdRate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $usdRate;
    /**
     * The userId
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $userId;
    /**
     * The userLogin
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $userLogin;
    /**
     * The userPSW
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $userPSW;
    /**
     * The usersLogin
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $usersLogin;
    /**
     * The usersName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $usersName;
    /**
     * Constructor method for importedCommon
     * @uses ImportedCommon::setCountriesForPrint()
     * @uses ImportedCommon::setAdditionalCode()
     * @uses ImportedCommon::setAdditionalCondition()
     * @uses ImportedCommon::setAdditionalCondition2()
     * @uses ImportedCommon::setAddressTel()
     * @uses ImportedCommon::setComments()
     * @uses ImportedCommon::setCountryUID()
     * @uses ImportedCommon::setDateOfBirth()
     * @uses ImportedCommon::setDocumentNumber()
     * @uses ImportedCommon::setDocumentSerial()
     * @uses ImportedCommon::setDocumentType()
     * @uses ImportedCommon::setDtCreated()
     * @uses ImportedCommon::setEMail()
     * @uses ImportedCommon::setEurRate()
     * @uses ImportedCommon::setFio()
     * @uses ImportedCommon::setInsuranceProgrammUID()
     * @uses ImportedCommon::setMultiDays()
     * @uses ImportedCommon::setNettoPremRUR()
     * @uses ImportedCommon::setOperation()
     * @uses ImportedCommon::setPolicyNumber()
     * @uses ImportedCommon::setPolicyPeriodFrom()
     * @uses ImportedCommon::setPolicyPeriodTill()
     * @uses ImportedCommon::setUsdRate()
     * @uses ImportedCommon::setUserId()
     * @uses ImportedCommon::setUserLogin()
     * @uses ImportedCommon::setUserPSW()
     * @uses ImportedCommon::setUsersLogin()
     * @uses ImportedCommon::setUsersName()
     * @param string $countriesForPrint
     * @param string $additionalCode
     * @param string $additionalCondition
     * @param string $additionalCondition2
     * @param string $addressTel
     * @param string $comments
     * @param string $countryUID
     * @param string $dateOfBirth
     * @param string $documentNumber
     * @param string $documentSerial
     * @param string $documentType
     * @param string $dtCreated
     * @param string $eMail
     * @param float $eurRate
     * @param string $fio
     * @param string $insuranceProgrammUID
     * @param int $multiDays
     * @param float $nettoPremRUR
     * @param string $operation
     * @param string $policyNumber
     * @param string $policyPeriodFrom
     * @param string $policyPeriodTill
     * @param float $usdRate
     * @param string $userId
     * @param string $userLogin
     * @param string $userPSW
     * @param string $usersLogin
     * @param string $usersName
     */
    public function __construct($countriesForPrint = null, $additionalCode = null, $additionalCondition = null, $additionalCondition2 = null, $addressTel = null, $comments = null, $countryUID = null, $dateOfBirth = null, $documentNumber = null, $documentSerial = null, $documentType = null, $dtCreated = null, $eMail = null, $eurRate = null, $fio = null, $insuranceProgrammUID = null, $multiDays = null, $nettoPremRUR = null, $operation = null, $policyNumber = null, $policyPeriodFrom = null, $policyPeriodTill = null, $usdRate = null, $userId = null, $userLogin = null, $userPSW = null, $usersLogin = null, $usersName = null)
    {
        $this
            ->setCountriesForPrint($countriesForPrint)
            ->setAdditionalCode($additionalCode)
            ->setAdditionalCondition($additionalCondition)
            ->setAdditionalCondition2($additionalCondition2)
            ->setAddressTel($addressTel)
            ->setComments($comments)
            ->setCountryUID($countryUID)
            ->setDateOfBirth($dateOfBirth)
            ->setDocumentNumber($documentNumber)
            ->setDocumentSerial($documentSerial)
            ->setDocumentType($documentType)
            ->setDtCreated($dtCreated)
            ->setEMail($eMail)
            ->setEurRate($eurRate)
            ->setFio($fio)
            ->setInsuranceProgrammUID($insuranceProgrammUID)
            ->setMultiDays($multiDays)
            ->setNettoPremRUR($nettoPremRUR)
            ->setOperation($operation)
            ->setPolicyNumber($policyNumber)
            ->setPolicyPeriodFrom($policyPeriodFrom)
            ->setPolicyPeriodTill($policyPeriodTill)
            ->setUsdRate($usdRate)
            ->setUserId($userId)
            ->setUserLogin($userLogin)
            ->setUserPSW($userPSW)
            ->setUsersLogin($usersLogin)
            ->setUsersName($usersName);
    }
    /**
     * Get CountriesForPrint value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountriesForPrint()
    {
        return isset($this->CountriesForPrint) ? $this->CountriesForPrint : null;
    }
    /**
     * Set CountriesForPrint value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countriesForPrint
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setCountriesForPrint($countriesForPrint = null)
    {
        // validation for constraint: string
        if (!is_null($countriesForPrint) && !is_string($countriesForPrint)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countriesForPrint, true), gettype($countriesForPrint)), __LINE__);
        }
        if (is_null($countriesForPrint) || (is_array($countriesForPrint) && empty($countriesForPrint))) {
            unset($this->CountriesForPrint);
        } else {
            $this->CountriesForPrint = $countriesForPrint;
        }
        return $this;
    }
    /**
     * Get additionalCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdditionalCode()
    {
        return isset($this->additionalCode) ? $this->additionalCode : null;
    }
    /**
     * Set additionalCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $additionalCode
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setAdditionalCode($additionalCode = null)
    {
        // validation for constraint: string
        if (!is_null($additionalCode) && !is_string($additionalCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalCode, true), gettype($additionalCode)), __LINE__);
        }
        if (is_null($additionalCode) || (is_array($additionalCode) && empty($additionalCode))) {
            unset($this->additionalCode);
        } else {
            $this->additionalCode = $additionalCode;
        }
        return $this;
    }
    /**
     * Get additionalCondition value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdditionalCondition()
    {
        return isset($this->additionalCondition) ? $this->additionalCondition : null;
    }
    /**
     * Set additionalCondition value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $additionalCondition
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setAdditionalCondition($additionalCondition = null)
    {
        // validation for constraint: string
        if (!is_null($additionalCondition) && !is_string($additionalCondition)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalCondition, true), gettype($additionalCondition)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($additionalCondition) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $additionalCondition)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($additionalCondition, true)), __LINE__);
        }
        if (is_null($additionalCondition) || (is_array($additionalCondition) && empty($additionalCondition))) {
            unset($this->additionalCondition);
        } else {
            $this->additionalCondition = $additionalCondition;
        }
        return $this;
    }
    /**
     * Get additionalCondition2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAdditionalCondition2()
    {
        return isset($this->additionalCondition2) ? $this->additionalCondition2 : null;
    }
    /**
     * Set additionalCondition2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $additionalCondition2
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setAdditionalCondition2($additionalCondition2 = null)
    {
        // validation for constraint: string
        if (!is_null($additionalCondition2) && !is_string($additionalCondition2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalCondition2, true), gettype($additionalCondition2)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($additionalCondition2) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $additionalCondition2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($additionalCondition2, true)), __LINE__);
        }
        if (is_null($additionalCondition2) || (is_array($additionalCondition2) && empty($additionalCondition2))) {
            unset($this->additionalCondition2);
        } else {
            $this->additionalCondition2 = $additionalCondition2;
        }
        return $this;
    }
    /**
     * Get addressTel value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAddressTel()
    {
        return isset($this->addressTel) ? $this->addressTel : null;
    }
    /**
     * Set addressTel value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $addressTel
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setAddressTel($addressTel = null)
    {
        // validation for constraint: string
        if (!is_null($addressTel) && !is_string($addressTel)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($addressTel, true), gettype($addressTel)), __LINE__);
        }
        if (is_null($addressTel) || (is_array($addressTel) && empty($addressTel))) {
            unset($this->addressTel);
        } else {
            $this->addressTel = $addressTel;
        }
        return $this;
    }
    /**
     * Get comments value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getComments()
    {
        return isset($this->comments) ? $this->comments : null;
    }
    /**
     * Set comments value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $comments
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setComments($comments = null)
    {
        // validation for constraint: string
        if (!is_null($comments) && !is_string($comments)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($comments, true), gettype($comments)), __LINE__);
        }
        if (is_null($comments) || (is_array($comments) && empty($comments))) {
            unset($this->comments);
        } else {
            $this->comments = $comments;
        }
        return $this;
    }
    /**
     * Get countryUID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryUID()
    {
        return isset($this->countryUID) ? $this->countryUID : null;
    }
    /**
     * Set countryUID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryUID
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setCountryUID($countryUID = null)
    {
        // validation for constraint: string
        if (!is_null($countryUID) && !is_string($countryUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryUID, true), gettype($countryUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($countryUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $countryUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($countryUID, true)), __LINE__);
        }
        if (is_null($countryUID) || (is_array($countryUID) && empty($countryUID))) {
            unset($this->countryUID);
        } else {
            $this->countryUID = $countryUID;
        }
        return $this;
    }
    /**
     * Get dateOfBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDateOfBirth()
    {
        return isset($this->dateOfBirth) ? $this->dateOfBirth : null;
    }
    /**
     * Set dateOfBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dateOfBirth
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setDateOfBirth($dateOfBirth = null)
    {
        // validation for constraint: string
        if (!is_null($dateOfBirth) && !is_string($dateOfBirth)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateOfBirth, true), gettype($dateOfBirth)), __LINE__);
        }
        if (is_null($dateOfBirth) || (is_array($dateOfBirth) && empty($dateOfBirth))) {
            unset($this->dateOfBirth);
        } else {
            $this->dateOfBirth = $dateOfBirth;
        }
        return $this;
    }
    /**
     * Get documentNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDocumentNumber()
    {
        return isset($this->documentNumber) ? $this->documentNumber : null;
    }
    /**
     * Set documentNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $documentNumber
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setDocumentNumber($documentNumber = null)
    {
        // validation for constraint: string
        if (!is_null($documentNumber) && !is_string($documentNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentNumber, true), gettype($documentNumber)), __LINE__);
        }
        if (is_null($documentNumber) || (is_array($documentNumber) && empty($documentNumber))) {
            unset($this->documentNumber);
        } else {
            $this->documentNumber = $documentNumber;
        }
        return $this;
    }
    /**
     * Get documentSerial value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDocumentSerial()
    {
        return isset($this->documentSerial) ? $this->documentSerial : null;
    }
    /**
     * Set documentSerial value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $documentSerial
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setDocumentSerial($documentSerial = null)
    {
        // validation for constraint: string
        if (!is_null($documentSerial) && !is_string($documentSerial)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentSerial, true), gettype($documentSerial)), __LINE__);
        }
        if (is_null($documentSerial) || (is_array($documentSerial) && empty($documentSerial))) {
            unset($this->documentSerial);
        } else {
            $this->documentSerial = $documentSerial;
        }
        return $this;
    }
    /**
     * Get documentType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDocumentType()
    {
        return isset($this->documentType) ? $this->documentType : null;
    }
    /**
     * Set documentType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $documentType
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setDocumentType($documentType = null)
    {
        // validation for constraint: string
        if (!is_null($documentType) && !is_string($documentType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentType, true), gettype($documentType)), __LINE__);
        }
        if (is_null($documentType) || (is_array($documentType) && empty($documentType))) {
            unset($this->documentType);
        } else {
            $this->documentType = $documentType;
        }
        return $this;
    }
    /**
     * Get dtCreated value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDtCreated()
    {
        return isset($this->dtCreated) ? $this->dtCreated : null;
    }
    /**
     * Set dtCreated value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dtCreated
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setDtCreated($dtCreated = null)
    {
        // validation for constraint: string
        if (!is_null($dtCreated) && !is_string($dtCreated)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dtCreated, true), gettype($dtCreated)), __LINE__);
        }
        if (is_null($dtCreated) || (is_array($dtCreated) && empty($dtCreated))) {
            unset($this->dtCreated);
        } else {
            $this->dtCreated = $dtCreated;
        }
        return $this;
    }
    /**
     * Get eMail value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEMail()
    {
        return isset($this->eMail) ? $this->eMail : null;
    }
    /**
     * Set eMail value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $eMail
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setEMail($eMail = null)
    {
        // validation for constraint: string
        if (!is_null($eMail) && !is_string($eMail)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($eMail, true), gettype($eMail)), __LINE__);
        }
        if (is_null($eMail) || (is_array($eMail) && empty($eMail))) {
            unset($this->eMail);
        } else {
            $this->eMail = $eMail;
        }
        return $this;
    }
    /**
     * Get eurRate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getEurRate()
    {
        return isset($this->eurRate) ? $this->eurRate : null;
    }
    /**
     * Set eurRate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $eurRate
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setEurRate($eurRate = null)
    {
        // validation for constraint: float
        if (!is_null($eurRate) && !(is_float($eurRate) || is_numeric($eurRate))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($eurRate, true), gettype($eurRate)), __LINE__);
        }
        if (is_null($eurRate) || (is_array($eurRate) && empty($eurRate))) {
            unset($this->eurRate);
        } else {
            $this->eurRate = $eurRate;
        }
        return $this;
    }
    /**
     * Get fio value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFio()
    {
        return isset($this->fio) ? $this->fio : null;
    }
    /**
     * Set fio value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $fio
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setFio($fio = null)
    {
        // validation for constraint: string
        if (!is_null($fio) && !is_string($fio)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fio, true), gettype($fio)), __LINE__);
        }
        if (is_null($fio) || (is_array($fio) && empty($fio))) {
            unset($this->fio);
        } else {
            $this->fio = $fio;
        }
        return $this;
    }
    /**
     * Get insuranceProgrammUID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getInsuranceProgrammUID()
    {
        return isset($this->insuranceProgrammUID) ? $this->insuranceProgrammUID : null;
    }
    /**
     * Set insuranceProgrammUID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $insuranceProgrammUID
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setInsuranceProgrammUID($insuranceProgrammUID = null)
    {
        // validation for constraint: string
        if (!is_null($insuranceProgrammUID) && !is_string($insuranceProgrammUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($insuranceProgrammUID, true), gettype($insuranceProgrammUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($insuranceProgrammUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $insuranceProgrammUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($insuranceProgrammUID, true)), __LINE__);
        }
        if (is_null($insuranceProgrammUID) || (is_array($insuranceProgrammUID) && empty($insuranceProgrammUID))) {
            unset($this->insuranceProgrammUID);
        } else {
            $this->insuranceProgrammUID = $insuranceProgrammUID;
        }
        return $this;
    }
    /**
     * Get multiDays value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getMultiDays()
    {
        return isset($this->multiDays) ? $this->multiDays : null;
    }
    /**
     * Set multiDays value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $multiDays
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setMultiDays($multiDays = null)
    {
        // validation for constraint: int
        if (!is_null($multiDays) && !(is_int($multiDays) || ctype_digit($multiDays))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($multiDays, true), gettype($multiDays)), __LINE__);
        }
        if (is_null($multiDays) || (is_array($multiDays) && empty($multiDays))) {
            unset($this->multiDays);
        } else {
            $this->multiDays = $multiDays;
        }
        return $this;
    }
    /**
     * Get nettoPremRUR value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getNettoPremRUR()
    {
        return isset($this->nettoPremRUR) ? $this->nettoPremRUR : null;
    }
    /**
     * Set nettoPremRUR value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $nettoPremRUR
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setNettoPremRUR($nettoPremRUR = null)
    {
        // validation for constraint: float
        if (!is_null($nettoPremRUR) && !(is_float($nettoPremRUR) || is_numeric($nettoPremRUR))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($nettoPremRUR, true), gettype($nettoPremRUR)), __LINE__);
        }
        if (is_null($nettoPremRUR) || (is_array($nettoPremRUR) && empty($nettoPremRUR))) {
            unset($this->nettoPremRUR);
        } else {
            $this->nettoPremRUR = $nettoPremRUR;
        }
        return $this;
    }
    /**
     * Get operation value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOperation()
    {
        return isset($this->operation) ? $this->operation : null;
    }
    /**
     * Set operation value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $operation
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setOperation($operation = null)
    {
        // validation for constraint: string
        if (!is_null($operation) && !is_string($operation)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($operation, true), gettype($operation)), __LINE__);
        }
        if (is_null($operation) || (is_array($operation) && empty($operation))) {
            unset($this->operation);
        } else {
            $this->operation = $operation;
        }
        return $this;
    }
    /**
     * Get policyNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPolicyNumber()
    {
        return isset($this->policyNumber) ? $this->policyNumber : null;
    }
    /**
     * Set policyNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $policyNumber
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setPolicyNumber($policyNumber = null)
    {
        // validation for constraint: string
        if (!is_null($policyNumber) && !is_string($policyNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($policyNumber, true), gettype($policyNumber)), __LINE__);
        }
        if (is_null($policyNumber) || (is_array($policyNumber) && empty($policyNumber))) {
            unset($this->policyNumber);
        } else {
            $this->policyNumber = $policyNumber;
        }
        return $this;
    }
    /**
     * Get policyPeriodFrom value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPolicyPeriodFrom()
    {
        return isset($this->policyPeriodFrom) ? $this->policyPeriodFrom : null;
    }
    /**
     * Set policyPeriodFrom value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $policyPeriodFrom
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setPolicyPeriodFrom($policyPeriodFrom = null)
    {
        // validation for constraint: string
        if (!is_null($policyPeriodFrom) && !is_string($policyPeriodFrom)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($policyPeriodFrom, true), gettype($policyPeriodFrom)), __LINE__);
        }
        if (is_null($policyPeriodFrom) || (is_array($policyPeriodFrom) && empty($policyPeriodFrom))) {
            unset($this->policyPeriodFrom);
        } else {
            $this->policyPeriodFrom = $policyPeriodFrom;
        }
        return $this;
    }
    /**
     * Get policyPeriodTill value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPolicyPeriodTill()
    {
        return isset($this->policyPeriodTill) ? $this->policyPeriodTill : null;
    }
    /**
     * Set policyPeriodTill value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $policyPeriodTill
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setPolicyPeriodTill($policyPeriodTill = null)
    {
        // validation for constraint: string
        if (!is_null($policyPeriodTill) && !is_string($policyPeriodTill)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($policyPeriodTill, true), gettype($policyPeriodTill)), __LINE__);
        }
        if (is_null($policyPeriodTill) || (is_array($policyPeriodTill) && empty($policyPeriodTill))) {
            unset($this->policyPeriodTill);
        } else {
            $this->policyPeriodTill = $policyPeriodTill;
        }
        return $this;
    }
    /**
     * Get usdRate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getUsdRate()
    {
        return isset($this->usdRate) ? $this->usdRate : null;
    }
    /**
     * Set usdRate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $usdRate
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setUsdRate($usdRate = null)
    {
        // validation for constraint: float
        if (!is_null($usdRate) && !(is_float($usdRate) || is_numeric($usdRate))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($usdRate, true), gettype($usdRate)), __LINE__);
        }
        if (is_null($usdRate) || (is_array($usdRate) && empty($usdRate))) {
            unset($this->usdRate);
        } else {
            $this->usdRate = $usdRate;
        }
        return $this;
    }
    /**
     * Get userId value
     * @return string|null
     */
    public function getUserId()
    {
        return $this->userId;
    }
    /**
     * Set userId value
     * @param string $userId
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setUserId($userId = null)
    {
        // validation for constraint: string
        if (!is_null($userId) && !is_string($userId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userId, true), gettype($userId)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($userId) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $userId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($userId, true)), __LINE__);
        }
        $this->userId = $userId;
        return $this;
    }
    /**
     * Get userLogin value
     * @return string|null
     */
    public function getUserLogin()
    {
        return $this->userLogin;
    }
    /**
     * Set userLogin value
     * @param string $userLogin
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setUserLogin($userLogin = null)
    {
        // validation for constraint: string
        if (!is_null($userLogin) && !is_string($userLogin)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userLogin, true), gettype($userLogin)), __LINE__);
        }
        $this->userLogin = $userLogin;
        return $this;
    }
    /**
     * Get userPSW value
     * @return string|null
     */
    public function getUserPSW()
    {
        return $this->userPSW;
    }
    /**
     * Set userPSW value
     * @param string $userPSW
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setUserPSW($userPSW = null)
    {
        // validation for constraint: string
        if (!is_null($userPSW) && !is_string($userPSW)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userPSW, true), gettype($userPSW)), __LINE__);
        }
        $this->userPSW = $userPSW;
        return $this;
    }
    /**
     * Get usersLogin value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsersLogin()
    {
        return isset($this->usersLogin) ? $this->usersLogin : null;
    }
    /**
     * Set usersLogin value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $usersLogin
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setUsersLogin($usersLogin = null)
    {
        // validation for constraint: string
        if (!is_null($usersLogin) && !is_string($usersLogin)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($usersLogin, true), gettype($usersLogin)), __LINE__);
        }
        if (is_null($usersLogin) || (is_array($usersLogin) && empty($usersLogin))) {
            unset($this->usersLogin);
        } else {
            $this->usersLogin = $usersLogin;
        }
        return $this;
    }
    /**
     * Get usersName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getUsersName()
    {
        return isset($this->usersName) ? $this->usersName : null;
    }
    /**
     * Set usersName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $usersName
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public function setUsersName($usersName = null)
    {
        // validation for constraint: string
        if (!is_null($usersName) && !is_string($usersName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($usersName, true), gettype($usersName)), __LINE__);
        }
        if (is_null($usersName) || (is_array($usersName) && empty($usersName))) {
            unset($this->usersName);
        } else {
            $this->usersName = $usersName;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedCommon
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
