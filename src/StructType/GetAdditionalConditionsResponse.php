<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAdditionalConditionsResponse StructType
 * @subpackage Structs
 */
class GetAdditionalConditionsResponse extends AbstractStructBase
{
    /**
     * The GetAdditionalConditionsResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition
     */
    public $GetAdditionalConditionsResult;
    /**
     * Constructor method for GetAdditionalConditionsResponse
     * @uses GetAdditionalConditionsResponse::setGetAdditionalConditionsResult()
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition $getAdditionalConditionsResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition $getAdditionalConditionsResult = null)
    {
        $this
            ->setGetAdditionalConditionsResult($getAdditionalConditionsResult);
    }
    /**
     * Get GetAdditionalConditionsResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition|null
     */
    public function getGetAdditionalConditionsResult()
    {
        return isset($this->GetAdditionalConditionsResult) ? $this->GetAdditionalConditionsResult : null;
    }
    /**
     * Set GetAdditionalConditionsResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition $getAdditionalConditionsResult
     * @return \RFD\AlfaStrahInsurance\StructType\GetAdditionalConditionsResponse
     */
    public function setGetAdditionalConditionsResult(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition $getAdditionalConditionsResult = null)
    {
        if (is_null($getAdditionalConditionsResult) || (is_array($getAdditionalConditionsResult) && empty($getAdditionalConditionsResult))) {
            unset($this->GetAdditionalConditionsResult);
        } else {
            $this->GetAdditionalConditionsResult = $getAdditionalConditionsResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetAdditionalConditionsResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
