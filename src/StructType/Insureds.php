<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for insureds StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q89:insureds
 * @subpackage Structs
 */
class Insureds extends AbstractStructBase
{
    /**
     * The insured
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Insured[]
     */
    public $insured;
    /**
     * Constructor method for insureds
     * @uses Insureds::setInsured()
     * @param \RFD\AlfaStrahInsurance\StructType\Insured[] $insured
     */
    public function __construct(array $insured = array())
    {
        $this
            ->setInsured($insured);
    }
    /**
     * Get insured value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Insured[]|null
     */
    public function getInsured()
    {
        return isset($this->insured) ? $this->insured : null;
    }
    /**
     * This method is responsible for validating the values passed to the setInsured method
     * This method is willingly generated in order to preserve the one-line inline validation within the setInsured method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateInsuredForArrayConstraintsFromSetInsured(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $insuredsInsuredItem) {
            // validation for constraint: itemType
            if (!$insuredsInsuredItem instanceof \RFD\AlfaStrahInsurance\StructType\Insured) {
                $invalidValues[] = is_object($insuredsInsuredItem) ? get_class($insuredsInsuredItem) : sprintf('%s(%s)', gettype($insuredsInsuredItem), var_export($insuredsInsuredItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The insured property can only contain items of type \RFD\AlfaStrahInsurance\StructType\Insured, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set insured value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\Insured[] $insured
     * @return \RFD\AlfaStrahInsurance\StructType\Insureds
     */
    public function setInsured(array $insured = array())
    {
        // validation for constraint: array
        if ('' !== ($insuredArrayErrorMessage = self::validateInsuredForArrayConstraintsFromSetInsured($insured))) {
            throw new \InvalidArgumentException($insuredArrayErrorMessage, __LINE__);
        }
        if (is_null($insured) || (is_array($insured) && empty($insured))) {
            unset($this->insured);
        } else {
            $this->insured = $insured;
        }
        return $this;
    }
    /**
     * Add item to insured value
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\Insured $item
     * @return \RFD\AlfaStrahInsurance\StructType\Insureds
     */
    public function addToInsured(\RFD\AlfaStrahInsurance\StructType\Insured $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \RFD\AlfaStrahInsurance\StructType\Insured) {
            throw new \InvalidArgumentException(sprintf('The insured property can only contain items of type \RFD\AlfaStrahInsurance\StructType\Insured, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->insured[] = $item;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\Insureds
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
