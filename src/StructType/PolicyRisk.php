<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for policyRisk StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q94:policyRisk
 * @subpackage Structs
 */
class PolicyRisk extends AbstractStructBase
{
    /**
     * The amountAtRisk
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $amountAtRisk;
    /**
     * The amountCurrency
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $amountCurrency;
    /**
     * The franshize
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $franshize;
    /**
     * The insAdress
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $insAdress;
    /**
     * The premCurrency
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $premCurrency;
    /**
     * The premRUR
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $premRUR;
    /**
     * The riskName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $riskName;
    /**
     * The riskUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $riskUID;
    /**
     * The riskVariantName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $riskVariantName;
    /**
     * Constructor method for policyRisk
     * @uses PolicyRisk::setAmountAtRisk()
     * @uses PolicyRisk::setAmountCurrency()
     * @uses PolicyRisk::setFranshize()
     * @uses PolicyRisk::setInsAdress()
     * @uses PolicyRisk::setPremCurrency()
     * @uses PolicyRisk::setPremRUR()
     * @uses PolicyRisk::setRiskName()
     * @uses PolicyRisk::setRiskUID()
     * @uses PolicyRisk::setRiskVariantName()
     * @param float $amountAtRisk
     * @param string $amountCurrency
     * @param string $franshize
     * @param string $insAdress
     * @param float $premCurrency
     * @param float $premRUR
     * @param string $riskName
     * @param string $riskUID
     * @param string $riskVariantName
     */
    public function __construct($amountAtRisk = null, $amountCurrency = null, $franshize = null, $insAdress = null, $premCurrency = null, $premRUR = null, $riskName = null, $riskUID = null, $riskVariantName = null)
    {
        $this
            ->setAmountAtRisk($amountAtRisk)
            ->setAmountCurrency($amountCurrency)
            ->setFranshize($franshize)
            ->setInsAdress($insAdress)
            ->setPremCurrency($premCurrency)
            ->setPremRUR($premRUR)
            ->setRiskName($riskName)
            ->setRiskUID($riskUID)
            ->setRiskVariantName($riskVariantName);
    }
    /**
     * Get amountAtRisk value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getAmountAtRisk()
    {
        return isset($this->amountAtRisk) ? $this->amountAtRisk : null;
    }
    /**
     * Set amountAtRisk value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $amountAtRisk
     * @return \RFD\AlfaStrahInsurance\StructType\PolicyRisk
     */
    public function setAmountAtRisk($amountAtRisk = null)
    {
        // validation for constraint: float
        if (!is_null($amountAtRisk) && !(is_float($amountAtRisk) || is_numeric($amountAtRisk))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amountAtRisk, true), gettype($amountAtRisk)), __LINE__);
        }
        if (is_null($amountAtRisk) || (is_array($amountAtRisk) && empty($amountAtRisk))) {
            unset($this->amountAtRisk);
        } else {
            $this->amountAtRisk = $amountAtRisk;
        }
        return $this;
    }
    /**
     * Get amountCurrency value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAmountCurrency()
    {
        return isset($this->amountCurrency) ? $this->amountCurrency : null;
    }
    /**
     * Set amountCurrency value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $amountCurrency
     * @return \RFD\AlfaStrahInsurance\StructType\PolicyRisk
     */
    public function setAmountCurrency($amountCurrency = null)
    {
        // validation for constraint: string
        if (!is_null($amountCurrency) && !is_string($amountCurrency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($amountCurrency, true), gettype($amountCurrency)), __LINE__);
        }
        if (is_null($amountCurrency) || (is_array($amountCurrency) && empty($amountCurrency))) {
            unset($this->amountCurrency);
        } else {
            $this->amountCurrency = $amountCurrency;
        }
        return $this;
    }
    /**
     * Get franshize value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFranshize()
    {
        return isset($this->franshize) ? $this->franshize : null;
    }
    /**
     * Set franshize value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $franshize
     * @return \RFD\AlfaStrahInsurance\StructType\PolicyRisk
     */
    public function setFranshize($franshize = null)
    {
        // validation for constraint: string
        if (!is_null($franshize) && !is_string($franshize)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($franshize, true), gettype($franshize)), __LINE__);
        }
        if (is_null($franshize) || (is_array($franshize) && empty($franshize))) {
            unset($this->franshize);
        } else {
            $this->franshize = $franshize;
        }
        return $this;
    }
    /**
     * Get insAdress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getInsAdress()
    {
        return isset($this->insAdress) ? $this->insAdress : null;
    }
    /**
     * Set insAdress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $insAdress
     * @return \RFD\AlfaStrahInsurance\StructType\PolicyRisk
     */
    public function setInsAdress($insAdress = null)
    {
        // validation for constraint: string
        if (!is_null($insAdress) && !is_string($insAdress)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($insAdress, true), gettype($insAdress)), __LINE__);
        }
        if (is_null($insAdress) || (is_array($insAdress) && empty($insAdress))) {
            unset($this->insAdress);
        } else {
            $this->insAdress = $insAdress;
        }
        return $this;
    }
    /**
     * Get premCurrency value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getPremCurrency()
    {
        return isset($this->premCurrency) ? $this->premCurrency : null;
    }
    /**
     * Set premCurrency value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $premCurrency
     * @return \RFD\AlfaStrahInsurance\StructType\PolicyRisk
     */
    public function setPremCurrency($premCurrency = null)
    {
        // validation for constraint: float
        if (!is_null($premCurrency) && !(is_float($premCurrency) || is_numeric($premCurrency))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($premCurrency, true), gettype($premCurrency)), __LINE__);
        }
        if (is_null($premCurrency) || (is_array($premCurrency) && empty($premCurrency))) {
            unset($this->premCurrency);
        } else {
            $this->premCurrency = $premCurrency;
        }
        return $this;
    }
    /**
     * Get premRUR value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getPremRUR()
    {
        return isset($this->premRUR) ? $this->premRUR : null;
    }
    /**
     * Set premRUR value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $premRUR
     * @return \RFD\AlfaStrahInsurance\StructType\PolicyRisk
     */
    public function setPremRUR($premRUR = null)
    {
        // validation for constraint: float
        if (!is_null($premRUR) && !(is_float($premRUR) || is_numeric($premRUR))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($premRUR, true), gettype($premRUR)), __LINE__);
        }
        if (is_null($premRUR) || (is_array($premRUR) && empty($premRUR))) {
            unset($this->premRUR);
        } else {
            $this->premRUR = $premRUR;
        }
        return $this;
    }
    /**
     * Get riskName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRiskName()
    {
        return isset($this->riskName) ? $this->riskName : null;
    }
    /**
     * Set riskName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $riskName
     * @return \RFD\AlfaStrahInsurance\StructType\PolicyRisk
     */
    public function setRiskName($riskName = null)
    {
        // validation for constraint: string
        if (!is_null($riskName) && !is_string($riskName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($riskName, true), gettype($riskName)), __LINE__);
        }
        if (is_null($riskName) || (is_array($riskName) && empty($riskName))) {
            unset($this->riskName);
        } else {
            $this->riskName = $riskName;
        }
        return $this;
    }
    /**
     * Get riskUID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRiskUID()
    {
        return isset($this->riskUID) ? $this->riskUID : null;
    }
    /**
     * Set riskUID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $riskUID
     * @return \RFD\AlfaStrahInsurance\StructType\PolicyRisk
     */
    public function setRiskUID($riskUID = null)
    {
        // validation for constraint: string
        if (!is_null($riskUID) && !is_string($riskUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($riskUID, true), gettype($riskUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($riskUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $riskUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($riskUID, true)), __LINE__);
        }
        if (is_null($riskUID) || (is_array($riskUID) && empty($riskUID))) {
            unset($this->riskUID);
        } else {
            $this->riskUID = $riskUID;
        }
        return $this;
    }
    /**
     * Get riskVariantName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRiskVariantName()
    {
        return isset($this->riskVariantName) ? $this->riskVariantName : null;
    }
    /**
     * Set riskVariantName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $riskVariantName
     * @return \RFD\AlfaStrahInsurance\StructType\PolicyRisk
     */
    public function setRiskVariantName($riskVariantName = null)
    {
        // validation for constraint: string
        if (!is_null($riskVariantName) && !is_string($riskVariantName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($riskVariantName, true), gettype($riskVariantName)), __LINE__);
        }
        if (is_null($riskVariantName) || (is_array($riskVariantName) && empty($riskVariantName))) {
            unset($this->riskVariantName);
        } else {
            $this->riskVariantName = $riskVariantName;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\PolicyRisk
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
