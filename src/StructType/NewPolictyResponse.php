<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for NewPolictyResponse StructType
 * @subpackage Structs
 */
class NewPolictyResponse extends AbstractStructBase
{
    /**
     * The NewPolictyResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public $NewPolictyResult;
    /**
     * Constructor method for NewPolictyResponse
     * @uses NewPolictyResponse::setNewPolictyResult()
     * @param \RFD\AlfaStrahInsurance\StructType\Policy $newPolictyResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\StructType\Policy $newPolictyResult = null)
    {
        $this
            ->setNewPolictyResult($newPolictyResult);
    }
    /**
     * Get NewPolictyResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Policy|null
     */
    public function getNewPolictyResult()
    {
        return isset($this->NewPolictyResult) ? $this->NewPolictyResult : null;
    }
    /**
     * Set NewPolictyResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\Policy $newPolictyResult
     * @return \RFD\AlfaStrahInsurance\StructType\NewPolictyResponse
     */
    public function setNewPolictyResult(\RFD\AlfaStrahInsurance\StructType\Policy $newPolictyResult = null)
    {
        if (is_null($newPolictyResult) || (is_array($newPolictyResult) && empty($newPolictyResult))) {
            unset($this->NewPolictyResult);
        } else {
            $this->NewPolictyResult = $newPolictyResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\NewPolictyResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
