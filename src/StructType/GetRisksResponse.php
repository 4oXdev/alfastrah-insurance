<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetRisksResponse StructType
 * @subpackage Structs
 */
class GetRisksResponse extends AbstractStructBase
{
    /**
     * The GetRisksResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\ArrayType\ArrayOfrisk
     */
    public $GetRisksResult;
    /**
     * Constructor method for GetRisksResponse
     * @uses GetRisksResponse::setGetRisksResult()
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfrisk $getRisksResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfrisk $getRisksResult = null)
    {
        $this
            ->setGetRisksResult($getRisksResult);
    }
    /**
     * Get GetRisksResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfrisk|null
     */
    public function getGetRisksResult()
    {
        return isset($this->GetRisksResult) ? $this->GetRisksResult : null;
    }
    /**
     * Set GetRisksResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfrisk $getRisksResult
     * @return \RFD\AlfaStrahInsurance\StructType\GetRisksResponse
     */
    public function setGetRisksResult(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfrisk $getRisksResult = null)
    {
        if (is_null($getRisksResult) || (is_array($getRisksResult) && empty($getRisksResult))) {
            unset($this->GetRisksResult);
        } else {
            $this->GetRisksResult = $getRisksResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetRisksResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
