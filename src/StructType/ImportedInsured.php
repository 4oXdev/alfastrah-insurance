<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for importedInsured StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q113:importedInsured
 * @subpackage Structs
 */
class ImportedInsured extends AbstractStructBase
{
    /**
     * The dateOfBirth
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $dateOfBirth;
    /**
     * The fio
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $fio;
    /**
     * The passport
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $passport;
    /**
     * Constructor method for importedInsured
     * @uses ImportedInsured::setDateOfBirth()
     * @uses ImportedInsured::setFio()
     * @uses ImportedInsured::setPassport()
     * @param string $dateOfBirth
     * @param string $fio
     * @param string $passport
     */
    public function __construct($dateOfBirth = null, $fio = null, $passport = null)
    {
        $this
            ->setDateOfBirth($dateOfBirth)
            ->setFio($fio)
            ->setPassport($passport);
    }
    /**
     * Get dateOfBirth value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDateOfBirth()
    {
        return isset($this->dateOfBirth) ? $this->dateOfBirth : null;
    }
    /**
     * Set dateOfBirth value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $dateOfBirth
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedInsured
     */
    public function setDateOfBirth($dateOfBirth = null)
    {
        // validation for constraint: string
        if (!is_null($dateOfBirth) && !is_string($dateOfBirth)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateOfBirth, true), gettype($dateOfBirth)), __LINE__);
        }
        if (is_null($dateOfBirth) || (is_array($dateOfBirth) && empty($dateOfBirth))) {
            unset($this->dateOfBirth);
        } else {
            $this->dateOfBirth = $dateOfBirth;
        }
        return $this;
    }
    /**
     * Get fio value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFio()
    {
        return isset($this->fio) ? $this->fio : null;
    }
    /**
     * Set fio value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $fio
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedInsured
     */
    public function setFio($fio = null)
    {
        // validation for constraint: string
        if (!is_null($fio) && !is_string($fio)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fio, true), gettype($fio)), __LINE__);
        }
        if (is_null($fio) || (is_array($fio) && empty($fio))) {
            unset($this->fio);
        } else {
            $this->fio = $fio;
        }
        return $this;
    }
    /**
     * Get passport value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPassport()
    {
        return isset($this->passport) ? $this->passport : null;
    }
    /**
     * Set passport value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $passport
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedInsured
     */
    public function setPassport($passport = null)
    {
        // validation for constraint: string
        if (!is_null($passport) && !is_string($passport)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($passport, true), gettype($passport)), __LINE__);
        }
        if (is_null($passport) || (is_array($passport) && empty($passport))) {
            unset($this->passport);
        } else {
            $this->passport = $passport;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\ImportedInsured
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
