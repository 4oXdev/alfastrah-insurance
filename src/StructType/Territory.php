<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for territory StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q68:territory
 * @subpackage Structs
 */
class Territory extends BaseDictionary
{
    /**
     * The territory
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $territory;
    /**
     * The territoryENG
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $territoryENG;
    /**
     * The territoryRUR
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $territoryRUR;
    /**
     * Constructor method for territory
     * @uses Territory::setTerritory()
     * @uses Territory::setTerritoryENG()
     * @uses Territory::setTerritoryRUR()
     * @param string $territory
     * @param string $territoryENG
     * @param string $territoryRUR
     */
    public function __construct($territory = null, $territoryENG = null, $territoryRUR = null)
    {
        $this
            ->setTerritory($territory)
            ->setTerritoryENG($territoryENG)
            ->setTerritoryRUR($territoryRUR);
    }
    /**
     * Get territory value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTerritory()
    {
        return isset($this->territory) ? $this->territory : null;
    }
    /**
     * Set territory value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $territory
     * @return \RFD\AlfaStrahInsurance\StructType\Territory
     */
    public function setTerritory($territory = null)
    {
        // validation for constraint: string
        if (!is_null($territory) && !is_string($territory)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($territory, true), gettype($territory)), __LINE__);
        }
        if (is_null($territory) || (is_array($territory) && empty($territory))) {
            unset($this->territory);
        } else {
            $this->territory = $territory;
        }
        return $this;
    }
    /**
     * Get territoryENG value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTerritoryENG()
    {
        return isset($this->territoryENG) ? $this->territoryENG : null;
    }
    /**
     * Set territoryENG value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $territoryENG
     * @return \RFD\AlfaStrahInsurance\StructType\Territory
     */
    public function setTerritoryENG($territoryENG = null)
    {
        // validation for constraint: string
        if (!is_null($territoryENG) && !is_string($territoryENG)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($territoryENG, true), gettype($territoryENG)), __LINE__);
        }
        if (is_null($territoryENG) || (is_array($territoryENG) && empty($territoryENG))) {
            unset($this->territoryENG);
        } else {
            $this->territoryENG = $territoryENG;
        }
        return $this;
    }
    /**
     * Get territoryRUR value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTerritoryRUR()
    {
        return isset($this->territoryRUR) ? $this->territoryRUR : null;
    }
    /**
     * Set territoryRUR value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $territoryRUR
     * @return \RFD\AlfaStrahInsurance\StructType\Territory
     */
    public function setTerritoryRUR($territoryRUR = null)
    {
        // validation for constraint: string
        if (!is_null($territoryRUR) && !is_string($territoryRUR)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($territoryRUR, true), gettype($territoryRUR)), __LINE__);
        }
        if (is_null($territoryRUR) || (is_array($territoryRUR) && empty($territoryRUR))) {
            unset($this->territoryRUR);
        } else {
            $this->territoryRUR = $territoryRUR;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\Territory
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
