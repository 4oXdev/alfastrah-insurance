<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SetCancelPolicyResponse StructType
 * @subpackage Structs
 */
class SetCancelPolicyResponse extends AbstractStructBase
{
    /**
     * The SetCancelPolicyResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public $SetCancelPolicyResult;
    /**
     * Constructor method for SetCancelPolicyResponse
     * @uses SetCancelPolicyResponse::setSetCancelPolicyResult()
     * @param \RFD\AlfaStrahInsurance\StructType\Policy $setCancelPolicyResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\StructType\Policy $setCancelPolicyResult = null)
    {
        $this
            ->setSetCancelPolicyResult($setCancelPolicyResult);
    }
    /**
     * Get SetCancelPolicyResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Policy|null
     */
    public function getSetCancelPolicyResult()
    {
        return isset($this->SetCancelPolicyResult) ? $this->SetCancelPolicyResult : null;
    }
    /**
     * Set SetCancelPolicyResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\Policy $setCancelPolicyResult
     * @return \RFD\AlfaStrahInsurance\StructType\SetCancelPolicyResponse
     */
    public function setSetCancelPolicyResult(\RFD\AlfaStrahInsurance\StructType\Policy $setCancelPolicyResult = null)
    {
        if (is_null($setCancelPolicyResult) || (is_array($setCancelPolicyResult) && empty($setCancelPolicyResult))) {
            unset($this->SetCancelPolicyResult);
        } else {
            $this->SetCancelPolicyResult = $setCancelPolicyResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\SetCancelPolicyResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
