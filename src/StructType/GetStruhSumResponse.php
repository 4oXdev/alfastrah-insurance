<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetStruhSumResponse StructType
 * @subpackage Structs
 */
class GetStruhSumResponse extends AbstractStructBase
{
    /**
     * The GetStruhSumResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\ArrayType\ArrayOfstruhSum
     */
    public $GetStruhSumResult;
    /**
     * Constructor method for GetStruhSumResponse
     * @uses GetStruhSumResponse::setGetStruhSumResult()
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfstruhSum $getStruhSumResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfstruhSum $getStruhSumResult = null)
    {
        $this
            ->setGetStruhSumResult($getStruhSumResult);
    }
    /**
     * Get GetStruhSumResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfstruhSum|null
     */
    public function getGetStruhSumResult()
    {
        return isset($this->GetStruhSumResult) ? $this->GetStruhSumResult : null;
    }
    /**
     * Set GetStruhSumResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfstruhSum $getStruhSumResult
     * @return \RFD\AlfaStrahInsurance\StructType\GetStruhSumResponse
     */
    public function setGetStruhSumResult(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfstruhSum $getStruhSumResult = null)
    {
        if (is_null($getStruhSumResult) || (is_array($getStruhSumResult) && empty($getStruhSumResult))) {
            unset($this->GetStruhSumResult);
        } else {
            $this->GetStruhSumResult = $getStruhSumResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetStruhSumResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
