<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for regions StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q91:regions
 * @subpackage Structs
 */
class Regions extends AbstractStructBase
{
    /**
     * The region
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var string[]
     */
    public $region;
    /**
     * Constructor method for regions
     * @uses Regions::setRegion()
     * @param string[] $region
     */
    public function __construct(array $region = array())
    {
        $this
            ->setRegion($region);
    }
    /**
     * Get region value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string[]|null
     */
    public function getRegion()
    {
        return isset($this->region) ? $this->region : null;
    }
    /**
     * This method is responsible for validating the values passed to the setRegion method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRegion method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRegionForArrayConstraintsFromSetRegion(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $regionsRegionItem) {
            // validation for constraint: itemType
            if (!is_string($regionsRegionItem)) {
                $invalidValues[] = is_object($regionsRegionItem) ? get_class($regionsRegionItem) : sprintf('%s(%s)', gettype($regionsRegionItem), var_export($regionsRegionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The region property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set region value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param string[] $region
     * @return \RFD\AlfaStrahInsurance\StructType\Regions
     */
    public function setRegion(array $region = array())
    {
        // validation for constraint: array
        if ('' !== ($regionArrayErrorMessage = self::validateRegionForArrayConstraintsFromSetRegion($region))) {
            throw new \InvalidArgumentException($regionArrayErrorMessage, __LINE__);
        }
        if (is_null($region) || (is_array($region) && empty($region))) {
            unset($this->region);
        } else {
            $this->region = $region;
        }
        return $this;
    }
    /**
     * Add item to region value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \RFD\AlfaStrahInsurance\StructType\Regions
     */
    public function addToRegion($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The region property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->region[] = $item;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\Regions
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
