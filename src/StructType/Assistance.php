<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for assistance StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q18:assistance
 * @subpackage Structs
 */
class Assistance extends BaseDictionary
{
    /**
     * The assistanceCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $assistanceCode;
    /**
     * The assistanceName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $assistanceName;
    /**
     * The assistancePhones
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $assistancePhones;
    /**
     * The assistanteID
     * @var int
     */
    public $assistanteID;
    /**
     * The assistanteUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $assistanteUID;
    /**
     * Constructor method for assistance
     * @uses Assistance::setAssistanceCode()
     * @uses Assistance::setAssistanceName()
     * @uses Assistance::setAssistancePhones()
     * @uses Assistance::setAssistanteID()
     * @uses Assistance::setAssistanteUID()
     * @param string $assistanceCode
     * @param string $assistanceName
     * @param string $assistancePhones
     * @param int $assistanteID
     * @param string $assistanteUID
     */
    public function __construct($assistanceCode = null, $assistanceName = null, $assistancePhones = null, $assistanteID = null, $assistanteUID = null)
    {
        $this
            ->setAssistanceCode($assistanceCode)
            ->setAssistanceName($assistanceName)
            ->setAssistancePhones($assistancePhones)
            ->setAssistanteID($assistanteID)
            ->setAssistanteUID($assistanteUID);
    }
    /**
     * Get assistanceCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAssistanceCode()
    {
        return isset($this->assistanceCode) ? $this->assistanceCode : null;
    }
    /**
     * Set assistanceCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $assistanceCode
     * @return \RFD\AlfaStrahInsurance\StructType\Assistance
     */
    public function setAssistanceCode($assistanceCode = null)
    {
        // validation for constraint: string
        if (!is_null($assistanceCode) && !is_string($assistanceCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($assistanceCode, true), gettype($assistanceCode)), __LINE__);
        }
        if (is_null($assistanceCode) || (is_array($assistanceCode) && empty($assistanceCode))) {
            unset($this->assistanceCode);
        } else {
            $this->assistanceCode = $assistanceCode;
        }
        return $this;
    }
    /**
     * Get assistanceName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAssistanceName()
    {
        return isset($this->assistanceName) ? $this->assistanceName : null;
    }
    /**
     * Set assistanceName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $assistanceName
     * @return \RFD\AlfaStrahInsurance\StructType\Assistance
     */
    public function setAssistanceName($assistanceName = null)
    {
        // validation for constraint: string
        if (!is_null($assistanceName) && !is_string($assistanceName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($assistanceName, true), gettype($assistanceName)), __LINE__);
        }
        if (is_null($assistanceName) || (is_array($assistanceName) && empty($assistanceName))) {
            unset($this->assistanceName);
        } else {
            $this->assistanceName = $assistanceName;
        }
        return $this;
    }
    /**
     * Get assistancePhones value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAssistancePhones()
    {
        return isset($this->assistancePhones) ? $this->assistancePhones : null;
    }
    /**
     * Set assistancePhones value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $assistancePhones
     * @return \RFD\AlfaStrahInsurance\StructType\Assistance
     */
    public function setAssistancePhones($assistancePhones = null)
    {
        // validation for constraint: string
        if (!is_null($assistancePhones) && !is_string($assistancePhones)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($assistancePhones, true), gettype($assistancePhones)), __LINE__);
        }
        if (is_null($assistancePhones) || (is_array($assistancePhones) && empty($assistancePhones))) {
            unset($this->assistancePhones);
        } else {
            $this->assistancePhones = $assistancePhones;
        }
        return $this;
    }
    /**
     * Get assistanteID value
     * @return int|null
     */
    public function getAssistanteID()
    {
        return $this->assistanteID;
    }
    /**
     * Set assistanteID value
     * @param int $assistanteID
     * @return \RFD\AlfaStrahInsurance\StructType\Assistance
     */
    public function setAssistanteID($assistanteID = null)
    {
        // validation for constraint: int
        if (!is_null($assistanteID) && !(is_int($assistanteID) || ctype_digit($assistanteID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($assistanteID, true), gettype($assistanteID)), __LINE__);
        }
        $this->assistanteID = $assistanteID;
        return $this;
    }
    /**
     * Get assistanteUID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAssistanteUID()
    {
        return isset($this->assistanteUID) ? $this->assistanteUID : null;
    }
    /**
     * Set assistanteUID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $assistanteUID
     * @return \RFD\AlfaStrahInsurance\StructType\Assistance
     */
    public function setAssistanteUID($assistanteUID = null)
    {
        // validation for constraint: string
        if (!is_null($assistanteUID) && !is_string($assistanteUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($assistanteUID, true), gettype($assistanteUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($assistanteUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $assistanteUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($assistanteUID, true)), __LINE__);
        }
        if (is_null($assistanteUID) || (is_array($assistanteUID) && empty($assistanteUID))) {
            unset($this->assistanteUID);
        } else {
            $this->assistanteUID = $assistanteUID;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\Assistance
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
