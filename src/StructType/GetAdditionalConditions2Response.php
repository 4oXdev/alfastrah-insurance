<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAdditionalConditions2Response StructType
 * @subpackage Structs
 */
class GetAdditionalConditions2Response extends AbstractStructBase
{
    /**
     * The GetAdditionalConditions2Result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition
     */
    public $GetAdditionalConditions2Result;
    /**
     * Constructor method for GetAdditionalConditions2Response
     * @uses GetAdditionalConditions2Response::setGetAdditionalConditions2Result()
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition $getAdditionalConditions2Result
     */
    public function __construct(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition $getAdditionalConditions2Result = null)
    {
        $this
            ->setGetAdditionalConditions2Result($getAdditionalConditions2Result);
    }
    /**
     * Get GetAdditionalConditions2Result value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition|null
     */
    public function getGetAdditionalConditions2Result()
    {
        return isset($this->GetAdditionalConditions2Result) ? $this->GetAdditionalConditions2Result : null;
    }
    /**
     * Set GetAdditionalConditions2Result value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition $getAdditionalConditions2Result
     * @return \RFD\AlfaStrahInsurance\StructType\GetAdditionalConditions2Response
     */
    public function setGetAdditionalConditions2Result(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfadditionalCondition $getAdditionalConditions2Result = null)
    {
        if (is_null($getAdditionalConditions2Result) || (is_array($getAdditionalConditions2Result) && empty($getAdditionalConditions2Result))) {
            unset($this->GetAdditionalConditions2Result);
        } else {
            $this->GetAdditionalConditions2Result = $getAdditionalConditions2Result;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetAdditionalConditions2Response
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
