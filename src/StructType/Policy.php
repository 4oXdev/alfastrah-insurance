<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for policy StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q82:policy
 * @subpackage Structs
 */
class Policy extends AbstractStructBase
{
    /**
     * The additionalConditions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\AdditionalConditions
     */
    public $additionalConditions;
    /**
     * The common
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Common
     */
    public $common;
    /**
     * The countries
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Countries
     */
    public $countries;
    /**
     * The countryUIDs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\CountryUIDs
     */
    public $countryUIDs;
    /**
     * The insureds
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Insureds
     */
    public $insureds;
    /**
     * The regionsExcl
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Regions
     */
    public $regionsExcl;
    /**
     * The regionsIncl
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Regions
     */
    public $regionsIncl;
    /**
     * The risks
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Risks
     */
    public $risks;
    /**
     * Constructor method for policy
     * @uses Policy::setAdditionalConditions()
     * @uses Policy::setCommon()
     * @uses Policy::setCountries()
     * @uses Policy::setCountryUIDs()
     * @uses Policy::setInsureds()
     * @uses Policy::setRegionsExcl()
     * @uses Policy::setRegionsIncl()
     * @uses Policy::setRisks()
     * @param \RFD\AlfaStrahInsurance\StructType\AdditionalConditions $additionalConditions
     * @param \RFD\AlfaStrahInsurance\StructType\Common $common
     * @param \RFD\AlfaStrahInsurance\StructType\Countries $countries
     * @param \RFD\AlfaStrahInsurance\StructType\CountryUIDs $countryUIDs
     * @param \RFD\AlfaStrahInsurance\StructType\Insureds $insureds
     * @param \RFD\AlfaStrahInsurance\StructType\Regions $regionsExcl
     * @param \RFD\AlfaStrahInsurance\StructType\Regions $regionsIncl
     * @param \RFD\AlfaStrahInsurance\StructType\Risks $risks
     */
    public function __construct(\RFD\AlfaStrahInsurance\StructType\AdditionalConditions $additionalConditions = null, \RFD\AlfaStrahInsurance\StructType\Common $common = null, \RFD\AlfaStrahInsurance\StructType\Countries $countries = null, \RFD\AlfaStrahInsurance\StructType\CountryUIDs $countryUIDs = null, \RFD\AlfaStrahInsurance\StructType\Insureds $insureds = null, \RFD\AlfaStrahInsurance\StructType\Regions $regionsExcl = null, \RFD\AlfaStrahInsurance\StructType\Regions $regionsIncl = null, \RFD\AlfaStrahInsurance\StructType\Risks $risks = null)
    {
        $this
            ->setAdditionalConditions($additionalConditions)
            ->setCommon($common)
            ->setCountries($countries)
            ->setCountryUIDs($countryUIDs)
            ->setInsureds($insureds)
            ->setRegionsExcl($regionsExcl)
            ->setRegionsIncl($regionsIncl)
            ->setRisks($risks);
    }
    /**
     * Get additionalConditions value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\AdditionalConditions|null
     */
    public function getAdditionalConditions()
    {
        return isset($this->additionalConditions) ? $this->additionalConditions : null;
    }
    /**
     * Set additionalConditions value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\AdditionalConditions $additionalConditions
     * @return \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public function setAdditionalConditions(\RFD\AlfaStrahInsurance\StructType\AdditionalConditions $additionalConditions = null)
    {
        if (is_null($additionalConditions) || (is_array($additionalConditions) && empty($additionalConditions))) {
            unset($this->additionalConditions);
        } else {
            $this->additionalConditions = $additionalConditions;
        }
        return $this;
    }
    /**
     * Get common value
     * @return \RFD\AlfaStrahInsurance\StructType\Common|null
     */
    public function getCommon()
    {
        return $this->common;
    }
    /**
     * Set common value
     * @param \RFD\AlfaStrahInsurance\StructType\Common $common
     * @return \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public function setCommon(\RFD\AlfaStrahInsurance\StructType\Common $common = null)
    {
        $this->common = $common;
        return $this;
    }
    /**
     * Get countries value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Countries|null
     */
    public function getCountries()
    {
        return isset($this->countries) ? $this->countries : null;
    }
    /**
     * Set countries value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\Countries $countries
     * @return \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public function setCountries(\RFD\AlfaStrahInsurance\StructType\Countries $countries = null)
    {
        if (is_null($countries) || (is_array($countries) && empty($countries))) {
            unset($this->countries);
        } else {
            $this->countries = $countries;
        }
        return $this;
    }
    /**
     * Get countryUIDs value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\CountryUIDs|null
     */
    public function getCountryUIDs()
    {
        return isset($this->countryUIDs) ? $this->countryUIDs : null;
    }
    /**
     * Set countryUIDs value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\CountryUIDs $countryUIDs
     * @return \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public function setCountryUIDs(\RFD\AlfaStrahInsurance\StructType\CountryUIDs $countryUIDs = null)
    {
        if (is_null($countryUIDs) || (is_array($countryUIDs) && empty($countryUIDs))) {
            unset($this->countryUIDs);
        } else {
            $this->countryUIDs = $countryUIDs;
        }
        return $this;
    }
    /**
     * Get insureds value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Insureds|null
     */
    public function getInsureds()
    {
        return isset($this->insureds) ? $this->insureds : null;
    }
    /**
     * Set insureds value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\Insureds $insureds
     * @return \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public function setInsureds(\RFD\AlfaStrahInsurance\StructType\Insureds $insureds = null)
    {
        if (is_null($insureds) || (is_array($insureds) && empty($insureds))) {
            unset($this->insureds);
        } else {
            $this->insureds = $insureds;
        }
        return $this;
    }
    /**
     * Get regionsExcl value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Regions|null
     */
    public function getRegionsExcl()
    {
        return isset($this->regionsExcl) ? $this->regionsExcl : null;
    }
    /**
     * Set regionsExcl value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\Regions $regionsExcl
     * @return \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public function setRegionsExcl(\RFD\AlfaStrahInsurance\StructType\Regions $regionsExcl = null)
    {
        if (is_null($regionsExcl) || (is_array($regionsExcl) && empty($regionsExcl))) {
            unset($this->regionsExcl);
        } else {
            $this->regionsExcl = $regionsExcl;
        }
        return $this;
    }
    /**
     * Get regionsIncl value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Regions|null
     */
    public function getRegionsIncl()
    {
        return isset($this->regionsIncl) ? $this->regionsIncl : null;
    }
    /**
     * Set regionsIncl value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\Regions $regionsIncl
     * @return \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public function setRegionsIncl(\RFD\AlfaStrahInsurance\StructType\Regions $regionsIncl = null)
    {
        if (is_null($regionsIncl) || (is_array($regionsIncl) && empty($regionsIncl))) {
            unset($this->regionsIncl);
        } else {
            $this->regionsIncl = $regionsIncl;
        }
        return $this;
    }
    /**
     * Get risks value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Risks|null
     */
    public function getRisks()
    {
        return isset($this->risks) ? $this->risks : null;
    }
    /**
     * Set risks value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\Risks $risks
     * @return \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public function setRisks(\RFD\AlfaStrahInsurance\StructType\Risks $risks = null)
    {
        if (is_null($risks) || (is_array($risks) && empty($risks))) {
            unset($this->risks);
        } else {
            $this->risks = $risks;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
