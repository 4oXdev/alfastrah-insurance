<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for risks StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q93:risks
 * @subpackage Structs
 */
class Risks extends AbstractStructBase
{
    /**
     * The risk
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\PolicyRisk[]
     */
    public $risk;
    /**
     * Constructor method for risks
     * @uses Risks::setRisk()
     * @param \RFD\AlfaStrahInsurance\StructType\PolicyRisk[] $risk
     */
    public function __construct(array $risk = array())
    {
        $this
            ->setRisk($risk);
    }
    /**
     * Get risk value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\PolicyRisk[]|null
     */
    public function getRisk()
    {
        return isset($this->risk) ? $this->risk : null;
    }
    /**
     * This method is responsible for validating the values passed to the setRisk method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRisk method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRiskForArrayConstraintsFromSetRisk(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $risksRiskItem) {
            // validation for constraint: itemType
            if (!$risksRiskItem instanceof \RFD\AlfaStrahInsurance\StructType\PolicyRisk) {
                $invalidValues[] = is_object($risksRiskItem) ? get_class($risksRiskItem) : sprintf('%s(%s)', gettype($risksRiskItem), var_export($risksRiskItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The risk property can only contain items of type \RFD\AlfaStrahInsurance\StructType\PolicyRisk, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set risk value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\PolicyRisk[] $risk
     * @return \RFD\AlfaStrahInsurance\StructType\Risks
     */
    public function setRisk(array $risk = array())
    {
        // validation for constraint: array
        if ('' !== ($riskArrayErrorMessage = self::validateRiskForArrayConstraintsFromSetRisk($risk))) {
            throw new \InvalidArgumentException($riskArrayErrorMessage, __LINE__);
        }
        if (is_null($risk) || (is_array($risk) && empty($risk))) {
            unset($this->risk);
        } else {
            $this->risk = $risk;
        }
        return $this;
    }
    /**
     * Add item to risk value
     * @throws \InvalidArgumentException
     * @param \RFD\AlfaStrahInsurance\StructType\PolicyRisk $item
     * @return \RFD\AlfaStrahInsurance\StructType\Risks
     */
    public function addToRisk(\RFD\AlfaStrahInsurance\StructType\PolicyRisk $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \RFD\AlfaStrahInsurance\StructType\PolicyRisk) {
            throw new \InvalidArgumentException(sprintf('The risk property can only contain items of type \RFD\AlfaStrahInsurance\StructType\PolicyRisk, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->risk[] = $item;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\Risks
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
