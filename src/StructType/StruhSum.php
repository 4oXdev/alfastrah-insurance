<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for struhSum StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q38:struhSum
 * @subpackage Structs
 */
class StruhSum extends BaseDictionary
{
    /**
     * The risk
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $risk;
    /**
     * The riskID
     * @var int
     */
    public $riskID;
    /**
     * The riskUID
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $riskUID;
    /**
     * The strahSummFrom
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $strahSummFrom;
    /**
     * The strahSummTo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $strahSummTo;
    /**
     * The territory
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $territory;
    /**
     * The territoryID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $territoryID;
    /**
     * The valutaCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $valutaCode;
    /**
     * The variant
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $variant;
    /**
     * The variantUid
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - minOccurs: 0
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $variantUid;
    /**
     * Constructor method for struhSum
     * @uses StruhSum::setRisk()
     * @uses StruhSum::setRiskID()
     * @uses StruhSum::setRiskUID()
     * @uses StruhSum::setStrahSummFrom()
     * @uses StruhSum::setStrahSummTo()
     * @uses StruhSum::setTerritory()
     * @uses StruhSum::setTerritoryID()
     * @uses StruhSum::setValutaCode()
     * @uses StruhSum::setVariant()
     * @uses StruhSum::setVariantUid()
     * @param string $risk
     * @param int $riskID
     * @param string $riskUID
     * @param int $strahSummFrom
     * @param int $strahSummTo
     * @param string $territory
     * @param int $territoryID
     * @param string $valutaCode
     * @param string $variant
     * @param string $variantUid
     */
    public function __construct($risk = null, $riskID = null, $riskUID = null, $strahSummFrom = null, $strahSummTo = null, $territory = null, $territoryID = null, $valutaCode = null, $variant = null, $variantUid = null)
    {
        $this
            ->setRisk($risk)
            ->setRiskID($riskID)
            ->setRiskUID($riskUID)
            ->setStrahSummFrom($strahSummFrom)
            ->setStrahSummTo($strahSummTo)
            ->setTerritory($territory)
            ->setTerritoryID($territoryID)
            ->setValutaCode($valutaCode)
            ->setVariant($variant)
            ->setVariantUid($variantUid);
    }
    /**
     * Get risk value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRisk()
    {
        return isset($this->risk) ? $this->risk : null;
    }
    /**
     * Set risk value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $risk
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum
     */
    public function setRisk($risk = null)
    {
        // validation for constraint: string
        if (!is_null($risk) && !is_string($risk)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($risk, true), gettype($risk)), __LINE__);
        }
        if (is_null($risk) || (is_array($risk) && empty($risk))) {
            unset($this->risk);
        } else {
            $this->risk = $risk;
        }
        return $this;
    }
    /**
     * Get riskID value
     * @return int|null
     */
    public function getRiskID()
    {
        return $this->riskID;
    }
    /**
     * Set riskID value
     * @param int $riskID
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum
     */
    public function setRiskID($riskID = null)
    {
        // validation for constraint: int
        if (!is_null($riskID) && !(is_int($riskID) || ctype_digit($riskID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($riskID, true), gettype($riskID)), __LINE__);
        }
        $this->riskID = $riskID;
        return $this;
    }
    /**
     * Get riskUID value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRiskUID()
    {
        return isset($this->riskUID) ? $this->riskUID : null;
    }
    /**
     * Set riskUID value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $riskUID
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum
     */
    public function setRiskUID($riskUID = null)
    {
        // validation for constraint: string
        if (!is_null($riskUID) && !is_string($riskUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($riskUID, true), gettype($riskUID)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($riskUID) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $riskUID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($riskUID, true)), __LINE__);
        }
        if (is_null($riskUID) || (is_array($riskUID) && empty($riskUID))) {
            unset($this->riskUID);
        } else {
            $this->riskUID = $riskUID;
        }
        return $this;
    }
    /**
     * Get strahSummFrom value
     * @return int|null
     */
    public function getStrahSummFrom()
    {
        return $this->strahSummFrom;
    }
    /**
     * Set strahSummFrom value
     * @param int $strahSummFrom
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum
     */
    public function setStrahSummFrom($strahSummFrom = null)
    {
        // validation for constraint: int
        if (!is_null($strahSummFrom) && !(is_int($strahSummFrom) || ctype_digit($strahSummFrom))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($strahSummFrom, true), gettype($strahSummFrom)), __LINE__);
        }
        $this->strahSummFrom = $strahSummFrom;
        return $this;
    }
    /**
     * Get strahSummTo value
     * @return int|null
     */
    public function getStrahSummTo()
    {
        return $this->strahSummTo;
    }
    /**
     * Set strahSummTo value
     * @param int $strahSummTo
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum
     */
    public function setStrahSummTo($strahSummTo = null)
    {
        // validation for constraint: int
        if (!is_null($strahSummTo) && !(is_int($strahSummTo) || ctype_digit($strahSummTo))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($strahSummTo, true), gettype($strahSummTo)), __LINE__);
        }
        $this->strahSummTo = $strahSummTo;
        return $this;
    }
    /**
     * Get territory value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTerritory()
    {
        return isset($this->territory) ? $this->territory : null;
    }
    /**
     * Set territory value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $territory
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum
     */
    public function setTerritory($territory = null)
    {
        // validation for constraint: string
        if (!is_null($territory) && !is_string($territory)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($territory, true), gettype($territory)), __LINE__);
        }
        if (is_null($territory) || (is_array($territory) && empty($territory))) {
            unset($this->territory);
        } else {
            $this->territory = $territory;
        }
        return $this;
    }
    /**
     * Get territoryID value
     * @return int|null
     */
    public function getTerritoryID()
    {
        return $this->territoryID;
    }
    /**
     * Set territoryID value
     * @param int $territoryID
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum
     */
    public function setTerritoryID($territoryID = null)
    {
        // validation for constraint: int
        if (!is_null($territoryID) && !(is_int($territoryID) || ctype_digit($territoryID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($territoryID, true), gettype($territoryID)), __LINE__);
        }
        $this->territoryID = $territoryID;
        return $this;
    }
    /**
     * Get valutaCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getValutaCode()
    {
        return isset($this->valutaCode) ? $this->valutaCode : null;
    }
    /**
     * Set valutaCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $valutaCode
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum
     */
    public function setValutaCode($valutaCode = null)
    {
        // validation for constraint: string
        if (!is_null($valutaCode) && !is_string($valutaCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($valutaCode, true), gettype($valutaCode)), __LINE__);
        }
        if (is_null($valutaCode) || (is_array($valutaCode) && empty($valutaCode))) {
            unset($this->valutaCode);
        } else {
            $this->valutaCode = $valutaCode;
        }
        return $this;
    }
    /**
     * Get variant value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getVariant()
    {
        return isset($this->variant) ? $this->variant : null;
    }
    /**
     * Set variant value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $variant
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum
     */
    public function setVariant($variant = null)
    {
        // validation for constraint: string
        if (!is_null($variant) && !is_string($variant)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($variant, true), gettype($variant)), __LINE__);
        }
        if (is_null($variant) || (is_array($variant) && empty($variant))) {
            unset($this->variant);
        } else {
            $this->variant = $variant;
        }
        return $this;
    }
    /**
     * Get variantUid value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getVariantUid()
    {
        return isset($this->variantUid) ? $this->variantUid : null;
    }
    /**
     * Set variantUid value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $variantUid
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum
     */
    public function setVariantUid($variantUid = null)
    {
        // validation for constraint: string
        if (!is_null($variantUid) && !is_string($variantUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($variantUid, true), gettype($variantUid)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($variantUid) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $variantUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($variantUid, true)), __LINE__);
        }
        if (is_null($variantUid) || (is_array($variantUid) && empty($variantUid))) {
            unset($this->variantUid);
        } else {
            $this->variantUid = $variantUid;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\StruhSum
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
