<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PersonEditPolicyResponse StructType
 * @subpackage Structs
 */
class PersonEditPolicyResponse extends AbstractStructBase
{
    /**
     * The PersonEditPolicyResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public $PersonEditPolicyResult;
    /**
     * Constructor method for PersonEditPolicyResponse
     * @uses PersonEditPolicyResponse::setPersonEditPolicyResult()
     * @param \RFD\AlfaStrahInsurance\StructType\Policy $personEditPolicyResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\StructType\Policy $personEditPolicyResult = null)
    {
        $this
            ->setPersonEditPolicyResult($personEditPolicyResult);
    }
    /**
     * Get PersonEditPolicyResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Policy|null
     */
    public function getPersonEditPolicyResult()
    {
        return isset($this->PersonEditPolicyResult) ? $this->PersonEditPolicyResult : null;
    }
    /**
     * Set PersonEditPolicyResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\Policy $personEditPolicyResult
     * @return \RFD\AlfaStrahInsurance\StructType\PersonEditPolicyResponse
     */
    public function setPersonEditPolicyResult(\RFD\AlfaStrahInsurance\StructType\Policy $personEditPolicyResult = null)
    {
        if (is_null($personEditPolicyResult) || (is_array($personEditPolicyResult) && empty($personEditPolicyResult))) {
            unset($this->PersonEditPolicyResult);
        } else {
            $this->PersonEditPolicyResult = $personEditPolicyResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\PersonEditPolicyResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
