<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SetPayPolicyResponse StructType
 * @subpackage Structs
 */
class SetPayPolicyResponse extends AbstractStructBase
{
    /**
     * The SetPayPolicyResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\StructType\Policy
     */
    public $SetPayPolicyResult;
    /**
     * Constructor method for SetPayPolicyResponse
     * @uses SetPayPolicyResponse::setSetPayPolicyResult()
     * @param \RFD\AlfaStrahInsurance\StructType\Policy $setPayPolicyResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\StructType\Policy $setPayPolicyResult = null)
    {
        $this
            ->setSetPayPolicyResult($setPayPolicyResult);
    }
    /**
     * Get SetPayPolicyResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\StructType\Policy|null
     */
    public function getSetPayPolicyResult()
    {
        return isset($this->SetPayPolicyResult) ? $this->SetPayPolicyResult : null;
    }
    /**
     * Set SetPayPolicyResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\StructType\Policy $setPayPolicyResult
     * @return \RFD\AlfaStrahInsurance\StructType\SetPayPolicyResponse
     */
    public function setSetPayPolicyResult(\RFD\AlfaStrahInsurance\StructType\Policy $setPayPolicyResult = null)
    {
        if (is_null($setPayPolicyResult) || (is_array($setPayPolicyResult) && empty($setPayPolicyResult))) {
            unset($this->SetPayPolicyResult);
        } else {
            $this->SetPayPolicyResult = $setPayPolicyResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\SetPayPolicyResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
