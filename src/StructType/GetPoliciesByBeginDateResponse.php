<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPoliciesByBeginDateResponse StructType
 * @subpackage Structs
 */
class GetPoliciesByBeginDateResponse extends AbstractStructBase
{
    /**
     * The GetPoliciesByBeginDateResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy
     */
    public $GetPoliciesByBeginDateResult;
    /**
     * Constructor method for GetPoliciesByBeginDateResponse
     * @uses GetPoliciesByBeginDateResponse::setGetPoliciesByBeginDateResult()
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy $getPoliciesByBeginDateResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy $getPoliciesByBeginDateResult = null)
    {
        $this
            ->setGetPoliciesByBeginDateResult($getPoliciesByBeginDateResult);
    }
    /**
     * Get GetPoliciesByBeginDateResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy|null
     */
    public function getGetPoliciesByBeginDateResult()
    {
        return isset($this->GetPoliciesByBeginDateResult) ? $this->GetPoliciesByBeginDateResult : null;
    }
    /**
     * Set GetPoliciesByBeginDateResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy $getPoliciesByBeginDateResult
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesByBeginDateResponse
     */
    public function setGetPoliciesByBeginDateResult(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy $getPoliciesByBeginDateResult = null)
    {
        if (is_null($getPoliciesByBeginDateResult) || (is_array($getPoliciesByBeginDateResult) && empty($getPoliciesByBeginDateResult))) {
            unset($this->GetPoliciesByBeginDateResult);
        } else {
            $this->GetPoliciesByBeginDateResult = $getPoliciesByBeginDateResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesByBeginDateResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
