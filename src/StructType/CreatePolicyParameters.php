<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for createPolicyParameters StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: q127:createPolicyParameters
 * @subpackage Structs
 */
class CreatePolicyParameters extends AbstractStructBase
{
    /**
     * The agentLogin
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $agentLogin;
    /**
     * The agentPassword
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $agentPassword;
    /**
     * The agentUid
     * Meta information extracted from the WSDL
     * - base: xs:string
     * - nillable: true
     * - pattern: [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
     * - type: tns:guid
     * @var string
     */
    public $agentUid;
    /**
     * The dateIssue
     * @var string
     */
    public $dateIssue;
    /**
     * The failedUrl
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $failedUrl;
    /**
     * The number
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $number;
    /**
     * The returnUrl
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $returnUrl;
    /**
     * Constructor method for createPolicyParameters
     * @uses CreatePolicyParameters::setAgentLogin()
     * @uses CreatePolicyParameters::setAgentPassword()
     * @uses CreatePolicyParameters::setAgentUid()
     * @uses CreatePolicyParameters::setDateIssue()
     * @uses CreatePolicyParameters::setFailedUrl()
     * @uses CreatePolicyParameters::setNumber()
     * @uses CreatePolicyParameters::setReturnUrl()
     * @param string $agentLogin
     * @param string $agentPassword
     * @param string $agentUid
     * @param string $dateIssue
     * @param string $failedUrl
     * @param string $number
     * @param string $returnUrl
     */
    public function __construct($agentLogin = null, $agentPassword = null, $agentUid = null, $dateIssue = null, $failedUrl = null, $number = null, $returnUrl = null)
    {
        $this
            ->setAgentLogin($agentLogin)
            ->setAgentPassword($agentPassword)
            ->setAgentUid($agentUid)
            ->setDateIssue($dateIssue)
            ->setFailedUrl($failedUrl)
            ->setNumber($number)
            ->setReturnUrl($returnUrl);
    }
    /**
     * Get agentLogin value
     * @return string|null
     */
    public function getAgentLogin()
    {
        return $this->agentLogin;
    }
    /**
     * Set agentLogin value
     * @param string $agentLogin
     * @return \RFD\AlfaStrahInsurance\StructType\CreatePolicyParameters
     */
    public function setAgentLogin($agentLogin = null)
    {
        // validation for constraint: string
        if (!is_null($agentLogin) && !is_string($agentLogin)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($agentLogin, true), gettype($agentLogin)), __LINE__);
        }
        $this->agentLogin = $agentLogin;
        return $this;
    }
    /**
     * Get agentPassword value
     * @return string|null
     */
    public function getAgentPassword()
    {
        return $this->agentPassword;
    }
    /**
     * Set agentPassword value
     * @param string $agentPassword
     * @return \RFD\AlfaStrahInsurance\StructType\CreatePolicyParameters
     */
    public function setAgentPassword($agentPassword = null)
    {
        // validation for constraint: string
        if (!is_null($agentPassword) && !is_string($agentPassword)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($agentPassword, true), gettype($agentPassword)), __LINE__);
        }
        $this->agentPassword = $agentPassword;
        return $this;
    }
    /**
     * Get agentUid value
     * @return string|null
     */
    public function getAgentUid()
    {
        return $this->agentUid;
    }
    /**
     * Set agentUid value
     * @param string $agentUid
     * @return \RFD\AlfaStrahInsurance\StructType\CreatePolicyParameters
     */
    public function setAgentUid($agentUid = null)
    {
        // validation for constraint: string
        if (!is_null($agentUid) && !is_string($agentUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($agentUid, true), gettype($agentUid)), __LINE__);
        }
        // validation for constraint: pattern([\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12})
        if (!is_null($agentUid) && !preg_match('/[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}/', $agentUid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}', var_export($agentUid, true)), __LINE__);
        }
        $this->agentUid = $agentUid;
        return $this;
    }
    /**
     * Get dateIssue value
     * @return string|null
     */
    public function getDateIssue()
    {
        return $this->dateIssue;
    }
    /**
     * Set dateIssue value
     * @param string $dateIssue
     * @return \RFD\AlfaStrahInsurance\StructType\CreatePolicyParameters
     */
    public function setDateIssue($dateIssue = null)
    {
        // validation for constraint: string
        if (!is_null($dateIssue) && !is_string($dateIssue)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dateIssue, true), gettype($dateIssue)), __LINE__);
        }
        $this->dateIssue = $dateIssue;
        return $this;
    }
    /**
     * Get failedUrl value
     * @return string|null
     */
    public function getFailedUrl()
    {
        return $this->failedUrl;
    }
    /**
     * Set failedUrl value
     * @param string $failedUrl
     * @return \RFD\AlfaStrahInsurance\StructType\CreatePolicyParameters
     */
    public function setFailedUrl($failedUrl = null)
    {
        // validation for constraint: string
        if (!is_null($failedUrl) && !is_string($failedUrl)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($failedUrl, true), gettype($failedUrl)), __LINE__);
        }
        $this->failedUrl = $failedUrl;
        return $this;
    }
    /**
     * Get number value
     * @return string|null
     */
    public function getNumber()
    {
        return $this->number;
    }
    /**
     * Set number value
     * @param string $number
     * @return \RFD\AlfaStrahInsurance\StructType\CreatePolicyParameters
     */
    public function setNumber($number = null)
    {
        // validation for constraint: string
        if (!is_null($number) && !is_string($number)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($number, true), gettype($number)), __LINE__);
        }
        $this->number = $number;
        return $this;
    }
    /**
     * Get returnUrl value
     * @return string|null
     */
    public function getReturnUrl()
    {
        return $this->returnUrl;
    }
    /**
     * Set returnUrl value
     * @param string $returnUrl
     * @return \RFD\AlfaStrahInsurance\StructType\CreatePolicyParameters
     */
    public function setReturnUrl($returnUrl = null)
    {
        // validation for constraint: string
        if (!is_null($returnUrl) && !is_string($returnUrl)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnUrl, true), gettype($returnUrl)), __LINE__);
        }
        $this->returnUrl = $returnUrl;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\CreatePolicyParameters
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
