<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetCountriesResponse StructType
 * @subpackage Structs
 */
class GetCountriesResponse extends AbstractStructBase
{
    /**
     * The GetCountriesResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry
     */
    public $GetCountriesResult;
    /**
     * Constructor method for GetCountriesResponse
     * @uses GetCountriesResponse::setGetCountriesResult()
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry $getCountriesResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry $getCountriesResult = null)
    {
        $this
            ->setGetCountriesResult($getCountriesResult);
    }
    /**
     * Get GetCountriesResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry|null
     */
    public function getGetCountriesResult()
    {
        return isset($this->GetCountriesResult) ? $this->GetCountriesResult : null;
    }
    /**
     * Set GetCountriesResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry $getCountriesResult
     * @return \RFD\AlfaStrahInsurance\StructType\GetCountriesResponse
     */
    public function setGetCountriesResult(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfcountry $getCountriesResult = null)
    {
        if (is_null($getCountriesResult) || (is_array($getCountriesResult) && empty($getCountriesResult))) {
            unset($this->GetCountriesResult);
        } else {
            $this->GetCountriesResult = $getCountriesResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetCountriesResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
