<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetFransizeResponse StructType
 * @subpackage Structs
 */
class GetFransizeResponse extends AbstractStructBase
{
    /**
     * The GetFransizeResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\ArrayType\ArrayOfriskFranshize
     */
    public $GetFransizeResult;
    /**
     * Constructor method for GetFransizeResponse
     * @uses GetFransizeResponse::setGetFransizeResult()
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfriskFranshize $getFransizeResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfriskFranshize $getFransizeResult = null)
    {
        $this
            ->setGetFransizeResult($getFransizeResult);
    }
    /**
     * Get GetFransizeResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfriskFranshize|null
     */
    public function getGetFransizeResult()
    {
        return isset($this->GetFransizeResult) ? $this->GetFransizeResult : null;
    }
    /**
     * Set GetFransizeResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfriskFranshize $getFransizeResult
     * @return \RFD\AlfaStrahInsurance\StructType\GetFransizeResponse
     */
    public function setGetFransizeResult(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfriskFranshize $getFransizeResult = null)
    {
        if (is_null($getFransizeResult) || (is_array($getFransizeResult) && empty($getFransizeResult))) {
            unset($this->GetFransizeResult);
        } else {
            $this->GetFransizeResult = $getFransizeResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetFransizeResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
