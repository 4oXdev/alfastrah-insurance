<?php

namespace RFD\AlfaStrahInsurance\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetPoliciesByCreateDateResponse StructType
 * @subpackage Structs
 */
class GetPoliciesByCreateDateResponse extends AbstractStructBase
{
    /**
     * The GetPoliciesByCreateDateResult
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy
     */
    public $GetPoliciesByCreateDateResult;
    /**
     * Constructor method for GetPoliciesByCreateDateResponse
     * @uses GetPoliciesByCreateDateResponse::setGetPoliciesByCreateDateResult()
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy $getPoliciesByCreateDateResult
     */
    public function __construct(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy $getPoliciesByCreateDateResult = null)
    {
        $this
            ->setGetPoliciesByCreateDateResult($getPoliciesByCreateDateResult);
    }
    /**
     * Get GetPoliciesByCreateDateResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy|null
     */
    public function getGetPoliciesByCreateDateResult()
    {
        return isset($this->GetPoliciesByCreateDateResult) ? $this->GetPoliciesByCreateDateResult : null;
    }
    /**
     * Set GetPoliciesByCreateDateResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy $getPoliciesByCreateDateResult
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesByCreateDateResponse
     */
    public function setGetPoliciesByCreateDateResult(\RFD\AlfaStrahInsurance\ArrayType\ArrayOfpolicy $getPoliciesByCreateDateResult = null)
    {
        if (is_null($getPoliciesByCreateDateResult) || (is_array($getPoliciesByCreateDateResult) && empty($getPoliciesByCreateDateResult))) {
            unset($this->GetPoliciesByCreateDateResult);
        } else {
            $this->GetPoliciesByCreateDateResult = $getPoliciesByCreateDateResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \RFD\AlfaStrahInsurance\StructType\GetPoliciesByCreateDateResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
