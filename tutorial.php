<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the fist needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientbase class each generated ServiceType class extends this class
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = array(
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://ti.alfastrah.ru/TIService/InsuranceAlfaService.svc?singleWsdl',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * );
 * etc....
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = array(
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://ti.alfastrah.ru/TIService/InsuranceAlfaService.svc?singleWsdl',
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \RFD\AlfaStrahInsurance\ClassMap::get(),
);
/**
 * Samples for Get ServiceType
 */
$get = new \RFD\AlfaStrahInsurance\ServiceType\Get($options);
/**
 * Sample call for GetAdditionalConditions operation/method
 */
if ($get->GetAdditionalConditions(new \RFD\AlfaStrahInsurance\StructType\GetAdditionalConditions()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetAdditionalConditions2 operation/method
 */
if ($get->GetAdditionalConditions2(new \RFD\AlfaStrahInsurance\StructType\GetAdditionalConditions2()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetAssistance operation/method
 */
if ($get->GetAssistance(new \RFD\AlfaStrahInsurance\StructType\GetAssistance()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCountries operation/method
 */
if ($get->GetCountries(new \RFD\AlfaStrahInsurance\StructType\GetCountries()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCountriesRus operation/method
 */
if ($get->GetCountriesRus(new \RFD\AlfaStrahInsurance\StructType\GetCountriesRus()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetCurrency operation/method
 */
if ($get->GetCurrency(new \RFD\AlfaStrahInsurance\StructType\GetCurrency()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetStruhSum operation/method
 */
if ($get->GetStruhSum(new \RFD\AlfaStrahInsurance\StructType\GetStruhSum()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetVariant operation/method
 */
if ($get->GetVariant(new \RFD\AlfaStrahInsurance\StructType\GetVariant()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetInsuranceProgramms operation/method
 */
if ($get->GetInsuranceProgramms(new \RFD\AlfaStrahInsurance\StructType\GetInsuranceProgramms()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetRisks operation/method
 */
if ($get->GetRisks(new \RFD\AlfaStrahInsurance\StructType\GetRisks()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetFransize operation/method
 */
if ($get->GetFransize(new \RFD\AlfaStrahInsurance\StructType\GetFransize()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetTerritory operation/method
 */
if ($get->GetTerritory(new \RFD\AlfaStrahInsurance\StructType\GetTerritory()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetPoliciesByBeginDate operation/method
 */
if ($get->GetPoliciesByBeginDate(new \RFD\AlfaStrahInsurance\StructType\GetPoliciesByBeginDate()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetPoliciesByCreateDate operation/method
 */
if ($get->GetPoliciesByCreateDate(new \RFD\AlfaStrahInsurance\StructType\GetPoliciesByCreateDate()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for GetPoliciesByPolicyNumber operation/method
 */
if ($get->GetPoliciesByPolicyNumber(new \RFD\AlfaStrahInsurance\StructType\GetPoliciesByPolicyNumber()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Samples for New ServiceType
 */
$new = new \RFD\AlfaStrahInsurance\ServiceType\_New($options);
/**
 * Sample call for NewPolicty operation/method
 */
if ($new->NewPolicty(new \RFD\AlfaStrahInsurance\StructType\NewPolicty()) !== false) {
    print_r($new->getResult());
} else {
    print_r($new->getLastError());
}
/**
 * Samples for Set ServiceType
 */
$set = new \RFD\AlfaStrahInsurance\ServiceType\Set($options);
/**
 * Sample call for SetCancelPolicy operation/method
 */
if ($set->SetCancelPolicy(new \RFD\AlfaStrahInsurance\StructType\SetCancelPolicy()) !== false) {
    print_r($set->getResult());
} else {
    print_r($set->getLastError());
}
/**
 * Sample call for SetAcceptPolicy operation/method
 */
if ($set->SetAcceptPolicy(new \RFD\AlfaStrahInsurance\StructType\SetAcceptPolicy()) !== false) {
    print_r($set->getResult());
} else {
    print_r($set->getLastError());
}
/**
 * Sample call for SetPayPolicy operation/method
 */
if ($set->SetPayPolicy(new \RFD\AlfaStrahInsurance\StructType\SetPayPolicy()) !== false) {
    print_r($set->getResult());
} else {
    print_r($set->getLastError());
}
/**
 * Samples for Create ServiceType
 */
$create = new \RFD\AlfaStrahInsurance\ServiceType\Create($options);
/**
 * Sample call for CreatePayPolicy operation/method
 */
if ($create->CreatePayPolicy(new \RFD\AlfaStrahInsurance\StructType\CreatePayPolicy()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Samples for Person ServiceType
 */
$person = new \RFD\AlfaStrahInsurance\ServiceType\Person($options);
/**
 * Sample call for PersonEditPolicy operation/method
 */
if ($person->PersonEditPolicy(new \RFD\AlfaStrahInsurance\StructType\PersonEditPolicy()) !== false) {
    print_r($person->getResult());
} else {
    print_r($person->getLastError());
}
